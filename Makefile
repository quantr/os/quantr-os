export SHELL=/bin/bash

export TOOLCHAIN_DIR=/toolchain
export AS=$(TOOLCHAIN_DIR)/bin/i586-peter-elf-as
export CC=$(TOOLCHAIN_DIR)/bin/i586-peter-elf-gcc
export CPP=$(TOOLCHAIN_DIR)/bin/i586-peter-elf-g++
export LD=$(TOOLCHAIN_DIR)/bin/i586-peter-elf-ld
export STRIP=$(TOOLCHAIN_DIR)/bin/i586-peter-elf-strip
export NASM=nasm
export AR=$(TOOLCHAIN_DIR)/bin/i586-peter-elf-ar
export NEWLIB_DIR=$(TOOLCHAIN_DIR)/i586-peter-elf
export LIBELF=$(TOOLCHAIN_DIR)/include
export LIBGCC=$(TOOLCHAIN_DIR)/lib/gcc/i586-peter-elf/5.4.0/
export PATH:=/toolchain/bin:/toolchain/i586-peter-elf/bin:/opt/X11/bin/:/opt/local/bin/:${PATH}

export CFLAGS=-w
export CXXFLAGS=-w

CHECKFILE = \
    if [ ! -f "hd.img" ]; then \
        echo "==========================================================="; \
        echo "hd.img does not exist"; \
        echo "==========================================================="; \
        exit 1; \
    fi; \
    FSIZE=$$(wc -c < hd.img) ; \
    if [ $$FSIZE -ne 31457280 ]; then \
        echo "==========================================================="; \
		echo "hd.img size is not correct, it should be 31457280 bytes"; \
        echo "==========================================================="; \
        exit 1; \
    fi

all: newlib-all newlib-install libelf-all libelf-install buildkernel pfs grub2 put_pfs checkHD

all_no_grub: buildkernel pfs

buildkernel: libpeter testingLibrary interrupts pshell
	make --directory=kernel

grub2: kernel
	./utils/createHD.sh

put_pfs:
	./utils/insertData.sh pfs.img hd.img 0 20971520

#all: bootloader/bootsector kernel/pm_setup summary
#	#./utils/fitSector bootloader/bootsector
#	./utils/insertData bootloader/bootsector hd.img 0 0
#	./utils/insertData kernel/pm_setup hd.img 0 512
#	./utils/insertData kernel/kernel hd.img 0 409600

summary:
	@echo
	@echo -ne 'bootsector : $(green)'
	@echo -n $$(ls -l bootloader/bootsector | awk '{print $$5}')
	@echo -e '$(no_color) bytes'
	@echo -ne 'libpeter.a : $(green)'
	@echo -n $$(ls -l library/libpeter/libpeter.a | awk '{print $$5}')
	@echo -e '$(no_color) bytes'
	@echo -ne 'kernel : $(green)'
	@echo -n $$(ls -l kernel/kernel | awk '{print $$5}')
	@echo -e '$(no_color) bytes'
	@echo "done"

bootimage_for_floppy: bootsector kernel
	cat ./bootloader/bootsector ./kernel/pm_setup > bootimage
	utils/fitSector bootimage


bootloader/bootsector :
	make --directory=bootloader

pshell:
	@cd app/pshell; make

app_print :
	@cd app/app_print; make

kernel/pm_setup : libpeter testingLibrary interrupts pshell
	make --directory=kernel

interrupts : interrupt0 interrupt1 interrupt2 interrupt3 interrupt4 interrupt5 interrupt6 interrupt7 interrupt8 interrupt9 interrupta interruptb interruptc interruptd

interrupt0:
	make --directory interrupt/interrupt0

interrupt1 :
	make --directory interrupt/interrupt1

interrupt2 :
	make --directory interrupt/interrupt2

interrupt3 :
	make --directory interrupt/interrupt3

interrupt4 :
	make --directory interrupt/interrupt4

interrupt5 :
	make --directory interrupt/interrupt5

interrupt6 :
	make --directory interrupt/interrupt6

interrupt7 :
	make --directory interrupt/interrupt7

interrupt8 :
	make --directory interrupt/interrupt8

interrupt9 :
	make --directory interrupt/interrupt9

interrupta :
	make --directory interrupt/interrupta

interruptb :
	make --directory interrupt/interruptb

interruptc :
	make --directory interrupt/interruptc

interruptd :
	make --directory interrupt/interruptd

libpeter :
	make --directory=library/libpeter

#plibc :
#	make --directory=library/plibc

testingLibrary:
	make --directory=library/testingLibrary

testingLibraryClean:
	make --directory=library/testingLibrary clean

clean :
	-@rm -f bootimage
	-@rm -f totalsource
	-@rm -f *.rar
	@cd bootloader; make clean
	@cd kernel; make clean
	@cd app/pshell; make clean
	@cd app/app_print; make clean
	@cd library/testingLibrary; make clean
	@cd library/libpeter; make clean
	@cd interrupt/interrupt0; make clean
	@cd interrupt/interrupt1; make clean
	@cd interrupt/interrupt2; make clean
	@cd interrupt/interrupt3; make clean
	@cd interrupt/interrupt4; make clean
	@cd interrupt/interrupt5; make clean
	@cd interrupt/interrupt6; make clean
	@cd interrupt/interrupt7; make clean
	@cd interrupt/interrupt8; make clean
	@cd interrupt/interrupt9; make clean
	@cd interrupt/interrupta; make clean
	@cd interrupt/interruptb; make clean
	@cd interrupt/interruptc; make clean
	@cd interrupt/interruptd; make clean
	-@rm -fr HTML
	-@rm -fr GPATH  GRTAGS  GSYMS  GTAGS
	-@rm -fr doxygen_output
	#-@find . -name "*~" -exec rm -f {} \;
	-@rm -fr utils/root
	-umount temp
	-rm -fr temp
	-kpartx -d hd.img
	-rm -fr hd.img
	-rm -fr pfs.img

html:
	gtags
	htags -F -m start_peter -fFnvat 'Peter os source'

doxygen :
	make clean
	doxygen

format:
	@find . -name "*.cpp" -exec indent -ts4 -kr -l999 {} \;
	@find . -name "predefine.h" -exec indent -ts4 -kr -l999 {} \;
	#@indent -v -br -brs -npsl -ce -l999 -i8 library/plib/predefine.h
	#@find . -name "*.cpp" -exec bcpp {} \;
	#@bcpp -s library/plib/predefine.h

pfs:
	cp -fr library/testingLibrary/libTesting.so utils/generatePFS/root/lib/
	cp -fr library/testingLibrary/libTesting2.so utils/generatePFS/root/lib/
	cp -fr library/testingLibrary/test.exe utils/generatePFS/root/lib/
	#cp -fr kernel/PeterLoader.o utils/generatePFS/root/kernel/
	#cp -fr library/testingLibrary/loader utils/generatePFS/root/lib/
	java -jar utils/PFSBuilder.jar -projectfile utils/generatePFS/project.ini -inputdirectory utils/generatePFS/root -output pfs.img -partitionsize 5M -partitionoffset 0m -blocksize 4096 -partitionname PFS -c # -trimTo=40,16,63
	#utils/insertData pfs.img hd.img 0 1048576

pfsbuilder:
	java -jar utils/PFSBuilder.jar -gui -x -input hd.img -partitionsize 5120k -partitionoffset 6291456 -outputdirectory temp -projectfile project.ini

run:
	xterm -geo 110x60-10-500 -bg black -fg white -e 'cd bochs;rm -fr bochsout.txt;/toolchain/bin/bochs -q -f bochsrcSDL.bxrc'

debugbochs:
	xterm -geo 130x60-10-500 -bg black -fg white -e 'stty erase ^?;cd bochs;rm -fr bochsout.txt;lldb -- /toolchain/bin/bochs -q -f bochsrcSDL.bxrc'

mac:
	osascript -e 'tell application "Terminal" to do script "cd bochs;rm -fr bochsout.txt;/toolchain/bin/bochs -q -f bochsrcSDL.bxrc"'

gkd:
	#cd bochs; java -Djava.library.path=../../peter-bochs-debugger/lib/jogl/jogl-1.1.1-rc8-linux-i586/lib -jar ../utils/peter-bochs-debugger.jar /root/bochs/bin/bochs -q -f bochsrc.bxrc -debug -loadBreakpoint
	cd bochs; java -Djava.library.path=. -jar ../utils/gkd.jar bochs -q -f bochsrcSDL.bxrc -debug -loadBreakpoint -loadelf=../kernel/kernel
	#cd bochs; java -jar ../utils/peter-bochs-debugger.jar bochs-debugger -q -f bochsrc.bxrc -debug -loadBreakpoint

winBochs:
	startCmd.bat

qemuDebug:
	#qemu-system-x86_64 -hda hd.img -boot c -m 128 -net none
	#~/qemu/bin/qemu-system-x86_64 -hda hd.img -gkd tcp::1234 -k en-us -S
	#qemu -L C:\\qemu-0.9.0-windows -hda hd.img -boot c -m 128
	/toolchain/bin/qemu-system-x86_64 -hda hd.img -k en-us -s -S

qemu:
	/toolchain/bin/qemu-system-x86_64 -hda hd.img -net none

update_PFSBuilder:
	wget http://pos.petersoft.com/PFSBuilder/PFSBuilder.jar
	mv PFSBuilder.jar utils/

checkHD:
	@$(CHECKFILE)

newlib-all:
	@cd toolchain/build-newlib; make

newlib-clean:
	@cd toolchain/build-newlib; make clean


newlib-install:
	@cd toolchain/build-newlib; make install

libelf-all:
	@cd toolchain/build-libelf; make

libelf-clean:
	@cd toolchain/build-libelf; make clean

libelf-install:
	@cd toolchain/build-libelf; make install
