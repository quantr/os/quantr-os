#include "predefine.h"
#include <stdio.h>

GDT::GDT()
{
}

void GDT::initGDT(unsigned int GDTTable_addr, unsigned int GDTR_addr)
{
	GDTTable = (Descriptor *) GDTTable_addr;
	gdtr = (unsigned char *) GDTR_addr;
	gdtr[0] = 0;
	gdtr[1] = 0;
	gdtr[2] = (unsigned int) GDTTable;
	gdtr[3] = (unsigned int) GDTTable >> 8;
	gdtr[4] = (unsigned int) GDTTable >> 16;
	gdtr[5] = (unsigned int) GDTTable >> 24;

	setGDTRLimit(0);
}

GDT *GDT::getGDT()
{
	static GDT gdt;
	return &gdt;
}

void GDT::setGDTRLimit(unsigned short numberOfGDTDescriptor)
{
	if (numberOfGDTDescriptor < 8192) {
		if (numberOfGDTDescriptor == 0) {
			*gdtr = (unsigned short) 0;	//gdtr's limit is 8N-1
		} else {
			*gdtr = (unsigned short) (8 * numberOfGDTDescriptor) - 1;	//gdtr's limit is 8N-1
		}
		__asm__ __volatile__("lgdt (%%eax)"::"a"(gdtr));
	}
}

unsigned short GDT::getNumberOfDescriptor()
{
	unsigned short *gdtr2 = (unsigned short *) gdtr;
	return (*gdtr2 + 1) / 8;
}

unsigned short GDT::getGDTRLimit()
{
	unsigned short *size = (unsigned short *) gdtr;
	return *size;
}

unsigned int GDT::getGDTRBase()
{
	unsigned int *gdtr2 = (unsigned int *) (gdtr + 2);
	return *gdtr2;
}

unsigned int GDT::getGDTR()
{
	unsigned int gdtr2 = (unsigned int) gdtr;
	return gdtr2;
}

unsigned int GDT::getGDTTable()
{
	unsigned int addr = (unsigned int) GDTTable;
	return addr;
}

bool GDT::setGDTDescriptor(unsigned int number, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType)
{
	if (number < 8192) {
		GDTTable[number].byte0 = segmentLimit;
		GDTTable[number].byte1 = segmentLimit >> 8;
		GDTTable[number].byte2 = baseAddress;
		GDTTable[number].byte3 = baseAddress >> 8;
		GDTTable[number].byte4 = baseAddress >> 16;
		GDTTable[number].byte5 = segmentType + (S << 4) + (DPL << 5) + (P << 7);
		GDTTable[number].byte6 = (G << 7) + (D << 6) + ((segmentLimit >> 16) & 0xf);
		GDTTable[number].byte7 = baseAddress >> 24;
		return true;
	} else {
		return false;
	}
}

bool GDT::setGDTDescriptor(unsigned int number, Descriptor gdtDescriptor)
{
	if (number < 8192) {
		GDTTable[number] = gdtDescriptor;
		return true;
	} else {
		return false;
	}
}

bool GDT::addGDTDescriptor(unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType)
{
	int number = getNumberOfDescriptor();
	if (setGDTDescriptor(number, baseAddress, segmentLimit, G, D, reserved0, AVL, P, DPL, S, segmentType)) {
		setGDTRLimit(number + 1);
		return true;
	} else {
		return false;
	}
}

bool GDT::addGDTDescriptor(Descriptor gdtDescriptor)
{
	int number = getNumberOfDescriptor();
	if (setGDTDescriptor(number, gdtDescriptor)) {
		setGDTRLimit(number + 1);
		return true;
	} else {
		return false;
	}
}

Descriptor *GDT::getGDTDescriptor(unsigned int x)
{
	if (x < 8192) {
		return &GDTTable[x];
	} else {
		return NULL;
	}
}
