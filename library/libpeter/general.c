#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>

/**************************************************
 function convert
 number of parameter : 2
 parameter 1 types :	char
 signed char
 unsigned char
 short
 int
 long
 unsigned short
 unsigned int
 unsigned long
 float
 double
 long double
 parameter 2 types :	char *
 **************************************************/
void convert(char in, char *out) {
	out[0] = in;
	out[1] = '\0';
}

//void convert(short in, char *out){
//	unsigned int x;
//	short tempinput;
//	unsigned int divisor;
//	if (in >= 0) {
//		out[0] = '+';
//	} else {
//		out[0] = '-';
//		in = abs(in);
//	}
//	tempinput = in;
//	short base = 10;
//	unsigned int numOfDigit = numberOfDigit(in);
//	divisor = pow(base, numOfDigit - 1);
//	for (x = 1; x <= numOfDigit; x++) {
//		if (divisor == 0) {
//			out[x] = 0x30;
//		} else {
//			out[x] = tempinput / divisor + 0x30;
//			tempinput -= (divisor * (tempinput / divisor));
//		}
//		divisor /= 10;
//	}
//	out[x] = '\0';
//}
//
//void convert(long in, char *out){
//	unsigned int x;
//	int tempinput;
//	unsigned int divisor;
//	if (in >= 0) {
//		out[0] = '+';
//	} else {
//		out[0] = '-';
//		in = abs(in);
//	}
//	tempinput = in;
//	int base = 10;
//	unsigned int numOfDigit = numberOfDigit(in);
//	divisor = pow(base, numOfDigit - 1);
//	for (x = 1; x <= numOfDigit; x++) {
//		if (divisor == 0) {
//			out[x] = 0x30;
//		} else {
//			out[x] = tempinput / divisor + 0x30;
//			tempinput -= (divisor * (tempinput / divisor));
//		}
//		divisor /= 10;
//	}
//	out[x] = '\0';
//}
//
//void convert(unsigned long in, char *out)
//{
//	unsigned int x;
//	int tempinput;
//	unsigned int divisor;
//	tempinput = in;
//	int base = 10;
//	unsigned int numOfDigit = numberOfDigit(in);
//	divisor = pow(base, numOfDigit - 1);
//	for (x = 0; x < numOfDigit; x++) {
//		if (divisor == 0) {
//			out[x] = 0x30;
//		} else {
//			out[x] = tempinput / divisor + 0x30;
//			tempinput -= (divisor * (tempinput / divisor));
//		}
//		divisor /= 10;
//	}
//	out[x] = '\0';
//}
//
//void convert(int in, char *out)
//{
//	unsigned int x;
//	int tempinput;
//	unsigned int divisor;
//	if (in >= 0) {
//		out[0] = '+';
//	} else {
//		out[0] = '-';
//		in = abs(in);
//	}
//	tempinput = in;
//	int base = 10;
//	unsigned int numOfDigit = numberOfDigit(in);
//	divisor = pow(base, numOfDigit - 1);
//	for (x = 1; x <= numOfDigit; x++) {
//		if (divisor == 0) {
//			out[x] = 0x30;
//		} else {
//			out[x] = tempinput / divisor + 0x30;
//			tempinput -= (divisor * (tempinput / divisor));
//		}
//		divisor /= 10;
//	}
//	out[x] = '\0';
//}
void convertUnsignedLongLong(unsigned long long in, char *out) {
	unsigned int x;
	unsigned long long tempinput;
	double divisor;
	if (in >= 0) {
		out[0] = '+';
	} else {
		out[0] = '-';
		in = abs(in);
	}
	tempinput = in;
	unsigned long long base = 10;
	unsigned int numOfDigit = numberOfDigit(in);
	divisor = pow(base, numOfDigit - 1);
	for (x = 1; x <= numOfDigit; x++) {
		if (divisor == 0) {
			out[x] = 0x30;
		} else {
			out[x] = tempinput / divisor + 0x30;
			tempinput -= (divisor * (tempinput / divisor));
		}
		divisor /= 10;
	}
	out[x] = '\0';
}
//
//void convert(char *in, unsigned long long *out)
//{
//	// bytes have to eight bytes long, otherwise crash
//	*out = in[0];
//	*out += in[1] << 8;
//	*out += in[2] << 16;
//	*out += in[3] << 24;
//	*out += (unsigned long long) in[4] << 32;
//	*out += (unsigned long long) in[5] << 40;
//	*out += (unsigned long long) in[6] << 48;
//	*out += (unsigned long long) in[7] << 56;
//}
//
//void convert(unsigned char *in, unsigned long long *out)
//{
//	// bytes have to eight bytes long, otherwise crash
//	*out = in[0];
//	*out += in[1] << 8;
//	*out += in[2] << 16;
//	*out += in[3] << 24;
//	*out += (unsigned long long) in[4] << 32;
//	*out += (unsigned long long) in[5] << 40;
//	*out += (unsigned long long) in[6] << 48;
//	*out += (unsigned long long) in[7] << 56;
//}
//
//
//void convert(unsigned short in, char *out)
//{
//}
//void convert(unsigned int in, char *out)
//{
//	unsigned int x;
//	unsigned int divisor;
//	//print("numOfDigit=%u\n",numOfDigit);
//	int base = 10;
//	unsigned int numOfDigit = numberOfDigit(in);
//	divisor = pow(base, numOfDigit - 1);
//	//print("divisor=%u\n",divisor);
//	for (x = 0; x < numOfDigit && divisor != 0; x++) {
//		out[x] = in / divisor + 0x30;
//		in -= (divisor * (in / divisor));
//		divisor /= 10;
//	}
//	out[x] = '\0';
//}
//
//void convert(float in, char *out)
//{
//}
//
//void convert(double in, char *out)
//{
//}
//
//void convert(long double in, char *out)
//{
//}
//
int convertToInt(char *in) {
	int returnValue = 0;
	int base = 1;
	int x;
	for (x = strlen(in) - 1; x >= 0; x--) {
		returnValue += (in[x] - 0x30) * base;
		base *= 10;
	}
	return returnValue;
}
//
//int convertToInt(char *in, int count)
//{
//	int returnValue = 0;
//	int base = 1;
//	int x;
//	for (x = count - 1; x >= 0; x--) {
//		returnValue += (in[x] - 0x30) * base;
//		base *= 10;
//	}
//	return returnValue;
//}

int convertBCDToInt(unsigned char c) {
	char firstDigit = c;
	char lastDigit = c;
	firstDigit = firstDigit & 0xf0;
	firstDigit = firstDigit >> 4;
	lastDigit = lastDigit & 0xf;
	return firstDigit * 10 + lastDigit;
}

void store(int in, char *out) {
	out[0] = in & 0xff;
	out[1] = (in >> 8) & 0xff;
	out[2] = (in >> 16) & 0xff;
	out[3] = (in >> 24) & 0xff;
}

/************************************************************************
 end convert
 ************************************************************************/
/**************************************************
 function sscanf
 number of parameter : 1
 parameter 1 types :	short
 int
 long
 unsigned short
 unsigned int
 unsigned long
 char
 signed char
 unsigned char
 float
 double
 long double
 **************************************************/
/*bool sscanf(short para1, char *args, ...){
 va_list ap;
 va_start(ap, args);
 unsigned int x=0;

 char	*c;
 char	*s;
 int	*d;
 long	*l;

 if (args[0]=='%'){
 switch(args[1]){
 case 'c':
 c = va_arg(ap, char *);
 *c = para1;
 case 's':
 s = va_arg(ap, char *);
 *s = para1;
 case 'd':
 d = va_arg(ap, int *);
 *d = para1;
 break;
 }
 }else{
 return false;
 }
 while(args[x]){
 switch(args[x]){
 }
 }
 }
 */
/************************************************************************
 end sscanf
 ************************************************************************/

int isFileExists(const char* filename) {
	FILE* fp = NULL;

	//this will fail if more capabilities to read the
	//contents of the file is required (e.g. \private\...)
	fp = fopen(filename, "r");
	printf("%s,%x\n", filename, fp);
	if (fp != NULL) {
		fclose(fp);
		return 1;
	}
	fclose(fp);

	return 0;
}
