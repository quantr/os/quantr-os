#ifndef __GENERAL_H__
#define __GENERAL_H__

#ifdef __cplusplus
extern "C" {
#endif 
	
void convert(char in, char *out);
//void convert(signed char in, char *out);
//void convert(unsigned char in, char *out);
//void convert(short in, char *out);
//void convert(long in, char *out);
//void convert(unsigned long in, char *out);
//void convert(int in, char *out);
//void convert(unsigned long long in, char *out);
//void convert(char *in, unsigned long long *out);
//void convert(unsigned char *in, unsigned long long *out);
//void convert(unsigned short in, char *out);
//void convert(unsigned int in, char *out);
//void convert(float in, char *out);
//void convert(double in, char *out);
//void convert(long double in, char *out);
int convertToInt(char *in);
//int convertToInt(char *in, int count);
int convertBCDToInt(unsigned char c);
void store(int in, char *out);
//unsigned int numberOfDigit(short input);
unsigned int numberOfDigit(int input);
//unsigned int numberOfDigit(long input);
//unsigned int numberOfDigit(unsigned short input);
//unsigned int numberOfDigit(unsigned int input);
//unsigned int numberOfDigit(short input);
//unsigned int numberOfDigit(unsigned long input);
//unsigned int numberOfDigit(unsigned long long input);
//unsigned int numberOfDigit(char input);
//unsigned int numberOfDigit(signed char input);
//unsigned int numberOfDigit(unsigned char input);
//unsigned int numberOfDigit(float input);
//unsigned int numberOfDigit(double input);
//unsigned int numberOfDigit(long double input);

int isFileExists(const char* filename);

#ifdef __cplusplus
}
#endif 
#endif
