#ifndef __VFS_H__
#define __VFS_H__

#include <stdio.h>
#include <dirent.h>

#ifdef __cplusplus
extern "C" {
#endif

	FILE *vfs_fopen(const char *filepath, const char *mode);

	int vfs_fread(void *buffer, size_t size, size_t count, FILE * stream);
	int vfs_fseek(FILE * stream, long offset, int whence);
	long vfs_ftell(FILE * stream);
	int vfs_fclose(FILE * stream);
	void vfs_rewind(FILE * stream);
	struct dirent *vfs_opendir(const char *filepath);
	//struct dirent *vfs_readdir(struct dirent * dir);
	const char * vfs_detectPatitionType();

#ifdef __cplusplus
}
#endif
#endif
