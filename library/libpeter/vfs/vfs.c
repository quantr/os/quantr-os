#include "vfs.h"
#include <string.h>
#include <stdio.h>
#include "pfs/pfs.h"

FILE *vfs_fopen(const char *filepath, const char *mode) {
	if (strlen(filepath) > 0) {
		FILE *file;
		if (filepath[0] == pfs_getSeparator()) {
			file = (FILE *) pfs_findFile(filepath, pfs_getSuperBlock()->rootDirectoryLink);
		} else {
			file = (FILE *) pfs_findFile(filepath, pfs_getCurrentDirectoryBlockNo());
		}
		//printf("opened file=%x\n", file);
		if (file == 0) {
			printf("unable to open\n");
			while (1);
		}
		return file;
	} else {
		printf("unable to open\n");
		return NULL;
	}
}

int vfs_fread(void *buffer, size_t size, size_t count, FILE * stream) {
	PFS_File *pfs_file = (PFS_File *) stream;
	return pfs_fread(buffer, size, count, pfs_file);
	//return pfs_file->fread(buffer, size, count);
}

int vfs_fseek(FILE * stream, long offset, int whence) {
	if (stream == NULL) {
		return -1;
	} else {
		PFS_File *pfs_file = (PFS_File *) stream;
		if (whence == SEEK_SET) {
			pfs_file->position = offset;
			//stream->position=offset;
			//return offset;
			//return 0;
		} else if (whence == SEEK_END) {
			pfs_file->position = pfs_file->filesize + offset;
			//return pfs_file->filesize + offset;
			//return offset;
		} else if (whence == SEEK_CUR) {
			//			stream->setFilePositionIndicator(stream->getFilePositionIndicator()
			//					+ offset);
			pfs_file->position += offset;
			//return 0;
		} else {
			pfs_file->position = offset;
			//return offset;
			//return 0;
		}
		return pfs_file->position;
	}
}

//long vfs_ftell(FILE * stream) {
//	return stream->position;
//}
//
//int vfs_fclose(FILE * stream) {
//	stream->position=0;
//	return -1;
//}
//
//void vfs_rewind(FILE * stream) {
//	stream->position=0;
//}

struct dirent *vfs_opendir(const char *filepath) {
	if (strlen(filepath) > 0) {
		struct dirent *dir;
		if (filepath[0] == pfs_getSeparator()) {
			dir = (struct dirent *) pfs_findDirectory(filepath,
					pfs_getSuperBlock()->rootDirectoryLink);
		} else {
			//dir = (struct dirent *) pfs_findDirectory(filepath, PFS_Main::getPFSMain()->getCurrentDirectoryBlockNo());
		}
		return dir;
	} else {
		return NULL;
	}
}

//struct dirent *vfs_readdir(struct dirent * dir)
//{
//	return dir->readdir();
//}

const char * vfs_detectPatitionType() {
	return "pfs";
}

int vfs_fclose(FILE * stream) {
	//return pfs_fclose(stream);
	return -1;
}
