#ifndef PFS_H_
#define PFS_H_

#include "PFS_File.h"
#include "PFS_Directory.h"
#include "init.h"
#include "SuperBlock.h"
#include <dirent.h>
#include <string.h>

extern unsigned long long currentDirectoryBlockNo;


char pfs_getSeparator();
PFS_File * pfs_findFile(const char *path, unsigned long long blockNumber);
PFS_Directory * pfs_findDirectory(const char *path, unsigned long long blockNumber);
unsigned long long getNumberOfFile(PFS_Directory);
unsigned long long getNumberOfDirectory(PFS_Directory);
FileOrDirectoryLinks_struct getFileOrDirectoryLink(PFS_Directory dir, unsigned long long index);
char * getName(unsigned long long);

//unsigned int readBlock(char *data, unsigned long long blockNumber, unsigned int numberOfByte=BLOCKSIZE);
SuperBlock * pfs_getSuperBlock();

unsigned long long getFileContentBlockNo(unsigned int x, PFS_File *pfs_file);

unsigned int pfs_readBlock(char *data, unsigned long long blockNumber);
int pfs_fread(void *buffer, size_t size, size_t count, PFS_File *pfs_file);
struct dirent * pfs_readdir(PFS_Directory *dir);

void pfs_setCurrentDirectoryBlockNo(unsigned long long currentDirectoryBlockNo);

unsigned long long pfs_getCurrentDirectoryBlockNo();

//int pfs_fclose(FILE * stream);

#endif /*PFS_H_*/
