#ifndef __SUPERBLOCK_H__
#define __SUPERBLOCK_H__

struct {
    char id[20];
    char partitionName[501];
    unsigned long long rootDirectoryLink;
    unsigned long long createTime;
    unsigned long long lastModifiedTime;
    unsigned short blockSize;
    unsigned long long numberOfFreeAddressBlock;
    unsigned char unused[3560];
} __attribute__ ((packed)) typedef SuperBlock;


#endif
