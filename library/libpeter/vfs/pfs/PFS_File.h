#ifndef PFS_FILE_H_
#define PFS_FILE_H_

#include "init.h"

struct{
	unsigned long long link;
	unsigned long long blockNumber;
    char id[5];
    char name[501];
    char permission[9];
    unsigned long long createTime;
    unsigned long long lastModifiedTime;
    unsigned long long filesize;
    unsigned long long fileIndirectBlockLink;
    unsigned long long fileContentBlockLinks[NUMBER_OF_FILECONTENTBLOCKLINKS];
    unsigned long long position;
} __attribute__ ((packed)) typedef PFS_File;

int loadFileBlock(PFS_File *, unsigned long long blockNumber);

#endif /*PFS_FILE_H_*/
