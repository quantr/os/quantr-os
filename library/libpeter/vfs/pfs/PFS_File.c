#include "pfs.h"
#include "PFS_File.h"
#include "system.h"

int loadFileBlock(PFS_File *pfs_file, unsigned long long blockNumber) {
	unsigned char bytes[BLOCKSIZE];

	/*readSector(PARTITIONOFFSET / 512, 0, 0 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes);
	 readSector(PARTITIONOFFSET / 512, 0, 1 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes + 512);
	 readSector(PARTITIONOFFSET / 512, 0, 2 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes + 1024);
	 readSector(PARTITIONOFFSET / 512, 0, 3 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes + 1536);
	 readSector(PARTITIONOFFSET / 512, 0, 4 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes + 2048);
	 readSector(PARTITIONOFFSET / 512, 0, 5 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes + 2560);
	 readSector(PARTITIONOFFSET / 512, 0, 6 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes + 3072);
	 readSector(PARTITIONOFFSET / 512, 0, 7 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes + 3584); */

	//unsigned int byteRead = readSector(PARTITIONOFFSET / 512, 0, 0 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes, 8 * 512);
	unsigned int byteRead = pfs_readBlock(bytes, blockNumber);
	if (byteRead != BLOCKSIZE) {
		return -1;
	}

	pfs_file->id[0] = bytes[0];
	pfs_file->id[1] = bytes[1];
	pfs_file->id[2] = bytes[2];
	pfs_file->id[3] = bytes[3];
	pfs_file->id[4] = '\0';

	int x;
	for (x = 0; x < 500; x++) {
		pfs_file->name[x] = bytes[x + 4];
	}

	pfs_file->permission[0] = bytes[504];
	pfs_file->permission[0] = bytes[505];
	pfs_file->permission[0] = bytes[506];
	pfs_file->permission[0] = bytes[507];
	pfs_file->permission[0] = bytes[508];
	pfs_file->permission[0] = bytes[509];
	pfs_file->permission[0] = bytes[510];
	pfs_file->permission[0] = bytes[511];
	pfs_file->permission[0] = bytes[512];

	pfs_file->createTime = bytes[513] + (bytes[514] << 8) + (bytes[515] << 16) + (bytes[516] << 24) + (bytes[517] << 32) + (bytes[518] << 40) + (bytes[519] << 48) + (bytes[520] << 56);
	//convert(bytes + 513, &createTime);
	//sscanf((char *)bytes+513, "%ul", &pfs_file->createTime);
	pfs_file->lastModifiedTime = bytes[521] + (bytes[522] << 8) + (bytes[523] << 16) + (bytes[524] << 24) + (bytes[525] << 32) + (bytes[526] << 40) + (bytes[527] << 48) + (bytes[528] << 56);
	//convert(bytes + 521, &lastModifiedTime);
	//sscanf((char *)bytes+521, "%ul", &pfs_file->lastModifiedTime);
	pfs_file->filesize = bytes[529] + (bytes[530] << 8) + (bytes[531] << 16) + (bytes[532] << 24) + (bytes[533] << 32) + (bytes[534] << 40) + (bytes[535] << 48) + (bytes[536] << 56);
	//convert(bytes + 529, &filesize);

	pfs_file->fileIndirectBlockLink = bytes[537] + (bytes[538] << 8) + (bytes[539] << 16) + (bytes[540] < 24) + (bytes[541] << 32) + (bytes[542] << 40) + (bytes[543] << 48) + (bytes[544] << 56);
	//convert(bytes + 537, &fileIndirectBlockLink);
	//sscanf((char *)bytes+537, "%ul", &pfs_file->fileIndirectBlockLink);
	printf(" 2> %x, %x\n", pfs_file, getPhysicalAddress(pfs_file));
	printf(" 2> %x, %x\n", &(pfs_file->createTime), getPhysicalAddress(&(pfs_file->createTime)));
	printf(" 4> %x, %x\n", &(pfs_file->filesize), getPhysicalAddress(&(pfs_file->filesize)));
	printf(" 5> %x, %x\n", &(pfs_file->fileContentBlockLinks), getPhysicalAddress(&(pfs_file->fileContentBlockLinks)));
	printf(" 2> %x\n", ((BLOCKSIZE - 545) / sizeof (pfs_file->fileContentBlockLinks[0])));
	printf(" 2> x=%x\n", &x);
	//for (x = 0; x < ((BLOCKSIZE - 545) / sizeof (pfs_file->fileContentBlockLinks[0])); x++) {
	for (int x = 545, z = 0; x <= BLOCKSIZE - 8; x += 8, z++) {
		if (&(bytes[x + 5])==0x242a000){
			printf("    3> %x, %x, %x\n", pfs_file, &(pfs_file->fileContentBlockLinks[z]), getPhysicalAddress(&(pfs_file->fileContentBlockLinks[z])));
			BOCHS_PAUSE
		}
		pfs_file->fileContentBlockLinks[z] = bytes[x] + (bytes[x + 1] << 8) + (bytes[x + 2] << 16) + (bytes[x + 3] << 24) + (bytes[x + 4] << 32) + (bytes[x + 5] << 40) + (bytes[x + 6] << 48) + (bytes[x + 7] << 56);
		//convert(bytes + (545 + x * 8), &fileContentBlockLinks[x]);
		//sscanf((char *)(bytes + (545 + x * 8)), "%ul", &pfs_file->fileContentBlockLinks[x]);
	}

	pfs_file->position = 0;
}
