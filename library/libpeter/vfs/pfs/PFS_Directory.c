#include "pfs.h"
#include "PFS_Directory.h"

int loadDirectoryBlock(PFS_Directory *pfs_directory, unsigned long long blockNumber) {
	//printf("loadDirectoryBlock %d\n",blockNumber);
	pfs_directory->blockNumber=blockNumber;
	unsigned char bytes[4096];
	/*unsigned int byteRead = readSector(PARTITIONOFFSET / 512, 0,
	 0 + blockNumber * 8 + SUPERBLOCKSIZE / 512, bytes, 8 * 512); */

	unsigned int byteRead = pfs_readBlock(bytes, blockNumber);

	if (byteRead != BLOCKSIZE) {
		volatile int x = 0;
		while (1) {
			x = x + 0x50;
		}
		return -1;
	}

	pfs_directory->id[0] = bytes[0];
	pfs_directory->id[1] = bytes[1];
	pfs_directory->id[2] = bytes[2];
	pfs_directory->id[3] = '\0';

	int x;
	for (x = 0; x < 500; x++) {
		pfs_directory->name[x] = bytes[x + 3];
	}
	pfs_directory->name[500] = '\0';
	//printf("		pfs_directory->name=%s\n",pfs_directory->name);


	pfs_directory->numberOfFile = bytes[503] + (bytes[504] << 8) + (bytes[505]
			<< 16) + (bytes[506] << 24);
	//while(1);
	pfs_directory->numberOfDirectory = bytes[507] + (bytes[508] << 8)
			+ (bytes[509] << 16) + (bytes[510] << 24);

	//printf("loadDirectoryBlock %d, %d\n",pfs_directory->numberOfFile,pfs_directory->numberOfDirectory);

	pfs_directory->permission[0] = bytes[511];
	pfs_directory->permission[0] = bytes[512];
	pfs_directory->permission[0] = bytes[513];
	pfs_directory->permission[0] = bytes[514];
	pfs_directory->permission[0] = bytes[515];
	pfs_directory->permission[0] = bytes[516];
	pfs_directory->permission[0] = bytes[517];
	pfs_directory->permission[0] = bytes[518];
	pfs_directory->permission[0] = bytes[519];

	//    createTime = bytes[520] + (bytes[521] << 8) + (bytes[522] << 16) + (bytes[523] << 24) + (bytes[524] << 32) + (bytes[525] << 40) + (bytes[526] << 48) + (bytes[527] << 56);
	//convert(bytes + 520, &createTime);
	sscanf((char *)(bytes + 520), "%ul", &pfs_directory->createTime);
	//    lastModifiedTime = bytes[528] + (bytes[529] << 8) + (bytes[530] << 16) + (bytes[531] << 24) + (bytes[532] << 32) + (bytes[533] << 40) + (bytes[534] << 48) + (bytes[535] << 56);
	//convert(bytes + 528, &lastModifiedTime);
	sscanf((char *)(bytes + 528), "%ul", &pfs_directory->lastModifiedTime);
	//    directoryIndirectBlock = bytes[536] + (bytes[537] << 8) + (bytes[538] << 16) + (bytes[539] < 24) + (bytes[540] << 32) + (bytes[541] << 40) + (bytes[542] << 48) + (bytes[543] << 56);
	//convert(bytes + 536, &directoryIndirectBlock);
	sscanf((char *)(bytes + 536), "%ul", &pfs_directory->directoryIndirectBlock);

	for (x = 0; x < ((BLOCKSIZE - 544) / sizeof(FileOrDirectoryLinks_struct))
			- 9; x++) {
		pfs_directory->fileOrDirectoryLinks[x].id = bytes[544 + x * 9];
		pfs_directory->fileOrDirectoryLinks[x].link = bytes[544 + x * 9 + 1]
				+ (bytes[544 + x * 9 + 2] << 8)
				+ (bytes[544 + x * 9 + 3] << 16) + (bytes[544 + x * 9 + 4]
				<< 24) + (bytes[544 + x * 9 + 5] << 32)
				+ (bytes[544 + x * 9 + 6] << 40) + (bytes[544 + x * 9 + 7]
				<< 48) + (bytes[544 + x * 9 + 8] << 56);
	}
	return 0;
}
