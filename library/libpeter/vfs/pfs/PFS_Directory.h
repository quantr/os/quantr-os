#ifndef PFS_DIRECTORY_H_
#define PFS_DIRECTORY_H_

#include "init.h"

struct {
	unsigned long long link;

	unsigned long long blockNumber;
	char id[4];
	char name[501];

	unsigned long long numberOfFile;
	unsigned long long numberOfDirectory;
	char permission[9];

	unsigned long long createTime;
	unsigned long long lastModifiedTime;
	unsigned long long directoryIndirectBlock;
	unsigned int directory_read;

	FileOrDirectoryLinks_struct fileOrDirectoryLinks[NUMBER_OF_FILEORDIRECTORYLINKS];

}typedef PFS_Directory;

int loadDirectoryBlock(PFS_Directory *, unsigned long long blockNumber);

#endif /*PFS_DIRECTORY_H_*/
