#ifndef INIT_H_
#define INIT_H_

struct{
	unsigned char id;
    unsigned long long link;
} __attribute__ ((packed)) typedef FileOrDirectoryLinks_struct;

#define PARTITIONOFFSET 20971520
#define SUPERBLOCKSIZE  4096
#define BLOCKSIZE       4096

#define NUMBER_OF_FILEORDIRECTORYLINKS (BLOCKSIZE-544)/sizeof(FileOrDirectoryLinks_struct)
#define NUMBER_OF_FILECONTENTBLOCKLINKS (BLOCKSIZE-537) / 4
#define CACHE_SIZE_IN_BLOCK	10

#endif /*INIT_H_*/
