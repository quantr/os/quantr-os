#include "pfs.h"
#include "PFS_File.h"
#include "PFS_Directory.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <system.h>
#include <io.h>

unsigned long long currentDirectoryBlockNo;

char blockCaches[CACHE_SIZE_IN_BLOCK * BLOCKSIZE];
unsigned long long blockIDCaches[CACHE_SIZE_IN_BLOCK];
int blockCacheSize = 0;
int blockCacheTail = -1;
unsigned int directory_read = 0;
unsigned long long currentDirectoryBlockNo = -1;

char pfs_getSeparator() {
	return '/';
}

PFS_File * pfs_findFile(const char *path, unsigned long long blockNumber) {
	//printf("pfs_findFile, path=%s, blockNumber=%ld\n", path, blockNumber);
	//////// error case ////////////////////////////////
	if (path[strlen(path) - 1] == pfs_getSeparator()) {
		return NULL;
	} else if (strlen(path) == 0) {
		return NULL;
	}
	/////// end error case /////////////////////////////
	if (path[0] == pfs_getSeparator()) {
		path++;
	}
	// search file in directory
	PFS_Directory dir;
	loadDirectoryBlock(&dir, blockNumber);
	//printf("dir->name=%s\n", dir.name);

	int tokenLength = strlen(path) - strlen(strchr(path, pfs_getSeparator()));
	char *tokenName;
	int isFile;

	if (strchr(path, pfs_getSeparator()) == 0) {
		isFile = 1;
		tokenName = (char *) malloc(strlen(path) + 1);
		strcpy(tokenName, path);
		tokenName[strlen(path)] = '\0';
	} else {
		isFile = 0;
		tokenName = (char *) malloc(tokenLength + 1);
		strncpy(tokenName, path, tokenLength);
		tokenName[tokenLength] = '\0';
	}

	if ((dir.numberOfFile + dir.numberOfDirectory) >= NUMBER_OF_FILEORDIRECTORYLINKS) {
		printf("pfs_findFile error, too large number of file and number of directory = %ull >= NUMBER_OF_FILEORDIRECTORYLINKS=%d\n", (dir.numberOfFile + dir.numberOfDirectory),NUMBER_OF_FILEORDIRECTORYLINKS);
		return NULL;
	}
	unsigned long long x;
	for (x = 0; x < dir.numberOfFile + dir.numberOfDirectory; x++) {
		if (isFile) {
			if (dir.fileOrDirectoryLinks[x].id == 0 && dir.fileOrDirectoryLinks[x].link != 0) {
				unsigned long long link = dir.fileOrDirectoryLinks[x].link;
				//PFS_File file;
//				BOCHS_PAUSE
//				START_INSTRUMENTATION
				printf("------------------\n");
				struct VirutalAddressBlock *freeVirtualAddressList = (struct VirutalAddressBlock *) FREEVIRTUALADDRESSLIST;
				printf(" [0]=0x%x, [1]=0x%x\n", freeVirtualAddressList->offset, freeVirtualAddressList->size);
				PFS_File *file = (PFS_File *) malloc(sizeof(PFS_File));
				printf("sizeof(PFS_File)=0x%x\n", sizeof(PFS_File));
//				STOP_INSTRUMENTATION
				printf("\tFUCK %s\n", path);
				printf("\tFUCK PFS_File=%x, %x\n", file, getPhysicalAddress(file));
//				PFS_File *file2 = (PFS_File *) malloc(sizeof(PFS_File));
//				printf("\t\t\t\t\tFUCK PFS_File2=%x\n", file2);
				loadFileBlock(file, link);
				//printf("tokenName=%s<, file->name=%s<, %d\n", tokenName, file->name, strcmp(tokenName, file->name));
				if (strcmp(tokenName, file->name) == 0) {
					printf("\t\t\t\t\tPFS_File=%x\n", file);
					return file;
				}
				//BOCHS_PAUSE
				
				free(file);
			}
		} else {
			if (dir.fileOrDirectoryLinks[x].id == 1 && dir.fileOrDirectoryLinks[x].link != 0) {

				//PFS_Directory dir2(dir.getFileOrDirectoryLink(x).link);
				PFS_Directory dir2;
				loadDirectoryBlock(&dir2, dir.fileOrDirectoryLinks[x].link);

				if (strcmp(tokenName, dir2.name) == 0) {
					//printf("recursive\n");
					//printf("dir=%s\n", dir2.getName());
					return pfs_findFile(path + tokenLength,
							dir.fileOrDirectoryLinks[x].link);
				}
			}
		}
	}
	return NULL;
}

PFS_Directory * pfs_findDirectory(const char *path,
		unsigned long long blockNumber) {
	///////////// error case /////////////////
	if (strlen(path) == 0) {
		return NULL;
	} else if (path[0] == pfs_getSeparator() && strlen(path) == 1) {
		//PFS_Directory *returnDir = new(blockNumber) PFS_Directory;
		PFS_Directory *returnDir = malloc(sizeof(PFS_Directory));
		loadDirectoryBlock(returnDir, blockNumber);
		directory_read = 0;
		return returnDir;
	}
	///////////// end error case /////////////
	PFS_Directory dir;
	loadDirectoryBlock(&dir, blockNumber);
	if (path[0] == pfs_getSeparator()) {
		path++;
	}

	char t[] = { pfs_getSeparator() };
	int tokenLength = strcspn(path, t);
	char *tokenName;
	int lastDir;
	if (tokenLength == strlen(path) || tokenLength == strlen(path) - 1) {
		// file name
		lastDir = 1;
		tokenName = (char *) kmalloc(strlen(path) + 1);
		strcpy(tokenName, path);
		tokenName[strlen(path)] = '\0';
	} else {
		// dir name
		lastDir = 0;
		tokenName = (char *) kmalloc(tokenLength + 1);
		strncpy(tokenName, path, tokenLength);
		tokenName[tokenLength] = '\0';
	}

	if (dir.numberOfFile + dir.numberOfDirectory
			>= NUMBER_OF_FILEORDIRECTORYLINKS) {
		printf(
				"PFS_Main::findDirectory error, too large number of file and number of directory = %d\n",
				dir.numberOfFile + dir.numberOfDirectory);
		return NULL;
	}

	unsigned long long x;
	for (x = 0; x < dir.numberOfFile + dir.numberOfDirectory; x++) {
		if (lastDir) {
			if (dir.fileOrDirectoryLinks[x].id == 1
					&& dir.fileOrDirectoryLinks[x].link != 0) {
				//PFS_Directory *returnDir = new(dir.fileOrDirectoryLinks[x].link) PFS_Directory;
				PFS_Directory *returnDir = malloc(sizeof(PFS_Directory));
				loadDirectoryBlock(returnDir, dir.fileOrDirectoryLinks[x].link);

				//printf("tokenName=%s\n",tokenName);
				//printf("	returnDir->name %s==%s\n",tokenName, returnDir->name);
				if (strcmp(tokenName, returnDir->name) == 0) {
					//printf("bingo\n");
					directory_read = 0;
					return returnDir;
				}

				free(returnDir);
			}
		} else {
			if (dir.fileOrDirectoryLinks[x].id == 1
					&& dir.fileOrDirectoryLinks[x].link != 0) {
				PFS_Directory dir2;
				loadDirectoryBlock(&dir2, dir.fileOrDirectoryLinks[x].link);
				if (strcmp(tokenName, dir2.name) == 0) {
					//print("recursive\n");
					//print("dir=%s\n", dir2.getName());
					return pfs_findDirectory(path + tokenLength,
							dir.fileOrDirectoryLinks[x].link);
				}
			}
		}
	}
	return NULL;
}

SuperBlock * pfs_getSuperBlock() {
	SuperBlock *superBlock = (SuperBlock *) malloc(sizeof(SuperBlock));
	superBlock->rootDirectoryLink = 1;
	return superBlock;
}

unsigned int pfs_readBlock(char *data, unsigned long long blockNumber) {
	// check the block is exist in the cache or not
	int x;
	for (x = 0; x < blockCacheSize; x++) {
		if (blockIDCaches[x] == blockNumber) {
			memcpy((char *) data, blockCaches + x * BLOCKSIZE, BLOCKSIZE);
			return BLOCKSIZE;
		}
	}
	// end check the block is exist in the cache or not
	unsigned int byteRead = readSector(PARTITIONOFFSET / 512, 0, 0 + blockNumber * 8 + SUPERBLOCKSIZE / 512, data, BLOCKSIZE);
	// put it in cache
	//	if (blockCacheSize < CACHE_SIZE_IN_BLOCK) {
	//		blockCacheSize++;
	//	}
	//
	//	if (blockCacheTail == CACHE_SIZE_IN_BLOCK) {
	//		strncpy(blockCaches + blockCacheTail * BLOCKSIZE, (constb char *)data,
	//		BLOCKSIZE);
	//		blockIDCaches[blockCacheTail] = blockNumber;
	//		blockCacheTail++;
	//	}
	// end put it in cache

	// put it in cache
	if (blockCacheSize < CACHE_SIZE_IN_BLOCK) {
		blockCacheSize++;
	}
	blockCacheTail++;
	if (blockCacheTail == CACHE_SIZE_IN_BLOCK) {
		blockCacheTail = 0;
	}
	memcpy(blockCaches + blockCacheTail * BLOCKSIZE, data, BLOCKSIZE);
	blockIDCaches[blockCacheTail] = blockNumber;

	// end put it in cache
	return byteRead;
}

unsigned long long getFileContentBlockNo(unsigned int x, PFS_File *pfs_file) {
	//print("      getFileContentBlockNo start x=%u\n", x);
	unsigned int numerOfFileContentLinkInFileBlock = (BLOCKSIZE - 545) / 8;
	unsigned int numberOfFileContentLinkInFIB = (BLOCKSIZE - 3 - 8) / 8;
	if (x < numerOfFileContentLinkInFileBlock) {
		/*for (unsigned int x = 0; x < ((BLOCKSIZE - 545) / sizeof(fileContentBlockLinks[0])) - 1; x++) {
		 //if (x>395 && x<400){
		 //print("%u=%llu,",x,fileContentBlockLinks[x]);
		 //}
		 } */
		//print("\nfileContentBlockLinks[%u]=%llu\n",x,fileContentBlockLinks[x]);
		return pfs_file->fileContentBlockLinks[x];
	} else {
		if (x < numerOfFileContentLinkInFileBlock) {
			return pfs_file->fileIndirectBlockLink;
		} else {
			int FIBNo = (x - numerOfFileContentLinkInFileBlock)
					/ numberOfFileContentLinkInFIB;
			//print("      1st FIBNo=%d\n", FIBNo);
			unsigned long long FIBLink = pfs_file->fileIndirectBlockLink;
			unsigned char fileContentBlock[BLOCKSIZE];
			int index;
			for (index = 0; index <= FIBNo; index++) {
				unsigned int byteRead = readSector(PARTITIONOFFSET / 512, 0,
						0 + FIBLink * 8 + SUPERBLOCKSIZE / 512,
						fileContentBlock, 8 * 512);
				if (byteRead != BLOCKSIZE) {
					return 0;
				} else {
					convert(fileContentBlock + BLOCKSIZE - 8, &FIBLink);
					if (index == 0) {
						x -= (BLOCKSIZE - 545) / 8;
					} else {
						x -= (BLOCKSIZE - 8 - 3) / 8;
					}
					//print("      FIBLink=%llu, x=%u\n", FIBLink, x);
				}
			}
			unsigned long long fileContentBlockLink;
			//print("      x====%u\n", x);
			convert(fileContentBlock + x * 8 + 3, &fileContentBlockLink);
			if (fileContentBlockLink == 0) {
				printf("      error fileContentBlockLink==0\n");
			}
			//print("      getFileContentBlockNo=%llu\n", fileContentBlockLink);
			return fileContentBlockLink;
		}
	}
}

int pfs_fread(void *buffer, size_t size, size_t count, PFS_File *pfs_file) {
	printf("       pfs_fread, pfs_file=%x\n", pfs_file);
	if (pfs_file->filesize == 0) {
		printf("       pfs_fread error, pfs_file->filesize == 0\n");
		return 0;
	} else if (pfs_file->position >= pfs_file->filesize) {
		printf("       pfs_fread error, pfs_file->position >= pfs_file->filesize\n");
		return -1;
	} else {
		unsigned long long totalSizeToRead = size * count;
		if (totalSizeToRead > pfs_file->filesize) {
			totalSizeToRead = pfs_file->filesize;
		}
		if (totalSizeToRead == 0) {
			return 0;
		} else {
			unsigned long long totalBlockToSkip =
					pfs_file->position / BLOCKSIZE;
			unsigned long long totalBytesToSkipOfCurrentBlock =
					pfs_file->position - totalBlockToSkip
							* BLOCKSIZE;
			unsigned int startBlock = pfs_file->position
					/ BLOCKSIZE;
			unsigned int endBlock;
			if ((pfs_file->position + totalSizeToRead)
					< pfs_file->filesize) {
				endBlock = (pfs_file->position + totalSizeToRead
						- 1) / BLOCKSIZE;
			} else {
				endBlock = (pfs_file->filesize - 1) / BLOCKSIZE;
			}

			//			printf("startBlock=%d, endBlock=%d\n", startBlock, endBlock);

			char tempBuffer[(endBlock - startBlock + 1) * BLOCKSIZE];

			//print("position=%llu\n", position);
			unsigned int x;
			for (x = startBlock; x <= endBlock; x++) {
				//				printf("readBlock=%d\n", x);
				unsigned int byteRead = pfs_readBlock(
						tempBuffer + ((x - startBlock) * BLOCKSIZE),
						getFileContentBlockNo(x, pfs_file));
				//print("%c%c%c%c\n", tempBuffer[((x - startBlock) * BLOCKSIZE) + 1], tempBuffer[((x - startBlock) * BLOCKSIZE) + 2], tempBuffer[((x - startBlock) * BLOCKSIZE) + 3], tempBuffer[((x - startBlock) * BLOCKSIZE) + 4]);
				if (byteRead != BLOCKSIZE) {
					printf(
							"PFS_File::fread() error : Fail to readBlock, block number=%llu,byteRead=%u\n",
							getFileContentBlockNo(x, pfs_file), byteRead);
					while (1)
						;
				}
			}

			//print("totalBlockToSkip=%llu\n", totalBlockToSkip);
			//print("totalBytesToSkipOfCurrentBlock=%llu\n", totalBytesToSkipOfCurrentBlock);
			//print("block no=%u > %u\n", startBlock, endBlock);
			//print("<<<\n");

			int numberOfByteToRead;
			if ((pfs_file->position + totalSizeToRead)
					<= pfs_file->filesize) {
				numberOfByteToRead = totalSizeToRead;
			} else {
				numberOfByteToRead = pfs_file->filesize
						- pfs_file->position;
			}
			memcpy((char *) buffer,
					tempBuffer + totalBytesToSkipOfCurrentBlock,
					numberOfByteToRead);
			pfs_file->position += numberOfByteToRead;
			return numberOfByteToRead;
		}
	}
}

struct dirent * pfs_readdir(PFS_Directory *dir) {
	PFS_Directory *pfs_Directory = (PFS_Directory *) dir;
	if (pfs_Directory->fileOrDirectoryLinks[directory_read].link == 0) {
		return NULL;
	} else {
		struct dirent *temp = (struct dirent *) malloc(sizeof(struct dirent));
		if (pfs_Directory->fileOrDirectoryLinks[directory_read].id == 1) {

			PFS_Directory subDirectory;
			loadDirectoryBlock(&subDirectory,
					pfs_Directory->fileOrDirectoryLinks[directory_read].link);

			//PFS_Directory subDirectory(fileOrDirectoryLinks[directory_read].link);
			strcpy(temp->d_name ,(char*) malloc(strlen(subDirectory.name) + 1));
			strcpy(temp->d_name, subDirectory.name);
		} else {
			PFS_File subFile;
			loadFileBlock(&subFile,
					pfs_Directory->fileOrDirectoryLinks[directory_read].link);
			//PFS_File subFile(fileOrDirectoryLinks[directory_read].link);
			strcpy(temp->d_name,(char *) malloc(strlen(subFile.name) + 1));
			strcpy(temp->d_name, subFile.name);
		}
		directory_read++;
		return temp;
	}
}

void pfs_setCurrentDirectoryBlockNo(unsigned long long x) {
	currentDirectoryBlockNo = x;
}

unsigned long long pfs_getCurrentDirectoryBlockNo() {
	if (currentDirectoryBlockNo == -1) {
		currentDirectoryBlockNo = pfs_getSuperBlock()->rootDirectoryLink;
	}
	return currentDirectoryBlockNo;
}
/*
int pfs_fclose(FILE * stream){
	PFS_File *p=(PFS_File *)stream;
	p->position=0;
	return -1;
}
*/
