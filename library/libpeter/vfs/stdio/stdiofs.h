#ifndef __STDIOFS_H__
#define __STDIOFS_H__

#include <dirent.h>

struct dirent *readdir(DIR *dir);
FILE *fopen(const char *filepath, const char *mode);
int fread(void *buffer, size_t size, size_t count, FILE * stream);
int fseek(FILE * stream, long offset, int whence);
long ftell(FILE * stream);
int fclose(FILE * stream);
void rewind(FILE * stream);

int detectPatitionType(FILE * stream);

#endif
