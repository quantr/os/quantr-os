
FILE *fopen(const char *filepath, const char *mode)
{
	/*FILE *file = (FILE *) PFS_Main::getPFSMain()->findFile(filepath,
	   PFS_Main::getPFSMain()->getSuperBlock()->getRootDirectoryLink()); */
	if (strlen(filepath) > 0) {
		FILE *file;
		if (filepath[0] == PFS_Main::getPFSMain()->getSeparator()) {
			file = (FILE *) PFS_Main::getPFSMain()->findFile(filepath, PFS_Main::getPFSMain()->getSuperBlock()->getRootDirectoryLink());
		} else {
			file = (FILE *) PFS_Main::getPFSMain()->findFile(filepath, PFS_Main::getPFSMain()->getCurrentDirectoryBlockNo());
		}
		return file;
	} else {
		return NULL;
	}
}

int fread(void *buffer, size_t size, size_t count, FILE * stream)
{
	return stream->fread(buffer, size, count);
}

int fseek(FILE * stream, long offset, int whence)
{
	if (stream == NULL) {
		return -1;
	} else {
		if (whence == SEEK_SET) {
			stream->setFilePositionIndicator(offset);
			return 0;
		} else if (whence == SEEK_END) {
			unsigned long long tempX = stream->getFilesize();
			stream->setFilePositionIndicator(tempX);
			return 0;
		} else if (whence == SEEK_CUR) {
			stream->setFilePositionIndicator(stream->getFilePositionIndicator() + offset);
			return 0;
		} else {
			return -1;
		}
	}
}
/*
long ftell(FILE * stream)
{
	return stream->getFilePositionIndicator();
}

int fclose(FILE * stream)
{
	return vfs_fclose(stream);
}

void rewind(FILE * stream)
{
	stream->setFilePositionIndicator(0);
}
*/

DIR *opendir(const char *filepath)
{
	if (strlen(filepath) > 0) {
		DIR *dir;
		if (filepath[0] == PFS_Main::getPFSMain()->getSeparator()) {
			dir = (DIR *) PFS_Main::getPFSMain()->findDirectory(filepath, PFS_Main::getPFSMain()->getSuperBlock()->getRootDirectoryLink());
		} else {
			dir = (DIR *) PFS_Main::getPFSMain()->findDirectory(filepath, PFS_Main::getPFSMain()->getCurrentDirectoryBlockNo());
		}
		return dir;
	} else {
		return NULL;
	}
}

struct dirent *readdir(DIR * dir)
{
	return dir->readdir();
}

int detectPatitionType(FILE * stream)
{
	return 0;
}
