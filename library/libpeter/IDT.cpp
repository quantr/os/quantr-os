#include "predefine.h"
#include <stdio.h>

IDT::IDT()
{
}

void IDT::initIDT(unsigned int IDTTable_addr, unsigned int IDTR_addr)
{
	IDTTable = (Descriptor *) IDTTable_addr;
	idtr = (unsigned char *) IDTR_addr;
	idtr[0] = 0;
	idtr[1] = 0;
	idtr[2] = (unsigned int) IDTTable;
	idtr[3] = (unsigned int) IDTTable >> 8;
	idtr[4] = (unsigned int) IDTTable >> 16;
	idtr[5] = (unsigned int) IDTTable >> 24;

	setIDTRLimit(0);
}

IDT *IDT::getIDT()
{
	static IDT idt;
	return &idt;
}

void IDT::setIDTRLimit(unsigned short numberOfIDTDescriptor)
{
	if (numberOfIDTDescriptor < 256) {
		if (numberOfIDTDescriptor == 0) {
			*idtr = (unsigned short) 0;	//idtr's limit is 8N-1
		} else {
			*idtr = (unsigned short) (8 * numberOfIDTDescriptor) - 1;	//idtr's limit is 8N-1
		}
		__asm__ __volatile__("lidt (%%eax)"::"a"(idtr));
	}
}

unsigned short IDT::getIDTRLimit()
{
	unsigned short *size = (unsigned short *) idtr;
	return *size;
}

unsigned int IDT::getIDTRBase()
{
	return (unsigned int) (idtr + 2);
}

unsigned int IDT::getIDTR()
{
	return (unsigned int) idtr;
}

unsigned int *IDT::getIDTTable()
{
	return (unsigned int *) IDTTable;
}

unsigned short IDT::getNumberOfDescriptor()
{
	unsigned short *idtr2 = (unsigned short *) idtr;
	return (*idtr2 + 1) / 8;
}

bool IDT::setIDTDescriptor(unsigned int number, Descriptor idtDescriptor)
{
	if (number < 256) {
		IDTTable[number] = idtDescriptor;
		return true;
	} else {
		return false;
	}
}

bool IDT::setIDTDescriptor(unsigned int number, unsigned long offset, unsigned short selector, unsigned char type, unsigned char DPL, bool P)
{
	if (number < 256) {
		IDTTable[number].byte0 = offset;
		IDTTable[number].byte1 = offset >> 8;
		IDTTable[number].byte2 = selector;
		IDTTable[number].byte3 = selector >> 8;
		IDTTable[number].byte4 = 0;
		IDTTable[number].byte5 = type;
		DPL &= 0x3;
		IDTTable[number].byte5 |= DPL;
		if (P) {
			IDTTable[number].byte5 |= 80;
		} else {
			IDTTable[number].byte5 |= 0x0;
		}
		IDTTable[number].byte6 = offset >> 16;
		IDTTable[number].byte7 = offset >> 24;
		return true;
	} else {
		return false;
	}
}

bool IDT::addIDTDescriptor(unsigned long offset, unsigned short selector, unsigned char type, unsigned char DPL, bool P)
{
	int number = getNumberOfDescriptor();
	if (setIDTDescriptor(number, offset, selector, type, DPL, P)) {
		setIDTRLimit(number + 1);
		return true;
	} else {
		return false;
	}
}

bool IDT::addIDTDescriptor(Descriptor idtDescriptor)
{
	int number = getNumberOfDescriptor();
	if (setIDTDescriptor(number, idtDescriptor)) {
		setIDTRLimit(number + 1);
		return true;
	} else {
		return false;
	}
}
