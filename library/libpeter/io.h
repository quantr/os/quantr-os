#ifndef __IO_H__
#define __IO_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../../kernel/header.h"
#define Cs 0
#define Hs 0
#define Ss 1

#define READSECTOR_TIMEOUT 10000;

unsigned short getDriveType();

unsigned short getMaxCylinder();

unsigned char getMaxHead();

unsigned char getMaxSector();

unsigned int readSector(unsigned long partitionOffset,
		unsigned long driveNumber, unsigned long lba, unsigned char *data,
		int limit);

void outport(unsigned short portNumber, unsigned char outputByte);

void outStringToPort(unsigned short portNumbder, const char *str);

void outHexToPort(unsigned short PortNumber, unsigned int input);

void outUnsignedLongLongToPort(unsigned short PortNumber,
		unsigned long long ull);

void outLongToPort(unsigned short PortNumber, long ld);

void outUnsignedLongToPort(unsigned short PortNumber, unsigned long lu);

char getChar();
char getPasswordChar(char *passwordChar);

//void getline(char *buffer, unsigned int len);
void getlinePassword(char *buffer, unsigned int len, char *passwordChar);

char *getKeyboardBuffer();

volatile unsigned int *getKeyboardBufferTail();

unsigned int getKeyboardBufferSize();

void disableAllIRQs();

void enableIRQ(unsigned char portNumber);

void disableIRQ(unsigned char portNumber);

int isShiftKeyPressing();

void setShiftKeyPressing(int b);

int isCapsLockOn();

unsigned char inport(unsigned short portNumber);

unsigned short inWordFromPort(unsigned short portNumber);

void outport(unsigned short portNumber, unsigned char outputByte);

unsigned int readSector(unsigned long partitionOffset,
		unsigned long driveNumber, unsigned long lba, unsigned char *data,
		int limit);

#ifdef __cplusplus
}
#endif

#endif
