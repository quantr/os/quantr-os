#include "display.h"
#include "../../kernel/header.h"

unsigned int cursorX;
unsigned int cursorY;
unsigned int videoMode;

void setTextMode() {
	videoMode = 0;
}

void setGraphicMode() {
	videoMode = 1;
}

unsigned int getScreenWidth() {
	return 80;
}

unsigned int getScreenHeight() {
	return 25;
}

void clearScreen() {
	char *vga = (char *) VIDEOMEMORYADDRESS;
	memset(vga, (char) 0, getScreenWidth() * 2 * getScreenHeight());
}

void k_clearScreen() {
	char *vga = (char *) VIDEOMEMORYPHYSICALADDRESS;
	memset(vga, (char) 0, getScreenWidth() * 2 * getScreenHeight());
}

void clearScreenText() {
	unsigned int x, y;
	char *vga = (char *) VIDEOMEMORYADDRESS;
	for (y = 0; y < getScreenHeight(); y++) {
		for (x = 0; x < getScreenWidth(); x++) {
			*(vga + x * 2 + y * getScreenWidth() * 2) = 0;
		}
	}
}

void k_clearScreenText() {
	unsigned int x, y;
	char *vga = (char *) VIDEOMEMORYPHYSICALADDRESS;
	for (y = 0; y < getScreenHeight(); y++) {
		for (x = 0; x < getScreenWidth(); x++) {
			*(vga + x * 2 + y * getScreenWidth() * 2) = 0;
		}
	}
}

int vesaBiosEnable() {
	unsigned char *vbe;
	vbe = (unsigned char *) VBE;
	if (*vbe == 1)
		return 1;
	else
		return 0;
}

void setCursor(unsigned x, unsigned y) {
	cursorX = x;
	cursorY = y;
}

/***************** _k **************************/
void k_moveCursorLeft() {
	if (cursorX > 0) {
		cursorX--;
	}
}

void pageUp() {
	char *vga = (char *) VIDEOMEMORYADDRESS;
	memcpy(vga, vga + getScreenWidth() * 2, getScreenWidth() * 2 * (getScreenHeight() - 1));
	memset(vga + (getScreenHeight() - 1) * getScreenWidth() * 2, (char) 0, getScreenWidth() * 2);
}

void k_pageUp() {
	char *vga = (char *) VIDEOMEMORYPHYSICALADDRESS;
	memcpy(vga, vga + getScreenWidth() * 2, getScreenWidth() * 2 * (getScreenHeight() - 1));
	memset(vga + (getScreenHeight() - 1) * getScreenWidth() * 2, (char) 0, getScreenWidth() * 2);
}

void k_moveCursorNextLine() {
	cursorX = 0;
	if (cursorY == (getScreenHeight() - 1)) {
		k_pageUp();
	} else {
		cursorY++;
	}
}

void k_moveCursorRight(int count) {
	for (; count > 0; count--) {
		if (cursorX == 79) {
			cursorX = 0;
			if (cursorY == (getScreenHeight() - 1)) {
				k_pageUp();
			} else {
				cursorY++;
			}
		} else {
			cursorX++;
		}
	}
}

//////////////////////////////////////////////////////////// end k_ ///////////////////////////////////////////////////

void moveCursorLeft() {
	if (cursorX > 0) {
		cursorX--;
	}
}

void moveCursorRight(int count) {
	for (; count > 0; count--) {
		if (cursorX == 79) {
			cursorX = 0;
			if (cursorY == (getScreenHeight() - 1)) {
				pageUp();
			} else {
				cursorY++;
			}
		} else {
			cursorX++;
		}
	}
	/*
	if (cursorX > 79) {
		int x = x + 0x1980;
		while (1);
	}

	if (cursorY > getScreenHeight() - 1) {
		int x = x + 0x1980;
		while (1);
	}
	*/
}

void moveCursorNextLine() {
	cursorX = 0;
	if (cursorY == (getScreenHeight() - 1)) {
		pageUp();
	} else {
		cursorY++;
	}
}

////////////////////////////////////////// k_ ///////////////////////////////////////////////////////////

void k_outCharToDisplay(char chr, char attr) {
	/*__asm__ __volatile__("push %ds \n"
	 "mov	$0x10,%ax \n"
	 "mov	%ax,%ds"
	 );*/
	/*
	if (cursorX > 79) {
		int x = x + 0x1980;
		while (1);
	}
	if (cursorY > 24) {
		int x = x + 0x1981;
		while (1);
	}
	*/
	char *vga = (char *) VIDEOMEMORYPHYSICALADDRESS + cursorY * 160 + cursorX * 2;
	//__asm__ __volatile__ ("push   %ds\n" "push    $0x10\n" "pop   %ds");

	*vga = chr;
	*(vga + 1) = attr;
	//__asm__ __volatile__ ("pop    %ds");
	//__asm__ __volatile__("pop %ds");
	//k_moveCursorRight(1);
	//cursorX++;
	moveCursorRight(1);
}

void k_outStringToDisplay(char *str, char attr) {
	unsigned int x;
	unsigned len = strlen(str);
	for (x = 0; x < len; x++) {
		if (str[x] == '\n') {
			k_moveCursorNextLine();
		} else if (str[x] == '\t') {
			moveCursorRight(4);
		} else {
			k_outCharToDisplay(str[x], attr);
		}
	}
}

void k_outStringToDisplay_len(char *str, char attr, int len) {
	unsigned int x;
	for (x = 0; x < len; x++) {
		if (str[x] == '\n') {
			k_moveCursorNextLine();
		} else if (str[x] == '\t') {
			moveCursorRight(4);
		} else {
			k_outCharToDisplay(str[x], attr);
		}
	}
}

void k_outHexToDisplay_upper(unsigned int input, char attr) {
	unsigned int reminder[32];
	int r = 0;
	int index;
	index = 0;
	do {
		r = input % 16;
		reminder[index++] = r;
		input /= 16;
	} while (input >= 16);
	if (input != 0) {
		reminder[index] = input;
	} else {
		index--;
	}
	for (r = 0; index >= 0; index--, r++) {
		if (reminder[index] < 10) {
			k_outCharToDisplay((char) (reminder[index] + 0x30), attr);
		} else {

			k_outCharToDisplay((char) (reminder[index] + 0x37), attr);
		}
	}
}

void k_outHexToDisplay_lower(unsigned int input, char attr) {
	unsigned int reminder[32];
	int r = 0;
	int index;
	index = 0;
	do {
		r = input % 16;
		reminder[index++] = r;
		input /= 16;
	} while (input >= 16);
	if (input != 0) {
		reminder[index] = input;
	} else {
		index--;
	}
	for (r = 0; index >= 0; index--, r++) {
		if (reminder[index] < 10) {
			k_outCharToDisplay((char) (reminder[index] + 0x30), attr);
		} else {
			k_outCharToDisplay((char) (reminder[index] + 0x57), attr);
		}
	}
}

void k_outUnsignedLongLongToDisplay(unsigned long long ull, char attr) {
	char buffer[100];
	convert(ull, buffer);
	k_outStringToDisplay(buffer, 15);
}

void k_outLongToDisplay(long ld, char attr) {
	char buffer[100];
	convert(ld, buffer);
	k_outStringToDisplay(buffer, 15);
}

void k_outUnsignedLongToDisplay(unsigned long lu, char attr) {
	char buffer[100];
	convert(lu, buffer);
	k_outStringToDisplay(buffer, 15);
}

/////////////////////////////////////// end k_ ///////////////////////////////////////////////////////////

void outCharToDisplay(char chr, char attr) {
	/*__asm__ __volatile__("push %ds \n"
	 "mov	$0x10,%ax \n"
	 "mov	%ax,%ds"
	 );*/
	//char *vga = (char *)( VIDEOMEMORYADDRESS + cursorX * 2);
	//	while (1) {
	//		unsigned int xxx=cursorX;
	//	}
	char *vga = (char *) (VIDEOMEMORYADDRESS + cursorY * 160 + cursorX * 2);
	char *vga2 = (char *) (VIDEOMEMORYADDRESS);

	//char *vga = (char *)VIDEOMEMORYADDRESS;
	//__asm__ __volatile__ ("push   %ds\n" "push    $0x10\n" "pop   %ds");
	*vga = chr;
	*(vga + 1) = attr;
	//__asm__ __volatile__ ("pop    %ds");
	//__asm__ __volatile__("pop %ds");
	moveCursorRight(1);
}

void outStringToDisplay2(char *str, char attr, int len) {
	unsigned int x;
	if (len == -1) {
		len = strlen(str);
	}
	for (x = 0; x < len; x++) {
		//outHexToDisplay_lower(str[x],9);
		if (str[x] == '\n') {
			moveCursorNextLine();
		} else {
			outCharToDisplay(str[x], attr);
		}
	}
}

void outIntToDisplay(int x, char attr) {
	char str[10]; 
	sprintf(str,"0x%x", x);
	outStringToDisplay2(str, attr, -1);
	outStringToDisplay2(" ", attr, -1);
}

void outIntToDisplayLn(int x, char attr) {
	char str[10]; 
	sprintf(str,"0x%x", x);
	outStringToDisplay2(str, attr, -1);
	outStringToDisplay2("\n", attr, -1);
}

void outStringToDisplay(char *str, char attr) {
	outStringToDisplay2(str, attr, -1);
}

void outStringToDisplay_len(char *str, char attr, int len) {
	unsigned int x;
	for (x = 0; x < len; x++) {
		if (str[x] == '\n') {
			moveCursorNextLine();
		} else if (str[x] == '\t') {
			moveCursorRight(4);
		} else {
			outCharToDisplay(str[x], attr);
		}
	}
}

void outHexToDisplay_upper(unsigned int input, char attr) {
	unsigned int reminder[32];
	int r = 0;
	int index;
	index = 0;
	do {
		r = input % 16;
		reminder[index++] = r;
		input /= 16;
	} while (input >= 16);
	if (input != 0) {
		reminder[index] = input;
	} else {
		index--;
	}
	for (r = 0; index >= 0; index--, r++) {
		if (reminder[index] < 10) {
			outCharToDisplay((char) (reminder[index] + 0x30), attr);
		} else {

			outCharToDisplay((char) (reminder[index] + 0x37), attr);
		}
	}
}

void outHexToDisplay_lower(unsigned int input, char attr) {
	unsigned int reminder[32];
	int r = 0;
	int index;
	index = 0;
	do {
		r = input % 16;
		reminder[index++] = r;
		input /= 16;
	} while (input >= 16);
	if (input != 0) {
		reminder[index] = input;
	} else {
		index--;
	}
	for (r = 0; index >= 0; index--, r++) {
		if (reminder[index] < 10) {
			outCharToDisplay((char) (reminder[index] + 0x30), attr);
		} else {
			outCharToDisplay((char) (reminder[index] + 0x57), attr);
		}
	}
}

void outUnsignedLongLongToDisplay(unsigned long long ull, char attr) {
	char buffer[100];
	convertUnsignedLongLong(ull, buffer);
	outStringToDisplay(buffer, 15);
}

void outLongToDisplay(long ld, char attr) {
	char buffer[100];
	convert(ld, buffer);
	outStringToDisplay(buffer, 15);
}

void outUnsignedLongToDisplay(unsigned long lu, char attr) {
	char buffer[100];
	//convert(lu, buffer);
	//outStringToDisplay(buffer, 15);
}

void printHex(char *str) {
	unsigned int x;
	for (x = 0; x < strlen(str); x++) {
		int firstDigit = (str[x] & 0xf0) >> 4;
		int secondDigit = str[x] & 0xf;
		if (x < strlen(str) - 1) {
			printf("0x%x%x ,", firstDigit, secondDigit);
		} else {
			printf("0x%x%x", firstDigit, secondDigit);
		}
	}
	printf("\n");
}

void printHex2(char *str, unsigned int count) {
	unsigned int x;
	for (x = 0; x < count; x++) {
		int firstDigit = (str[x] & 0xf0) >> 4;
		//              print("firstDigit=%d\n",firstDigit);
		int secondDigit = str[x] & 0xf;
		//              print("secondDigit=%d\n",secondDigit);
		if (x < count - 1) {
			printf("0x%x%x ,", firstDigit, secondDigit);
		} else {
			printf("0x%x%x", firstDigit, secondDigit);
			moveCursorNextLine();
		}
	}
}

void clearLine() {
	/*for (;cursorX=0;cursorX--){
	 outCharToDisplay(0,15);
	 } */
	/*__asm__ __volatile__("pusha \n"
	 "mov	$0x10,%ax \n"
	 "mov	%ax,%ds"
	 );*/
	char *vga = (char *) VIDEOMEMORYADDRESS + getScreenWidth() * 2 * cursorY;
	memset(vga, (char) 0, getScreenWidth() * 2);
	cursorX = 0;
	//__asm__ __volatile__("popa");
}

void printString(char *str, int count) {
	int x;
	for (x = 0; x < count; x++) {
		outCharToDisplay(str[x], 15);
	}
}

////////////////////////////// VESA Information /////////////////////////////

struct VBE_VgaInfo *getVESAInfo() {
	return (struct VBE_VgaInfo *) VESA_BUFFER;
}
