#include "predefine.h"
#include <stdio.h>

LDT::LDT(unsigned int LDTTable_addr)
{
	LDTTable = (Descriptor *) LDTTable_addr;
}

void LDT::setLDTTable(unsigned int LDTTable_addr)
{
	LDTTable = (Descriptor *) LDTTable_addr;
}

unsigned int LDT::getLDTTable()
{
	return (unsigned int) LDTTable;
}

Descriptor *LDT::getLDTDescriptor(unsigned int x)
{
	if (x < 8192) {
		return &LDTTable[x];
	} else {
		return NULL;
	}
}

bool LDT::setLDTDescriptor(unsigned int number, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType)
{
	if (number >= 8192) {
		return false;
	} else {
		LDTTable[number].byte0 = segmentLimit;
		LDTTable[number].byte1 = segmentLimit >> 8;
		LDTTable[number].byte2 = baseAddress;
		LDTTable[number].byte3 = baseAddress >> 8;
		LDTTable[number].byte4 = baseAddress >> 16;
		LDTTable[number].byte5 = segmentType + (S << 4) + (DPL << 5) + (P << 7);
		LDTTable[number].byte6 = (G << 7) + (D << 6) + ((segmentLimit >> 16) & 0xf);
		LDTTable[number].byte7 = baseAddress >> 24;
		return true;
	}
}

bool LDT::setLDTDescriptor(unsigned int number, Descriptor ldtDescriptor)
{
	if (number >= 8192) {
		return false;
	} else {
		LDTTable[number] = ldtDescriptor;
		return true;
	}
}
