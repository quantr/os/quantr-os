struct Descriptor {
	unsigned char byte0;
	unsigned char byte1;
	unsigned char byte2;
	unsigned char byte3;
	unsigned char byte4;
	unsigned char byte5;
	unsigned char byte6;
	unsigned char byte7;
} __attribute__ ((packed));

typedef struct Descriptor Descriptor;

class GDT {
  private:
	unsigned char *gdtr;
	Descriptor *GDTTable;
	 GDT();

  public:
	void initGDT(unsigned int GDTTable_addr, unsigned int GDTR_addr);
	void setGDTRLimit(unsigned short numberOfGDTDescriptor);
	static GDT *getGDT();
	unsigned short getGDTRLimit();
	unsigned short getNumberOfDescriptor();
	unsigned int getGDTRBase();
	unsigned int getGDTR();
	unsigned int getGDTTable();
	Descriptor *getGDTDescriptor(unsigned int x);
	bool setGDTDescriptor(unsigned int number, Descriptor ldtDescriptor);
	bool setGDTDescriptor(unsigned int number, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType);
	bool addGDTDescriptor(unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType);
	bool addGDTDescriptor(Descriptor Descriptor);
};

class LDT {
  private:
	Descriptor * LDTTable;

  public:
	LDT(unsigned int LDTTable_addr);
	void setLDTTable(unsigned int LDTTable_addr);
	unsigned int getLDTTable();
	Descriptor *getLDTDescriptor(unsigned int x);
	bool setLDTDescriptor(unsigned int number, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType);
	bool setLDTDescriptor(unsigned int number, Descriptor ldtDescriptor);
};

class IDT {
  private:
	unsigned char *idtr;
	Descriptor *IDTTable;
	 IDT();

  public:
	void initIDT(unsigned int IDTTable_addr, unsigned int IDTR_addr);
	void setIDTRLimit(unsigned short numberOfIDTDescriptor);
	static IDT *getIDT();
	unsigned short getIDTRLimit();
	unsigned short getNumberOfDescriptor();
	unsigned int getIDTRBase();
	unsigned int getIDTR();
	unsigned int *getIDTTable();
	unsigned char getIDTDescriptor(unsigned int x, unsigned char *out);
	/*bool setIDTDescriptor(unsigned int number, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType);
	   bool setIDTDescriptor(unsigned int number, Descriptor IDTDescriptor);
	   bool addIDTDescriptor(unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType);
	   bool addIDTDescriptor(Descriptor IDTDescriptor); */

	bool setIDTDescriptor(unsigned int number, Descriptor idtDescriptor);
	bool setIDTDescriptor(unsigned int number, unsigned long offset, unsigned short selector, unsigned char type, unsigned char DPL, bool P);
	bool addIDTDescriptor(unsigned long offset, unsigned short selector, unsigned char type, unsigned char DPL, bool P);
	bool addIDTDescriptor(Descriptor idtDescriptor);
};

struct TSS {
	unsigned short backLink;
	unsigned short reserved0;
	unsigned long ESP0;
	unsigned short SS0;
	unsigned short reserved1;
	unsigned long ESP1;
	unsigned short SS1;
	unsigned short reserved2;
	unsigned long ESP2;
	unsigned short SS2;
	unsigned short reserved3;
	unsigned long CR3;
	unsigned long EIP;
	unsigned long EFLAGS;
	unsigned long EAX;
	unsigned long ECX;
	unsigned long EDX;
	unsigned long EBX;
	unsigned long ESP;
	unsigned long EBP;
	unsigned long ESI;
	unsigned long EDI;
	unsigned short ES;
	unsigned short reserved4;
	unsigned short CS;
	unsigned short reserved5;
	unsigned short SS;
	unsigned short reserved6;
	unsigned short DS;
	unsigned short reserved7;
	unsigned short FS;
	unsigned short reserved8;
	unsigned short GS;
	unsigned short reserved9;
	unsigned short LDTR;
	unsigned short reserved10;
	unsigned short T;
	unsigned short IOPB;
} __attribute__ ((packed));

struct Page_Table {
	unsigned char p:1;
	unsigned char rw:1;
	unsigned char us:1;
	unsigned char pwt:1;
	unsigned char pcd:1;
	unsigned char a:1;
	unsigned char d:1;
	unsigned char pat:1;
	unsigned char g:1;
	unsigned char avl:3;
	unsigned int pageTableBase:20;
} __attribute__ ((packed));

struct Page_Directory {
	unsigned char p:1;
	unsigned char rw:1;
	unsigned char us:1;
	unsigned char pwt:1;
	unsigned char pcd:1;
	unsigned char a:1;
	unsigned char d:1;
	unsigned char reserved:1;
	unsigned char g:1;
	unsigned char avl:3;
	unsigned int pageTableBase:20;
} __attribute__ ((packed));
