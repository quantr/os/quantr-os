#ifndef __MATH_H__
#define __MATH_H__

#ifdef __cplusplus
extern "C" {
#endif
	
double exp(double x);
 /* exponential of x */
double log(double x);
/* natural logarithm of x */
double log10(double x);
/* base-10 logarithm of x */
double pow(double x, double y);
/* x raised to power y */
double sqrt(double x);
/* square root of x */
double ceil(double x);
/* smallest integer not less than x */
double floor(double x);
/* largest integer not greater than x */
double fabs(double x);
/* absolute value of x */
double ldexp(double x, int n);
/* x times 2 to the power n */
double frexp(double x, int* exp);
/* if x non-zero, returns value, with absolute value in interval [1/2, 1), and assigns to *exp integer such that product of return value and 2 raised to the power *exp equals x; if x zero, both return value and *exp are zero */
double modf(double x, double* ip);
/* returns fractional part and assigns to *ip integral part of x, both with same sign as x */
double fmod(double x, double y);
/* if y non-zero, floating-point remainder of x/y, with same sign as x; if y zero, result is implementation-defined */
double sin(double x);
/* sine of x */
double cos(double x);
/* cosine of x */
double tan(double x);
/* tangent of x */
double asin(double x);
/* arc-sine of x */
double acos(double x);
/* arc-cosine of x */
double atan(double x);
/* arc-tangent of x */
double atan2(double y, double x);
/* arc-tangent of y/x */
double sinh(double x);
/* hyperbolic sine of x */
double cosh(double x);
/* hyperbolic cosine of x */
double tanh(double x);
/* hyperbolic tangent of x */

unsigned int numberOfDigit(long long input);

#ifdef __cplusplus
}
#endif

#endif
