#ifndef ELF_H
#define	ELF_H

#include <string>
#include <gelf.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdint.h>
using namespace std;

#define	PRINT_FMT 		"    %-20s 0x%x\n"
#define	PRINT_FMT2 		"    %-20s 0x%x"
#define PRINT_FIELD(N) 	printf(PRINT_FMT ,#N, (uintmax_t) ehdr.N);
#define PRINT_FIELD2(N)	printf(PRINT_FMT2, #N, (uintmax_t) phdr.N);
#define NL()			printf("\n");

class Elf {
private:
    
public:
	string filename;

    Elf(const char *);
};

#endif	/* ELF_H */

