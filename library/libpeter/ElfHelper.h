#ifndef ELFHELPER_H
#define	ELFHELPER_H

#include "Elf.h"

class ElfHelper {
public:
	ElfHelper();
	ElfHelper(const ElfHelper& orig);
	virtual ~ElfHelper();
	static Elf openElf(const char *filename);
	
private:

};

#endif	/* ELFHELPER_H */

