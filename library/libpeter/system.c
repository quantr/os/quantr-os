#include "../../kernel/header.h"

#include <stdio.h>
#include "io.h"
#include "system.h"

unsigned int getMemorySize() {
	unsigned int *size;
	size = (unsigned int *) 0x20001;
	return (*size * 64 * 1024) + (16 * 1024 * 1024);
	//return 9;
}

cpuid_regs_t cpuid(int functionNumber) {
	cpuid_regs_t regs;
	__asm__ __volatile__
			("movl %4,%%eax;" "cpuid;" "movl %%eax,%0; movl %%ebx,%1; movl %%ecx,%2; movl %%edx,%3" : "=m"(regs.eax), "=m"(regs.ebx), "=m"(regs.ecx), "=m"(regs.edx)
			: "g"(functionNumber)
			: "%eax", "%ebx", "%ecx", "%edx");
	return regs;
}

/*
 void cpuid()
 {
 //cpuid_regs_t regs, regs_ext;
 //char idstr[13];

 /////////////////////////// cpuid /////////////////////////////
 unsigned int MaxEAX,eax,ebx,ecx,edx;
 __asm__ __volatile__("movl	$0,%%eax\n"
 "cpuid"
 : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
 );
 MaxEAX=eax;
 char *VendorSign=(char *)0xD005;
 VendorSign[0]=ebx;
 VendorSign[1]=ebx>>8;
 VendorSign[2]=ebx>>16;
 VendorSign[3]=ebx>>24;
 VendorSign[4]=edx;
 VendorSign[5]=edx>>8;
 VendorSign[6]=edx>>16;
 VendorSign[7]=edx>>24;
 VendorSign[8]=ecx;
 VendorSign[9]=ecx>>8;
 VendorSign[10]=ecx>>16;
 VendorSign[11]=ecx>>24;
 VendorSign[12]='\0';

 //print("CPUID : %s\n",VendorSign);

 char *Compl[32];
 if (strcmp(VendorSign, "GenuineIntel")==0){
 Compl[0]="FPU";
 Compl[1]="VME";
 Compl[2]="DE";
 Compl[3]="PSE";
 Compl[4]="TSC";
 Compl[5]="MSR";
 Compl[6]="PAE";
 Compl[7]="MCE";
 Compl[8]="CX8";
 Compl[9]="APIC";
 Compl[10]="";
 Compl[11]="SEP";
 Compl[12]="MTRR";
 Compl[13]="PGE";
 Compl[14]="MCA";
 Compl[15]="CMOV";
 Compl[16]="PAT";
 Compl[17]="PSE-36";
 Compl[18]="PSN";
 Compl[19]="CLFSH";
 Compl[20]="";
 Compl[21]="DS";
 Compl[22]="ACPI";
 Compl[23]="MMX";
 Compl[24]="FXSR";
 Compl[25]="SSE";
 Compl[26]="SSE2";
 Compl[27]="SS";
 Compl[28]="HTT";
 Compl[29]="TM";
 Compl[30]="IA-64";
 Compl[31]="";
 }else if (strcmp(VendorSign,"AuthenticAMD")==0){
 Compl[0]="FPU";
 Compl[1]="VME";
 Compl[2]="DE";
 Compl[3]="PSE";
 Compl[4]="TSC";
 Compl[5]="MSR";
 Compl[6]="PAE";
 Compl[7]="MCE";
 Compl[8]="CX8";
 Compl[9]="APIC";
 Compl[10]="";
 Compl[11]="SEP";
 Compl[12]="MTRR";
 Compl[13]="PGE";
 Compl[14]="MCA";
 Compl[15]="CMOV";
 Compl[16]="PAT";
 Compl[17]="PSE-36";
 Compl[18]="";
 Compl[19]="MPC";
 Compl[20]="";
 Compl[21]="";
 Compl[22]="MIE";
 Compl[23]="MMX";
 Compl[24]="FXSR";
 Compl[25]="SSE";
 Compl[26]="";
 Compl[27]="";
 Compl[28]="";
 Compl[29]="";
 Compl[30]="3DNowExt";
 Compl[31]="3DNow";
 }

 long REGEAX,REGEBX,REGEDX;
 int *dFamily=(int *)0xD012;
 int *dModel=(int *)0xD016;
 int *dStepping=(int *)0xD01a;
 int *dFamilyEx=(int *)0xD01e;
 int *dModelEx=(int *)0xD022;
 char *dType=(char *)0xD026;
 int dComplSupported[32];
 int *dBrand=(int *)0xD039;
 int *dCacheLineSize=(int *)0xD03d;
 int *dLogicalProcessorCount=(int *)0xD041;
 int *dLocalAPICID=(int *)0xD045;

 if (MaxEAX>=1){
 __asm__ __volatile(
 "movl    $1,%%eax\n"
 "cpuid\n"
 : "=a" (REGEAX), "=b" (REGEBX), "=d" (REGEDX)
 );
 *dFamily=((REGEAX>>8) & 0xf);
 *dModel=((REGEAX>4) & 0xf);
 *dStepping=(REGEAX & 0xf);

 *dFamilyEx=((REGEAX>>20) & 0xff);
 *dModelEx=((REGEAX>>16) & 0xf);

 switch(((REGEAX>>12) & 0x7)){
 case 0:
 strcpy(dType, "Original");
 break;
 case 1:
 strcpy(dType, "OverDrive");
 break;
 case 2:
 strcpy(dType, "Dual");
 break;
 }

 for (unsigned long C=1,Q=0;Q<32 ;C*=2, Q++){
 dComplSupported[Q]=(REGEDX & C)!=0?1:0;
 }

 *dBrand=REGEBX & 0xff;
 *dCacheLineSize=((strcmp(Compl[19], "CLFSH")==0) && (dComplSupported[19]==1))?((REGEBX>>8)&0xff)*8:-1;
 *dLogicalProcessorCount=((strcmp(Compl[28], "HTT")==0) && (dComplSupported[28]==1))?((REGEBX>>16)&0xff):-1;
 *dLocalAPICID=((REGEBX>>24)&0xff); //This only works on P4 or later, must check for that in the future

 //print("dType : %s\n",dType);
 //print("Family %d, Model %d, Stepping %d\n", dFamily, dModel, dStepping);
 //print("Extended Family %d, Extended Model %d\n",dFamilyEx, dModelEx);
 ////print("dComplSupported : %d\n",dComplSupported[0]);
 //print("Supported flags :\n");
 for (unsigned long Q=0;Q<27 ;Q++ ){
 if (dComplSupported[Q]){
 //print("Compl[%d] : %s , ",Q,Compl[Q]);
 }
 if (Q%3==0){
 //print("\n");
 }
 }
 //print("\n");
 //print("CacheLineSize: %d\n",dCacheLineSize);
 //print("Logical processor count: %d\n",dLogicalProcessorCount);
 //print("Local APIC ID: %d\n", dLocalAPICID);
 }

 /////////////////////////////// END CPUID ////////////////////////////
 }
 */

/*
 char *getCpuidVendorsign ()
 {
 return (char *) 0xD005;
 }

 int getCpuidFamily ()
 {
 int *temp = (int *) 0xD012;
 return *temp;
 }

 int getCpuidModel ()
 {
 int *temp = (int *) 0xD016;
 return *temp;
 }

 int getCpuidFamilyEx ()
 {
 int *temp = (int *) 0xD01E;
 return *temp;
 }

 int getCpuidModelEx ()
 {
 int *temp = (int *) 0xD022;
 return *temp;
 }

 int getCpuidStepping ()
 {
 int *temp = (int *) 0xD01a;
 return *temp;
 }

 int getCPUSpeed ()
 {
 int *temp = (int *) 0xD049;
 return *temp;
 }
 */

unsigned char getSecond() {
	unsigned char c;
	__asm__ __volatile__
			("L1:\n" "mov    $0x0a,%%al\n" "outb   %%al,$0x70\n" "inb    $0x71,%%al\n" "or     %%al,%%al\n" "js     L1\n" "mov    $0x0,%%al\n" "outb   %%al,$0x70\n" "inb    $0x71,%%al" : "=a"(c)
			);
	//outport(0x70,0x0);
	return convertBCDToInt(c);
}

unsigned char getMinute() {
	unsigned char c;

	__asm__ __volatile__
			("L2:\n" "mov	$0x0a,%%al\n" "outb	%%al,$0x70\n" "inb	$0x71,%%al\n" "or	%%al,%%al\n" "js	L2\n" "mov	$0x2,%%al\n" "outb	%%al,$0x70\n" "inb	$0x71,%%al" : "=a"(c)
			);
	//outport(0x70,0x2);
	return convertBCDToInt(c);
}

unsigned char getHour() {
	//outport (0x70, 0x4);
	unsigned char c;
	__asm__ __volatile__
			("L3:\n" "mov      $0x0a,%%al\n" "outb     %%al,$0x70\n" "inb      $0x71,%%al\n" "or       %%al,%%al\n" "js        L3\n" "mov      $0x4,%%al\n" "outb      %%al,$0x70\n" "inb      $0x71,%%al" : "=a"(c));
	return convertBCDToInt(c);
}

unsigned char getDay() {
	outport(0x70, 0x7);
	return convertBCDToInt(inport(0x71));
}

unsigned char getMonth() {
	outport(0x70, 0x8);
	return convertBCDToInt(inport(0x71));
}

unsigned char getYear() {
	outport(0x70, 0x9);
	return convertBCDToInt(inport(0x71));
}

void ltr(unsigned short descriptorNumber) {
	descriptorNumber = descriptorNumber << 3;
	__asm__ __volatile__("ltr     %%ax\n"::"a"(descriptorNumber));
}

//union uu {
//      long long       q;              /* as a (signed) quad */
//      unsigned long long      uq;             /* as an unsigned quad */
//      long            sl[2];          /* as two signed longs */
//      unsigned long           ul[2];          /* as two unsigned longs */
//};

/*int __cmpdi2(unsigned long long a, unsigned long long b){
 union uu aa, bb;

 aa.q = a;
 bb.q = b;
 return (aa.sl[1] < bb.sl[1] ? 0 : aa.sl[1] > bb.sl[1] ? 2 : aa.ul[0] < bb.ul[0] ? 0 : aa.ul[0] > bb.ul[0] ? 2 : 1);
 }*/

unsigned long long rdtsc() {
	unsigned long long x;
	__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
	return x;
}

/*void sleep(int seconds) {
	unsigned int currentSecond = getSecond() + getMinute() * 60;
	currentSecond += seconds;
	unsigned int destMinute = currentSecond / 60;
	unsigned int destSecond = currentSecond - (destMinute * 60);

	unsigned char nowMinute = getMinute();
	unsigned char nowSecond = getSecond();
	while (!(nowSecond == destSecond && nowMinute == destMinute)) {
		nowMinute = getMinute();
		nowSecond = getSecond();
		//print("%d:%d < %d:%d\n",nowMinute,nowSecond,destMinute,destSecond);
	}
}*/

unsigned long long getTimeOfDay() {
	//print("minute=%d, second=%d\n",getMinute(),getSecond());
	return getMinute() * 60 + getSecond();
}

unsigned int getFreePage(unsigned size) {
	char *freePageList = (char *) FREEPAGELIST;
	int numberOfPage = (size % 4096 == 0) ? size / 4096 : size / 4096 + 1;
	//for (int x = 0; x < 0x100000 - numberOfPage + 1; x++) {
	for (int x = 0; x < (KERNEL_RESERVED_MEMORY_END - KERNEL_RESERVED_MEMORY + 1) / 4096 - numberOfPage + 1; x++) {
		int ok = 1;
		for (int y = 0; y < numberOfPage; y++) {
			if (*(freePageList + x + y) == 1) {
				ok = 0;
			}
		}
		//outStringToDisplay("\n");
		if (ok) {
			outIntToDisplay(x, 10);
			outIntToDisplayLn(*(freePageList + x), 10);
			for (int y = 0; y < numberOfPage; y++) {
				*(freePageList + x + y) = 1;
			}
			//BOCHS_PAUSE
			return x * 4096 + KERNEL_RESERVED_MEMORY;
		}
	}
	while (1);
	return 0;
}

unsigned int getFreePageForUpper1GB(unsigned size) {
	char *freePageList = (char *) FREEPAGELIST_UPPER_1GB;
	int numberOfPage = (size % 4096 == 0) ? size / 4096 : size / 4096 + 1;
	//for (int x = 0; x < 0x100000 - numberOfPage + 1; x++) {
	int x;
	for (x = 0; x < (KERNEL_RESERVED_MEMORY_FOR_UPPER1GB_END - KERNEL_RESERVED_MEMORY_FOR_UPPER1GB + 1) / 4096 - numberOfPage + 1; x++) {
		int ok = 1;
		int y;
		for (y = 0; y < numberOfPage; y++) {
			if (*(freePageList + x + y) == 1) {
				ok = 0;
			}
		}
		if (ok) {
			for (y = 0; y < numberOfPage; y++) {
				*(freePageList + x + y) = 1;
			}
			return x * 4096 + KERNEL_RESERVED_MEMORY_FOR_UPPER1GB;
		}
	}
	printf("getFreePage(), run out getFreePageForUpper1GB\n");
	while (1);
	return 0;
}

unsigned int getPhysicalAddressByPD(struct Page_Directory *pageDirectories, int *linearAddress) {
	unsigned int PDselector = (unsigned int) linearAddress >> 22;
	unsigned int PTselector = ((unsigned int) linearAddress >> 12) & 0x3ff;
	struct Page_Directory *pageDirectory = &pageDirectories[PDselector];
	struct Page_Table *pageTables = (struct Page_Table *) (pageDirectory->pageTableBase << 12);
	//printf("         %x, %d, %d\n", pageTables, PDselector, PTselector);
	return pageTables[PTselector].pageTableBase << 12 + ((unsigned int) linearAddress & 0xfff);
}

unsigned int getPhysicalAddress(unsigned int linearAddress) {
	unsigned int PDselector = linearAddress >> 22;
	unsigned int PTselector = (linearAddress >> 12) & 0x3ff;
	struct Page_Table *pageTablesBase = (struct Page_Table *) KERNEL_PTs + PTselector + PDselector * 1024;
	return (pageTablesBase->pageTableBase << 12) + (linearAddress & 0xfff);
}

unsigned int getPageTableEntryAddress(struct Page_Directory *pageDirectories, unsigned int linearAddress) {
	unsigned int PDselector = linearAddress >> 22;
	unsigned int PTselector = (linearAddress >> 12) & 0x3ff;
	struct Page_Directory *pageDirectory = &pageDirectories[PDselector];
	struct Page_Table *pageTables = (struct Page_Table *) (pageDirectory->pageTableBase << 12);
	printf("                  ++ %x\n", &(pageTables[PTselector]));
	return getPhysicalAddress(&(pageTables[PTselector]));
}

unsigned int getFreeLinearAddress(unsigned size) {
	if (size % PAGE_SIZE != 0) {
		size = ((size / PAGE_SIZE) + 1) * PAGE_SIZE;
	}
	// check the size can divide by PAGE_SIZE or not, if no, make it can dividable by PAGE_SIZE
	if (size % PAGE_SIZE != 0) {
		size = ((size / PAGE_SIZE) + 1) * PAGE_SIZE;
	}
	// end check the size can divide by PAGE_SIZE or not, if no, make it can dividable by PAGE_SIZE
	struct VirutalAddressBlock *freeVirtualAddressList = (struct VirutalAddressBlock *) FREEVIRTUALADDRESSLIST;
	struct VirutalAddressBlock *usedVirtualAddressList = (struct VirutalAddressBlock *) USEDVIRTUALADDRESSLIST;

	unsigned int x;
	unsigned int y;

	for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
		if ((freeVirtualAddressList + x)->size >= size) {
			for (y = 0; y < 1024 * 1024 / sizeof (struct VirutalAddressBlock); y++) {
				if ((usedVirtualAddressList + y)->offset == 0 && (usedVirtualAddressList + y)->size == 0) {
					// Get the free virtual address
					unsigned int freeLinearAddress = (freeVirtualAddressList + x)->offset;
					// fill that free virtual address in usedVirtualAddressList
					(usedVirtualAddressList + y)->offset = freeLinearAddress;
					(usedVirtualAddressList + y)->size = size;

					// clean up freeVirtualAddressList
					(freeVirtualAddressList + x)->offset += size;
					(freeVirtualAddressList + x)->size -= size;

					return freeLinearAddress;
				}
			}
		}
	}
	return 0;
}

void freeLinearAddress(unsigned ptr) {
	if (ptr != NULL) {
		struct VirutalAddressBlock *freeVirtualAddressList = (struct VirutalAddressBlock *) FREEVIRTUALADDRESSLIST;
		struct VirutalAddressBlock *usedVirtualAddressList = (struct VirutalAddressBlock *) USEDVIRTUALADDRESSLIST;

		unsigned int originalSize = 0;

		// clear the record in freePageList
		char *freePageList = (char *) FREEPAGELIST;
		unsigned int x;
		for (x = 0; x < getNumberOfPage(ptr); x++) {
			freePageList[(getPhysicallAddress(ptr) - KERNEL_RESERVED_MEMORY) / PAGE_SIZE + x] = 0;
		}

		// clear the record in usedVirtualAddressList
		for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
			if ((usedVirtualAddressList + x)->offset == (int) ptr) {
				(usedVirtualAddressList + x)->offset = 0;
				originalSize = (usedVirtualAddressList + x)->size;
				(usedVirtualAddressList + x)->size = 0;

				// move the back record to front
				unsigned int index1;
				for (index1 = x; index1 < (1024 * 1024 / sizeof (struct VirutalAddressBlock)) - 1; index1++) {
					(usedVirtualAddressList + index1)->offset = (usedVirtualAddressList + index1 + 1)->offset;
					(usedVirtualAddressList + index1)->size = (usedVirtualAddressList + index1 + 1)->size;
					if ((usedVirtualAddressList + index1 + 1)->offset == 0) {
						goto SORT_THE_FREEVIRTUALADDRESSLIST;
					}
				}
				// end clear the zero of usedVirtualAddressList
			}
		}
SORT_THE_FREEVIRTUALADDRESSLIST:
		// re-add the linear address in freeVirtualAddressList
		//    print("sort the freeVirtualAddressList\n");

		for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
			if ((freeVirtualAddressList + x)->offset == 0) {
				(freeVirtualAddressList + x)->offset = (int) ptr;
				(freeVirtualAddressList + x)->size = originalSize;
				// sort freeVirtualAddressList
				struct VirutalAddressBlock temp;
				unsigned int i;
				for (i = 0; i <= x; i++) {
					unsigned int j;
					for (j = 1; j <= x; j++) {
						if ((freeVirtualAddressList + j - 1)->offset > (freeVirtualAddressList + j)->offset && (freeVirtualAddressList + j)->offset != 0) {
							temp.offset = (freeVirtualAddressList + j - 1)->offset;
							temp.size = (freeVirtualAddressList + j - 1)->size;
							(freeVirtualAddressList + j - 1)->offset = (freeVirtualAddressList + j)->offset;
							(freeVirtualAddressList + j - 1)->size = (freeVirtualAddressList + j)->size;
							(freeVirtualAddressList + j)->offset = temp.offset;
							(freeVirtualAddressList + j)->size = temp.size;
						}
					}
				}

				break;
				// end sort freeVirtualAddressList
			}
		}
		//    print("end sort the freeVirtualAddressList\n");

		// merge continuous address
		//    print("merge continuous address\n");
		unsigned int i;
		for (i = 1; i < 1024 * 1024 / sizeof (struct VirutalAddressBlock); i++) {
			if ((freeVirtualAddressList + i)->offset == 0) {
				break;
			} else {
				if ((freeVirtualAddressList + i - 1)->offset + (freeVirtualAddressList + i - 1)->size == (freeVirtualAddressList + i)->offset) {
					// merge two records
					(freeVirtualAddressList + i)->offset = (freeVirtualAddressList + i - 1)->offset;
					(freeVirtualAddressList + i)->size += (freeVirtualAddressList + i - 1)->size;

					// clear the merged one
					(freeVirtualAddressList + i - 1)->offset = 0;
					(freeVirtualAddressList + i - 1)->size = 0;

					// switch all the record to left
					unsigned int j;
					for (j = 1; j < 1024 * 1024 / sizeof (struct VirutalAddressBlock); j++) {
						(freeVirtualAddressList + j - 1)->offset = (freeVirtualAddressList + j)->offset;
						(freeVirtualAddressList + j - 1)->size = (freeVirtualAddressList + j)->size;
					}
					i -= 1;
				}
			}
		}
	}
}

void *kmalloc(unsigned size) {
//	outStringToDisplay("   size=", 14);
//	outIntToDisplayLn(size, 13);
	// check the size can divide by PAGE_SIZE or not, if no, make it can dividable by PAGE_SIZE
	if (size % PAGE_SIZE != 0) {
		size = ((size / PAGE_SIZE) + 1) * PAGE_SIZE;
	}
	// end check the size can divide by PAGE_SIZE or not, if no, make it can dividable by PAGE_SIZE
	struct VirutalAddressBlock *freeVirtualAddressList = (struct VirutalAddressBlock *) FREEVIRTUALADDRESSLIST;
	struct VirutalAddressBlock *usedVirtualAddressList = (struct VirutalAddressBlock *) USEDVIRTUALADDRESSLIST;

	unsigned int x;
	unsigned int y;
	for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
		if ((freeVirtualAddressList + x)->size >= size) {
			for (y = 0; y < 1024 * 1024 / sizeof (struct VirutalAddressBlock); y++) {
				if ((usedVirtualAddressList + y)->offset == 0 && (usedVirtualAddressList + y)->size == 0) {

					// Get the free virtual address
					unsigned int freeLinearAddress = (freeVirtualAddressList + x)->offset;
//					outStringToDisplay("   freeLinearAddress=", 14);
//					outIntToDisplayLn(freeLinearAddress, 13);
//					outStringToDisplay("   (freeVirtualAddressList + x)->size=", 14);
//					outIntToDisplayLn((freeVirtualAddressList + x)->size, 12);
					// fill that free virtual address in usedVirtualAddressList
					(usedVirtualAddressList + y)->offset = freeLinearAddress;
					(usedVirtualAddressList + y)->size = size;
					//					outStringToDisplay("   (usedVirtualAddressList + y)->offset=", 14);
					//					outIntToDisplayLn(&((usedVirtualAddressList + y)->offset), 13);


					/*unsigned int PDselector = freeLinearAddress >> 22;
					//unsigned int PTselector = (freeLinearAddress >> 12) & 0x1000;
					unsigned int PTselector = (freeLinearAddress >> 12) & 0x3ff;

					// All the kernel's Page Directory is already linked to corresponding page table, so we don't need to create page directory, we just ended to fill in the free physical address into the page tables
					unsigned int freeContinuousPhysicalAddress = getFreePage(size);
					//print("freePhysicalAddress=%x\n", freeContinuousPhysicalAddress);
					outStringToDisplay("   freeContinuousPhysicalAddress=", 14);
					outIntToDisplayLn(freeContinuousPhysicalAddress, 13);

					//Page_Directory *pageDirectories=(Page_Directory *)0x1100000;
					unsigned int numberOfPage = (size % 4096 == 0) ? size / 4096 : size / 4096 + 1;
					//print("numberOfPage=%d\n", numberOfPage);
					//unsigned int freePhysicalAddresses[numberOfPage];
					struct Page_Table *pageTablesBase = (struct Page_Table *) KERNEL_PTs + PTselector + PDselector * 1024;
					unsigned int pageNo;
					outStringToDisplay("   numberOfPage=", 14);
					outIntToDisplayLn(numberOfPage, 13);
					for (pageNo = 0; pageNo < numberOfPage; pageNo++) {
						pageTablesBase->pageTableBase = (freeContinuousPhysicalAddress + (pageNo * 4096)) >> 12;
						pageTablesBase++;
					}*/

					//unsigned int lastPDselector=99999999;
					unsigned int numberOfPage = (size % 4096 == 0) ? size / 4096 : size / 4096 + 1;
					unsigned int freeContinuousPhysicalAddress = getFreePage(size);
					for (unsigned int pageNo = 0; pageNo < numberOfPage; pageNo++) {
						unsigned int linearAddress = freeLinearAddress + (pageNo * 4096);
						unsigned int PDselector = linearAddress >> 22;
						unsigned int PTselector = (linearAddress >> 12) & 0x3ff;
						outStringToDisplay("   >> linearAddress=", 10);
						outIntToDisplayLn(linearAddress, 9);
						outStringToDisplay("   PDselector=", 10);
						outIntToDisplayLn(PDselector, 9);
						outStringToDisplay("   PTselector=", 10);
						outIntToDisplayLn(PTselector, 9);
						//if (PDselector!=lastPDselector){
						struct Page_Table *pageTablesBase = (struct Page_Table *) KERNEL_PTs + PTselector + PDselector * 1024;
						
						outStringToDisplay("   pageTablesBase=", 10);
						outIntToDisplayLn(pageTablesBase, 9);
						//lastPDselector=PDselector;
						//}
						
						outStringToDisplay("   f=", 10);
						outIntToDisplayLn((freeContinuousPhysicalAddress + (pageNo * 4096)) >> 12, 9);
						
						pageTablesBase->pageTableBase = (freeContinuousPhysicalAddress + (pageNo * 4096)) >> 12;
					}


					// clean up freeVirtualAddressList
					(freeVirtualAddressList + x)->offset += size;
					(freeVirtualAddressList + x)->size -= size;

					// sometime all the size will used up, that mean the (freeVirtualAddressList + x)->size become zero,
					//then we drop this record and move the back to the front, look at the previous line "(freeVirtualAddressList + x)->size -= size", this cause the (freeVirtualAddressList + x)->size become zero;
					if ((freeVirtualAddressList + x)->size == 0) {
						unsigned int z;
						for (z = x + 1; z < 1024 * 1024 / sizeof (struct VirutalAddressBlock); z++) {
							if ((freeVirtualAddressList + z)->size == 0) {
								break;
							} else {
								(freeVirtualAddressList + z - 1)->offset = (freeVirtualAddressList + z)->offset;
								(freeVirtualAddressList + z - 1)->size = (freeVirtualAddressList + z)->size;
								(freeVirtualAddressList + z)->offset = 0;
								(freeVirtualAddressList + z)->size = 0;
							}
						}
					}
					// end clean up freeVirtualAddressList

					/*print("freeLinearAddress=%x\n", freeLinearAddress);
					 print("freeContinuousPhysicalAddress=%x\n", freeContinuousPhysicalAddress);
					 print("PDselector=%x\n", PDselector);
					 print("PTselector=%x\n", PTselector);
					 print("numberOfPage=%d\n", numberOfPage);
					 print("size=%d\n", size); */

					__native_flush_tlb_single(freeLinearAddress);

					//outStringToDisplay("   fuck end\n", 14);
					return (void *) freeLinearAddress;
				}
			}
			printf("run out usedVirtualAddressList\n");
			while (1);
		}
	}
	printf("kmalloc() error\n");
	while (1)
		;
	return (void *) NULL;
}

void kfree(void *ptr) {
	if (ptr != NULL) {
		struct VirutalAddressBlock *freeVirtualAddressList = (struct VirutalAddressBlock *) FREEVIRTUALADDRESSLIST;
		struct VirutalAddressBlock *usedVirtualAddressList = (struct VirutalAddressBlock *) USEDVIRTUALADDRESSLIST;

		unsigned int originalSize = 0;

		// clear the record in freePageList
		char *freePageList = (char *) FREEPAGELIST;
		unsigned int x;
		for (x = 0; x < getNumberOfPage(ptr); x++) {
			freePageList[(getPhysicallAddress(ptr) - KERNEL_RESERVED_MEMORY) / PAGE_SIZE + x] = 0;
		}

		// set zero to the address which *ptr is pointed to
		unsigned int PDselector = ((unsigned int) ptr) >> 22;
		unsigned int PTselector = (((unsigned int) ptr) >> 12) & 0x3ff;
		unsigned int numberOfPage = getNumberOfPage(ptr);
		struct Page_Table *pageTablesBase = (struct Page_Table *) KERNEL_PTs + PTselector + PDselector * 1024;
		unsigned int pageNo;
		for (pageNo = 0; pageNo < numberOfPage; pageNo++) {
			pageTablesBase->pageTableBase = 0;
			pageTablesBase++;
		}

		// clear the record in usedVirtualAddressList
		for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
			//printf("kfree %x\n", x);
			if ((usedVirtualAddressList + x)->offset == (int) ptr) {
				(usedVirtualAddressList + x)->offset = 0;
				originalSize = (usedVirtualAddressList + x)->size;
				(usedVirtualAddressList + x)->size = 0;

				// move the back record to front
				unsigned int index1;
				for (index1 = x; index1 < (1024 * 1024 / sizeof (struct VirutalAddressBlock)) - 1; index1++) {
					//printf("index1=%d\n", x);
					(usedVirtualAddressList + index1)->offset = (usedVirtualAddressList + index1 + 1)->offset;
					(usedVirtualAddressList + index1)->size = (usedVirtualAddressList + index1 + 1)->size;
					if ((usedVirtualAddressList + index1 + 1)->offset == 0) {
						goto SORT_THE_FREEVIRTUALADDRESSLIST;
					}
				}
				// end clear the zero of usedVirtualAddressList
			}
		}
SORT_THE_FREEVIRTUALADDRESSLIST:
		// re-add the linear address in freeVirtualAddressList
		//    print("sort the freeVirtualAddressList\n");

		for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
			//printf("x=%d\n", x);
			if ((freeVirtualAddressList + x)->offset == 0) {
				(freeVirtualAddressList + x)->offset = (int) ptr;
				(freeVirtualAddressList + x)->size = originalSize;
				// sort freeVirtualAddressList
				struct VirutalAddressBlock temp;
				unsigned int i;
				for (i = 0; i <= x; i++) {
					unsigned int j;
					for (j = 1; j <= x; j++) {
						if ((freeVirtualAddressList + j - 1)->offset > (freeVirtualAddressList + j)->offset && (freeVirtualAddressList + j)->offset != 0) {
							temp.offset = (freeVirtualAddressList + j - 1)->offset;
							temp.size = (freeVirtualAddressList + j - 1)->size;
							(freeVirtualAddressList + j - 1)->offset = (freeVirtualAddressList + j)->offset;
							(freeVirtualAddressList + j - 1)->size = (freeVirtualAddressList + j)->size;
							(freeVirtualAddressList + j)->offset = temp.offset;
							(freeVirtualAddressList + j)->size = temp.size;
						}
					}
				}

				break;
				// end sort freeVirtualAddressList
			}
		}
		//    print("end sort the freeVirtualAddressList\n");
		// merge continuous address
		//    print("merge continuous address\n");
		unsigned int i;
		for (i = 1; i < 1024 * 1024 / sizeof (struct VirutalAddressBlock); i++) {
			//printf("i=%d, %x\n", i, (freeVirtualAddressList + i));
			if ((freeVirtualAddressList + i)->offset == 0) {
				break;
			} else {
				if ((freeVirtualAddressList + i - 1)->offset + (freeVirtualAddressList + i - 1)->size == (freeVirtualAddressList + i)->offset) {
					// merge two records
					(freeVirtualAddressList + i)->offset = (freeVirtualAddressList + i - 1)->offset;
					(freeVirtualAddressList + i)->size += (freeVirtualAddressList + i - 1)->size;

					// clear the merged one
					(freeVirtualAddressList + i - 1)->offset = 0;
					(freeVirtualAddressList + i - 1)->size = 0;

					// switch all the record to left
					unsigned int j;
					for (j = 1; j < 1024 * 1024 / sizeof (struct VirutalAddressBlock); j++) {
						(freeVirtualAddressList + j - 1)->offset = (freeVirtualAddressList + j)->offset;
						(freeVirtualAddressList + j - 1)->size = (freeVirtualAddressList + j)->size;
					}
					i -= 1;
				}
			}
		}
	}
}

//void *kmalloc(unsigned int virtualAddress, unsigned size) {
//	// check the size can divide by PAGE_SIZE or not, if no, make it can divie by PAGE_SIZE
//	if (size % PAGE_SIZE != 0) {
//		size = ((size / PAGE_SIZE) + 1) * PAGE_SIZE;
//	}
//	// end check the size can divide by PAGE_SIZE or not, if no, make it can divie by PAGE_SIZE
//	VirutalAddressBlock *freeVirtualAddressList =
//			(VirutalAddressBlock *) FREEVIRTUALADDRESSLIST;
//	VirutalAddressBlock *usedVirtualAddressList =
//			(VirutalAddressBlock *) USEDVIRTUALADDRESSLIST;
//
//	// check the requested virtual address is avaliable or not
//	bool isVirutalAddressOK=false;
//	unsigned int originalFreeVirtualAddressIndex;
//	for (unsigned int x = 0; x < 1024 * 1024 / sizeof(VirutalAddressBlock); x++) {
//		if ((freeVirtualAddressList + x)->offset<=virtualAddress && ((freeVirtualAddressList + x)->offset+(freeVirtualAddressList + x)->size)<=virtualAddress+size) {
//			originalFreeVirtualAddressIndex=x;
//			isVirutalAddressOK=true;
//			break;
//		}
//	}
//	// end check the requested virtual address is avaliable or not
//
//	if (isVirutalAddressOK) {
//		for (unsigned int y = 0; y < 1024 * 1024 / sizeof(VirutalAddressBlock); y++) {
//			if ((usedVirtualAddressList + y)->offset == 0 && (usedVirtualAddressList + y)->size == 0) {
//				// Get the free virtual address
//				// fill that free virtual address in usedVirtualAddressList
//				(usedVirtualAddressList + y)->offset = virtualAddress;
//				(usedVirtualAddressList + y)->size = size;
//
//				unsigned int PDselector = virtualAddress >> 22;
//				//unsigned int PTselector = (freeLinearAddress >> 12) & 0x1000;
//				unsigned int PTselector = (virtualAddress >> 12) & 0x3ff;
//
//				// All the kernel's Page Directory is already linked to corresponding page table, so we don't need to create page directory, we just ended to fill in the free physical address into the page tables
//				unsigned int freeContinuousPhysicalAddress = getFreePage(size);
//				printf("freePhysicalAddress=%x\n", freeContinuousPhysicalAddress);
//
//				//Page_Directory *pageDirectories=(Page_Directory *)0x1100000;
//				unsigned int numberOfPage = (size % 4096 == 0) ? size / 4096
//						: size / 4096 + 1;
//				//printf("numberOfPage=%d\n", numberOfPage);
//				//unsigned int freePhysicalAddresses[numberOfPage];
//				Page_Table *pageTablesBase = (Page_Table *) KERNEL_PTs
//						+ PTselector + PDselector * 1024;
//				for (unsigned int pageNo = 0; pageNo < numberOfPage; pageNo++) {
//					pageTablesBase->pageTableBase
//							= (freeContinuousPhysicalAddress + (pageNo * 4096))
//									>> 12;
//					pageTablesBase++;
//				}
//
//				// clean up freeVirtualAddressList
//				int
//						originalOffset=
//								(freeVirtualAddressList + originalFreeVirtualAddressIndex)->offset;
//				int
//						originalSize=
//								(freeVirtualAddressList + originalFreeVirtualAddressIndex)->size;
//				if (virtualAddress
//						-(freeVirtualAddressList + originalFreeVirtualAddressIndex)->offset
//						>=0) {
//					// previous block
//					(freeVirtualAddressList + originalFreeVirtualAddressIndex)->size
//							=virtualAddress
//									-(freeVirtualAddressList + originalFreeVirtualAddressIndex)->offset;
//				}
//
//				if ((virtualAddress+size)>(originalOffset+originalSize)) {
//					// next block
//					(freeVirtualAddressList + originalFreeVirtualAddressIndex)->offset
//							=virtualAddress+size;
//					(freeVirtualAddressList + originalFreeVirtualAddressIndex)->size
//							=originalSize
//									-(freeVirtualAddressList + originalFreeVirtualAddressIndex)->offset;
//				}
//
//				//(freeVirtualAddressList + x)->offset += size;
//				//(freeVirtualAddressList + x)->size -= size;
//
//				// sometime all the size will used up, that mean the (freeVirtualAddressList + x)->size become zero,
//				//then we drop this record and move the back to the front
//				if ((freeVirtualAddressList + originalFreeVirtualAddressIndex)->size
//						== 0) {
//					for (unsigned int z = originalFreeVirtualAddressIndex + 1; z
//							< 1024 * 1024 / sizeof(VirutalAddressBlock); z++) {
//						if ((freeVirtualAddressList + z)->size == 0) {
//							break;
//						} else {
//							(freeVirtualAddressList + z - 1)->offset = (freeVirtualAddressList + z)->offset;
//							(freeVirtualAddressList + z - 1)->size = (freeVirtualAddressList + z)->size;
//							(freeVirtualAddressList + z)->offset = 0;
//							(freeVirtualAddressList + z)->size = 0;
//						}
//					}
//				}
//				// end clean up freeVirtualAddressList
//
//				/*printf("freeLinearAddress=%x\n", freeLinearAddress);
//				 printf("freeContinuousPhysicalAddress=%x\n", freeContinuousPhysicalAddress);
//				 printf("PDselector=%x\n", PDselector);
//				 printf("PTselector=%x\n", PTselector);
//				 printf("numberOfPage=%d\n", numberOfPage);
//				 printf("size=%d\n", size); */
//
//				return (void *) virtualAddress;
//			}
//		}
//		printf("run out usedVirtualAddressList\n");
//	}
//	printf("kmalloc() error\n");
//	while (1)
//		;
//	return (void *) NULL;
//}
//

void *kmalloc2(unsigned int virtualAddress, unsigned int physicalAddress, unsigned size) {
	unsigned int PDselector = virtualAddress >> 22;
	unsigned int PTselector = (virtualAddress >> 12) & 0x3ff;

	// All the kernel's Page Directory is already linked to corresponding page table, so we don't need to create page directory, we just ended to fill in the free physical address into the page tables
	unsigned int freeContinuousPhysicalAddress = physicalAddress;

	//Page_Directory *pageDirectories=(Page_Directory *)0x1100000;
	unsigned int numberOfPage = (size % 4096 == 0) ? size / 4096 : size / 4096 + 1;
	struct Page_Table *pageTablesBase = (struct Page_Table *) KERNEL_PTs + PTselector + PDselector * 1024;
	unsigned int pageNo;
	for (pageNo = 0; pageNo < numberOfPage; pageNo++) {
		pageTablesBase->pageTableBase = (freeContinuousPhysicalAddress + (pageNo * 4096)) >> 12;
		pageTablesBase++;
	}
	return (void *) virtualAddress;
}

void *kmalloc_upper1GB(unsigned size) {
	// check the size can divide by PAGE_SIZE or not, if no, make it can divie by PAGE_SIZE
	if (size % PAGE_SIZE != 0) {
		size = ((size / PAGE_SIZE) + 1) * PAGE_SIZE;
	}
	// end check the size can divide by PAGE_SIZE or not, if no, make it can divie by PAGE_SIZE
	struct VirutalAddressBlock *freeVirtualAddressList_upper1GB = (struct VirutalAddressBlock *) FREEVIRTUALADDRESSLIST_UPPER1GB;
	struct VirutalAddressBlock *usedVirtualAddressList_upper1GB = (struct VirutalAddressBlock *) USEDVIRTUALADDRESSLIST_UPPER1GB;

	unsigned int x;
	unsigned int y;
	for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
		if ((freeVirtualAddressList_upper1GB + x)->size >= size) {
			for (y = 0; y < 1024 * 1024 / sizeof (struct VirutalAddressBlock); y++) {
				if ((usedVirtualAddressList_upper1GB + y)->offset == 0
						&& (usedVirtualAddressList_upper1GB + y)->size == 0) {
					// Get the free virtual address
					unsigned int freeLinearAddress = (freeVirtualAddressList_upper1GB + x)->offset;
					// fill that free virtual address in usedVirtualAddressList_upper1GB
					(usedVirtualAddressList_upper1GB + y)->offset = freeLinearAddress;
					(usedVirtualAddressList_upper1GB + y)->size = size;

					unsigned int PDselector = freeLinearAddress >> 22;
					//unsigned int PTselector = (freeLinearAddress >> 12) & 0x1000;
					unsigned int PTselector = (freeLinearAddress >> 12) & 0x3ff;

					// All the kernel's Page Directory is already linked to corresponding page table, so we don't need to create page directory,
					// we just need to fill in the free physical address into the page tables
					unsigned int freeContinuousPhysicalAddress = getFreePageForUpper1GB(size);

					//Page_Directory *pageDirectories=(Page_Directory *)0x1100000;

					unsigned int numberOfPage = (size % 4096 == 0) ? size / 4096 : size / 4096 + 1;
					//printf("numberOfPage=%d\n", numberOfPage);
					//unsigned int freePhysicalAddresses[numberOfPage];
					struct Page_Table *pageTablesBase = (struct Page_Table *) KERNEL_PTs + PTselector + PDselector * 1024;

					// try 2008/10/24
					// Map Page table address to the corresponding page directory
					//printf("freeContinuousPhysicalAddress=%x\n",freeContinuousPhysicalAddress);
					//printf("PDselector=%x\n",PDselector);
					//printf("pageTablesBase=%x\n",pageTablesBase);

					struct Page_Directory *pageDirectories = (struct Page_Directory *) KERNEL_PDs;
					//printf("pageDirectories[PDselector]=%x\n",&pageDirectories[PDselector]);
					pageDirectories[PDselector].p = 1;
					pageDirectories[PDselector].rw = 1;
					pageDirectories[PDselector].us = 0;
					pageDirectories[PDselector].pwt = 0;
					pageDirectories[PDselector].pcd = 0;
					pageDirectories[PDselector].a = 0;
					pageDirectories[PDselector].d = 0;
					pageDirectories[PDselector].reserved = 0;
					pageDirectories[PDselector].g = 0;
					pageDirectories[PDselector].avl = 0;
					pageDirectories[PDselector].pageTableBase = ((unsigned int) pageTablesBase) >> 12;
					// end 2008/10/24

					unsigned int pageNo;
					for (pageNo = 0; pageNo < numberOfPage; pageNo++) {
						pageTablesBase->p = 1;
						pageTablesBase->rw = 1;
						pageTablesBase->us = 0;
						pageTablesBase->pwt = 0;
						pageTablesBase->pcd = 0;
						pageTablesBase->a = 0;
						pageTablesBase->d = 0;
						pageTablesBase->pat = 0;
						pageTablesBase->g = 0;
						pageTablesBase->avl = 0;
						pageTablesBase->pageTableBase = (freeContinuousPhysicalAddress + (pageNo * 4096)) >> 12;
						pageTablesBase++;
					}

					// clean up freeVirtualAddressList_upper1GB
					(freeVirtualAddressList_upper1GB + x)->offset += size;
					(freeVirtualAddressList_upper1GB + x)->size -= size;
					// sometime all the size will used up, that mean the (freeVirtualAddressList_upper1GB + x)->size become zero,
					//then we drop this record and move the back to the front
					if ((freeVirtualAddressList_upper1GB + x)->size == 0) {
						unsigned int z;
						for (z = x + 1; z < 1024 * 1024 / sizeof (struct VirutalAddressBlock); z++) {
							if ((freeVirtualAddressList_upper1GB + z)->size == 0) {
								break;
							} else {
								(freeVirtualAddressList_upper1GB + z - 1)->offset = (freeVirtualAddressList_upper1GB + z)->offset;
								(freeVirtualAddressList_upper1GB + z - 1)->size = (freeVirtualAddressList_upper1GB + z)->size;
								(freeVirtualAddressList_upper1GB + z)->offset = 0;
								(freeVirtualAddressList_upper1GB + z)->size = 0;
							}
						}
					}
					// end clean up freeVirtualAddressList_upper1GB

					/*printf("freeLinearAddress=%x\n", freeLinearAddress);
					 printf("freeContinuousPhysicalAddress=%x\n", freeContinuousPhysicalAddress);
					 printf("PDselector=%x\n", PDselector);
					 printf("PTselector=%x\n", PTselector);
					 printf("numberOfPage=%d\n", numberOfPage);
					 printf("size=%d\n", size); */
					return (void *) freeLinearAddress;
				}
			}
			printf("run out usedVirtualAddressList_upper1GB\n");
			while (1)
				;
		}
	}
	printf("kmalloc_upper1GB() error\n");
	while (1)
		;
	return (void *) NULL;
}

unsigned int getPhysicallAddress(void *virtualAddress) {
	unsigned int PDselector = ((unsigned int) virtualAddress) >> 22;
	unsigned int PTselector = (((unsigned int) virtualAddress) >> 12) & 0x3ff;
	struct Page_Table *pageTablesBase = (struct Page_Table *) KERNEL_PTs + PTselector + PDselector * 1024;
	return pageTablesBase->pageTableBase << 12;
}

unsigned int getNumberOfPage(void *virtualAddress) {
	struct VirutalAddressBlock *usedVirtualAddressList =
			(struct VirutalAddressBlock *) 0x1900000;
	unsigned int x;
	for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
		if ((usedVirtualAddressList + x)->offset == (int) virtualAddress) {
			return ((usedVirtualAddressList + x)->size % 4096 == 0) ? (usedVirtualAddressList + x)->size / 4096 : (usedVirtualAddressList + x)->size / 4096 + 1;
		}
	}
	return 0;
}

void setDescriptor(struct Descriptor *descriptor, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType) {
	descriptor->byte0 = segmentLimit;
	descriptor->byte1 = segmentLimit >> 8;
	descriptor->byte2 = baseAddress;
	descriptor->byte3 = baseAddress >> 8;
	descriptor->byte4 = baseAddress >> 16;
	descriptor->byte5 = segmentType + (S << 4) + (DPL << 5) + (P << 7);
	descriptor->byte6 = (G << 7) + (D << 6) + ((segmentLimit >> 16) & 0xf);
	descriptor->byte7 = baseAddress >> 24;
}

//void setLDTDescriptor(struct Descriptor *descriptor, unsigned int baseAddress,
//		unsigned int segmentLimit, unsigned char G, unsigned char D,
//		unsigned char reserved0, unsigned char AVL, unsigned char P,
//		unsigned char DPL, unsigned char S, unsigned char segmentType) {
//	descriptor->byte0 = segmentLimit;
//	descriptor->byte1 = segmentLimit >> 8;
//	descriptor->byte2 = baseAddress;
//	descriptor->byte3 = baseAddress >> 8;
//	descriptor->byte4 = baseAddress >> 16;
//	descriptor->byte5 = segmentType + (S << 4) + (DPL << 5) + (P << 7);
//	descriptor->byte6 = (G << 7) + (D << 6) + ((segmentLimit >> 16) & 0xf);
//	descriptor->byte7 = baseAddress >> 24;
//}

void setGDTR(char *gdt, unsigned short numberOfGDTDescriptor) {
	if (numberOfGDTDescriptor < 8192) {
		char *gdtr = kmalloc(6);
		if (numberOfGDTDescriptor == 0) {
			*(unsigned short *) gdtr = (unsigned short) 0; //gdtr's limit is 8N-1
		} else {
			*(unsigned short *) gdtr = (unsigned short) (8 * numberOfGDTDescriptor) - 1; //gdtr's limit is 8N-1
		}
		*(unsigned int *) (gdtr + 2) = gdt;
		//printf("gdtr=%x\n",gdtr);
		__asm__ __volatile__("lgdt (%%eax)"::"a"(gdtr));
		free(gdtr);
	}
}

struct Descriptor * getGDT() {
	char GDTRAddress[6];
	__asm__ __volatile__("sgdt %0" : "=m" (GDTRAddress));
	int *y = (int *) (GDTRAddress + 2);
	struct Descriptor * gdt = (struct Descriptor *) *y;
	return gdt;
}

int getGDTLimit() {
	char GDTRAddress[6];
	__asm__ __volatile__("sgdt %0" : "=m" (GDTRAddress));
	int x;
	short *y = (short *) GDTRAddress;
	return *y;
}

int getNumberOfGDTDescriptor() {
	/*
	// below is not working is second GDT is a null descriptor
	struct Descriptor *gdt = getGDT();
	int x;
	printf("gdt=%x\n", gdt);
	for (x = 1; x < 8192; x++) {
		//printf("gdt[%d]=%x\n", x, &gdt[x]);
		if (x<3){
			printf("%x, %x,  %x %x %x %x\n", &gdt[x].byte0, &gdt[x].byte1, gdt[x].byte0, gdt[x].byte1, gdt[x].byte2, gdt[x].byte3);
			__asm __volatile__("XCHG %BX, %BX");
		}
		if (gdt[x].byte0 == 0 && gdt[x].byte1 == 0 && gdt[x].byte2 == 0
				&& gdt[x].byte3 == 0 && gdt[x].byte4 == 0 && gdt[x].byte5 == 0
				&& gdt[x].byte6 == 0 && gdt[x].byte7 == 0) {
			printf("return %d\n",x);
			return x;
		}
	}
	return 0;
	 */
	return (getGDTLimit() + 1) / 8;
}

void setIDTDescriptor(unsigned char *idtDescriptor, unsigned long offset, unsigned short selector, unsigned char type, unsigned char DPL, int P) {
	idtDescriptor[0] = offset;
	idtDescriptor[1] = offset >> 8;
	idtDescriptor[2] = selector;
	idtDescriptor[3] = selector >> 8;
	idtDescriptor[4] = 0;
	idtDescriptor[5] = type;
	DPL &= 0x3;
	idtDescriptor[5] |= DPL << 5;
	if (P) {
		idtDescriptor[5] |= 0x80;
	} else {
		idtDescriptor[5] |= 0x0;
	}
	idtDescriptor[6] = offset >> 16;
	idtDescriptor[7] = offset >> 24;
}

void setIDTTaskGateDescriptor(unsigned char *idtDescriptor, unsigned short selector, unsigned char DPL, int P) {
	idtDescriptor[0] = 0;
	idtDescriptor[1] = 0;
	idtDescriptor[2] = selector << 3;
	idtDescriptor[3] = (selector << 3) >> 8;
	idtDescriptor[4] = 0;
	idtDescriptor[5] = 0x5;
	DPL &= 0x3;
	idtDescriptor[5] |= DPL << 5;
	if (P) {
		idtDescriptor[5] |= 0x80;
	} else {
		idtDescriptor[5] |= 0x0;
	}
	idtDescriptor[6] = 0;
	idtDescriptor[7] = 0;
}

