#include "../../kernel/header.h"
#include "io.h"
#include <stdio.h>
#include <math.h>

//volatile unsigned int keyboardBufferTail = 0;

int numberOfKeyHasPressed = 0;

unsigned short getDriveType() {
	char *temp = (char *) DRIVE_TYPE;
	return (unsigned short) *temp;
}

unsigned short getMaxCylinder() {
	/*unsigned short *temp = (unsigned short *) 0x26970;*/
	unsigned short *temp = (unsigned short *) MAX_CYLINDER;
	return *temp;
}

unsigned char getMaxHead() {
	unsigned char *temp = (unsigned char *) MAX_HEAD;
	return *temp;
}

unsigned char getMaxSector() {
	char *temp = (char *) MAX_SECTOR;
	return *temp;
}

unsigned int readSector(unsigned long partitionOffset,
		unsigned long driveNumber, unsigned long lba, unsigned char *data,
		int limit) {
	lba += partitionOffset;

	unsigned int numberOfSector;
	if (limit == 0) {
		return 0;
	} else if (limit < 512) {
		numberOfSector = 1;
	} else {
		numberOfSector = (limit % 512 == 0) ? limit / 512 : (limit / 512) + 1;
	}
	/*printf("numberOfSector=%u\n",numberOfSector);*/
	outport(0x1f2, numberOfSector);
	outport(0x1f3, lba);
	outport(0x1f4, lba >> 8);
	outport(0x1f5, (lba >> 16) & 0xff);
	outport(0x1f6, 0xe0 | (driveNumber << 4));
	outport(0x1f7, 0x20);
	/*printf("end outport\n");
	 sleep(1);
	 printf("readSector 2\n"); */

	unsigned char status = 0;
	int timeout = READSECTOR_TIMEOUT
	;
	do {
		status = inport(0x1f7);
		if (timeout-- == 0) {
			return 0;
		}
	} while ((status & 0x8) != 0x8);
	/*} while ((status & 0x89) != 0x8);*/
	/*printf("end while\n");*/
	unsigned short x;
	for (x = 0; x < numberOfSector * 512; x += 2) {
		/*printf("x=%u",x);*/
		if (x >= limit && limit != -1) {
			break;
		} else {
			/*printf("start in\n");*/
			short inChar = inWordFromPort(0x1f0);
			/*printf("end in\n");*/
			/*char s=inport(0x1f1);*/
			/*printf("statur=%u\n", s);*/
			char first = inChar & 0xff;
			char second = inChar >> 8;
			if (x < limit || limit == -1) {
				data[x] = first;
			}
			if (x + 1 < limit || limit == -1) {
				data[x + 1] = second;
			}
		}
	}
	/* printf("end for\n"); */
	if (limit != -1) {
		/*printf("returning...\n");*/
		return limit;
	} else {
		return numberOfSector * 512;
	}
}

void outport(unsigned short portNumber, unsigned char outputByte) {
	__asm__ __volatile__
	("outb  %%al,%%dx\n"::"a"(outputByte), "d"(portNumber));
}

void outStringToPort(unsigned short PortNumber, const char *str) {
	int x;
	for (x = 0; x < strlen(str); x++) {
		outport(PortNumber, str[x]);
	}
}

void outHexToPort(unsigned short PortNumber, unsigned int input) {
	unsigned int reminder[32];
	int r = 0;
	int index;
	index = 0;
	do {
		r = input % 16;
		reminder[index++] = r;
		input /= 16;
	} while (input >= 16);
	if (input != 0) {
		reminder[index] = input;
	} else {
		index--;
	}
	for (r = 0; index >= 0; index--, r++) {
		if (reminder[index] < 10) {
			outport(PortNumber, (char) (reminder[index] + 0x30));
		} else {
			outport(PortNumber, (char) (reminder[index] + 0x57));
		}
	}
}

void outUnsignedLongLongToPort(unsigned short PortNumber,
		unsigned long long ull) {
	char buffer[100];
	convert(ull, buffer);
	outStringToPort(PortNumber, buffer);
}

void outLongToPort(unsigned short PortNumber, long ld) {
	char buffer[100];
	convert(ld, buffer);
	outStringToPort(PortNumber, buffer);
}

void outUnsignedLongToPort(unsigned short PortNumber, unsigned long lu) {
	char buffer[100];
	convert(lu, buffer);
	outStringToPort(PortNumber, buffer);
}

char getChar() {
	return getPasswordChar(NULL);
}

char getPasswordChar(char *passwordChar) {
	for (;;) {
		//printf("keyboardBufferTail=%x\n",&keyboardBufferTail);
		//printf("KEYBOARD_BUFFER_TAIL=%x\n",KEYBOARD_BUFFER_TAIL);
		//printf("bug 1\n");
		volatile int *keyboardBufferTail = (int *) KEYBOARD_BUFFER_TAIL;
		//printf("keyboardBufferTail=%d\n",*keyboardBufferTail);
		//printf("bug 2\n");

		if (*keyboardBufferTail) {
			disableIRQ(1);
			char *keyboardBuffer = getKeyboardBuffer();
			char c = keyboardBuffer[0];
			unsigned int x;
			for (x = 1; x < *keyboardBufferTail; x++) {
				keyboardBuffer[x - 1] = keyboardBuffer[x];
			}
			for (x = *keyboardBufferTail; x < KEYBOARD_BUFFER_SIZE; x++) {
				keyboardBuffer[x] = 0;
			}
			//printf("keyboardBufferTail22=%d\n",*keyboardBufferTail);
			(*keyboardBufferTail)--;
			//printf("keyboardBufferTail33=%d\n",*keyboardBufferTail);

			enableIRQ(1);

			if (passwordChar == NULL) {
				if (c == 0x8) {
					//backspace
					if (numberOfKeyHasPressed > 0) {
						moveCursorLeft();
						printf(" ");
						fflush(stdout);
						moveCursorLeft();
						numberOfKeyHasPressed--;
					}
				} else {
					numberOfKeyHasPressed++;
					printf("%c", c);
					fflush(stdout);
				}
			} else {
				if (c == '\n') {
					numberOfKeyHasPressed = 0;
					printf("\n");
				} else if (c != 0x8) {
					printf("%s", passwordChar);
					fflush(stdout);
				}
			}
			return c;
			//sleep(1);
			//      }
		}
	}
}

//void getline(char *buffer, unsigned int len) {
//	unsigned int x = 0;
//	char c;
//	while ((c = getChar()) != '\n') {
//		if (x < len) {
//			//printf(">> getline >> %c,%x\n",c,c);
//			if (c == 0x8) {
//				//back space
//				if (x > 0) {
//					buffer[x - 1] = '\0';
//					x--;
//				}
//			} else {
//				buffer[x] = c;
//				x++;
//			}
//		}
//	}
//	buffer[x] = '\0';
//}

void getlinePassword(char *buffer, unsigned int len, char *passwordChar) {
	unsigned int x = 0;
	char c;
	while ((c = getPasswordChar(passwordChar)) != '\n') {
		if (x < len) {
			//printf(">> getline >> %c,%x\n",c,c);
			if (c == 0x8) {
				//back space
				if (x > 0) {
					buffer[x - 1] = '\0';
					x--;
				}
			} else {
				buffer[x] = c;
				x++;
			}
		}
	}
	buffer[x] = '\0';
}

char *getKeyboardBuffer() {
	return (char *) KEYBOARD_BUFFER;
}

volatile unsigned int *getKeyboardBufferTail() {
	return KEYBOARD_BUFFER_TAIL;
}

unsigned int getKeyboardBufferSize() {
	return KEYBOARD_BUFFER_SIZE;
}

void disableAllIRQs() {
	outport(0x21, 0xff);
}

void enableIRQ(unsigned char portNumber) {
	unsigned char c = inport(0x21);
	c ^= (unsigned char) pow(2, portNumber);
	outport(0x21, c);
}

void disableIRQ(unsigned char portNumber) {
	unsigned char c = inport(0x21);
	c |= (unsigned char) pow(2, portNumber);
	outport(0x21, c);
}

int isShiftKeyPressing() {
	char *capLock = (char *) KEYBOARD_IS_SHIFT_KEY_PRESSING;
	if (*capLock) {
		return 1;
	} else {
		return 0;
	}
}

void setShiftKeyPressing(int b) {
	char *capLock = (char *) KEYBOARD_IS_SHIFT_KEY_PRESSING;
	if (b) {
		*capLock = 1;
	} else {
		*capLock = 0;
	}
}

int isCapsLockOn() {
	char *capLock = (char *) KEYBOARD_IS_CAPSLOCK_ON;
	if (*capLock) {
		return 1;
	} else {
		return 0;
	}
}

unsigned char inport(unsigned short portNumber) {
	unsigned char c;
	__asm__ __volatile__
	( "inb	%%dx,%%al\n":"=a"(c)
			:"d"(portNumber)
	);
	return c;
}

unsigned short inWordFromPort(unsigned short portNumber) {
	unsigned short s;
	__asm__ __volatile__
	( "inw	%%dx,%%ax\n":"=a"(s):"d"(portNumber));

	return s;
}
