#include <dirent.h>
#include <string.h>
#include "vfs/pfs/pfs.h"

int dirtories[1024];
int dirPointer = 0;

DIR *opendir(const char *filepath)
{
	if (strlen(filepath) > 0) {
		DIR *dir=(DIR*) malloc(sizeof(DIR));
		PFS_Directory *pfsDirectory;
		if (filepath[0] == pfs_getSeparator()) {
			pfsDirectory = pfs_findDirectory(filepath, pfs_getSuperBlock()->rootDirectoryLink);
		} else {
			pfsDirectory = pfs_findDirectory(filepath, pfs_getCurrentDirectoryBlockNo());
		}
		if (pfsDirectory==NULL){
			return NULL;
		}
		dirtories[dirPointer]=pfsDirectory;
		dir->pfsDirectory=pfsDirectory;
		dirPointer++;
		return dir;
	} else {
		return NULL;
	}
	return NULL;
}

struct dirent * readdir(DIR * dir)
{
	return pfs_readdir(dir->pfsDirectory);
}
