#ifndef __DISPLAY_H__
#define __DISPLAY_H__

extern unsigned int cursorX;
extern unsigned int cursorY;
extern unsigned int videoMode;

typedef struct VBE_VgaInfo {
	char VESASignature[4];
	short VESAVersion;
	char *OemStringPtr;
	unsigned long Capabilities;
	unsigned long VideoModePtr;
	short TotalMemory;
	//VBE 2.0
	short OemSoftwareRev;
	char *OemVendorNamePtr;
	char *OemProductNamePtr;
	char *OemProductRevPtr;
	char reserved[222];
	char OemData[256];
}__attribute__ ((packed));

//pointed by VBE_VgaInfo->VideoModePtr
struct VBE_ModeNumberInfo {
	unsigned short modeNumber;
	int reservedforFutureExpansion;
	int userSpecifiedCRTCrefreshRateValuesint;
	int reservedForVBE;
	int initializesAcceleratorHardware;
	int useLinearFrameBuffer;
	int preserveDisplayMemoryOnModeChange;
}__attribute__ ((packed));

// get by int 0x10, function 0x4F01
struct VBE_ModeInfo {
	short ModeAttributes;
	char WinAAttributes;
	char WinBAttributes;
	short WinGranularity;
	short WinSize;
	unsigned short WinASegment;
	unsigned short WinBSegment;
	void *WinFuncPtr;
	short BytesPerScanLine;
	short XRes;
	short YRes;
	char XCharSize;
	char YCharSize;
	char NumberOfPlanes;
	char BitsPerPixel;
	char NumberOfBanks;
	char MemoryModel;
	char BankSize;
	char NumberOfImagePages;
	char res1;
	char RedMaskSize;
	char RedFieldPosition;
	char GreenMaskSize;
	char GreenFieldPosition;
	char BlueMaskSize;
	char BlueFieldPosition;
	char RsvedMaskSize;
	char RsvedFieldPosition;
	char DirectColorModeInfo; //MISSED IN THIS TUTORIAL!! SEE ABOVE
	//VBE 2.0
	unsigned long PhysBasePtr;
	unsigned long OffScreenMemOffset;
	short OffScreenMemSize;
	//VBE 2.1
	short LinbytesPerScanLine;
	char BankNumberOfImagePages;
	char LinNumberOfImagePages;
	char LinRedMaskSize;
	char LinRedFieldPosition;
	char LingreenMaskSize;
	char LinGreenFieldPosition;
	char LinBlueMaskSize;
	char LinBlueFieldPosition;
	char LinRsvdMaskSize;
	char LinRsvdFieldPosition;
	char res2[194];
}__attribute__ ((packed));



void setTextMode();
void setGraphicMode();
unsigned int getScreenWidth();
unsigned int getScreenHeight();
void clearScreen();
void k_clearScreen();
void clearScreenText();
void k_clearScreenText();
int vesaBiosEnable();
void setCursor(unsigned x, unsigned y);
void k_moveCursorLeft();
void k_moveCursorRight(int count);
void k_moveCursorNextLine();
void moveCursorLeft();
void moveCursorRight(int count);
void moveCursorNextLine();
void k_outCharToDisplay(char chr, char attr);
void k_outStringToDisplay(char *str, char attr);
void k_outStringToDisplay_len(char *str, char attr, int len);
void k_outHexToDisplay_upper(unsigned int input, char attr);
void k_outHexToDisplay_lower(unsigned int input, char attr);
void k_outUnsignedLongLongToDisplay(unsigned long long ull, char attr);
void k_outLongToDisplay(long ld, char attr);
void k_outUnsignedLongToDisplay(unsigned long lu, char attr);
void outCharToDisplay(char chr, char attr);
void outIntToDisplay(int x, char attr);
void outIntToDisplayLn(int x, char attr);
void outStringToDisplay(char *str, char attr);
void outStringToDisplay_len(char *str, char attr, int len);
void outHexToDisplay_upper(unsigned int input, char attr);
void outHexToDisplay_lower(unsigned int input, char attr);
void outUnsignedLongLongToDisplay(unsigned long long ull, char attr);
void outLongToDisplay(long ld, char attr);
void outUnsignedLongToDisplay(unsigned long lu, char attr);
void printHex(char *str);
void printHex2(char *str, unsigned int count);
void clearLine();
void printString(char *str, int count);
struct VBE_VgaInfo *getVESAInfo();

#endif
