#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define BOCHS_PAUSE __asm__ __volatile__("XCHG %BX, %BX");
#define START_INSTRUMENTATION __asm__ __volatile__("PREFETCHT0 0x12345678");
#define STOP_INSTRUMENTATION __asm__ __volatile__("PREFETCHT0 0x87654321");

struct Descriptor {
	unsigned char byte0;
	unsigned char byte1;
	unsigned char byte2;
	unsigned char byte3;
	unsigned char byte4;
	unsigned char byte5;
	unsigned char byte6;
	unsigned char byte7;
} __attribute__((packed));

typedef struct Descriptor Descriptor;

struct TSS {
	unsigned short backLink;
	unsigned short reserved0;
	unsigned long ESP0;
	unsigned short SS0;
	unsigned short reserved1;
	unsigned long ESP1;
	unsigned short SS1;
	unsigned short reserved2;
	unsigned long ESP2;
	unsigned short SS2;
	unsigned short reserved3;
	unsigned long CR3;
	unsigned long EIP;
	unsigned long EFLAGS;
	unsigned long EAX;
	unsigned long ECX;
	unsigned long EDX;
	unsigned long EBX;
	unsigned long ESP;
	unsigned long EBP;
	unsigned long ESI;
	unsigned long EDI;
	unsigned short ES;
	unsigned short reserved4;
	unsigned short CS;
	unsigned short reserved5;
	unsigned short SS;
	unsigned short reserved6;
	unsigned short DS;
	unsigned short reserved7;
	unsigned short FS;
	unsigned short reserved8;
	unsigned short GS;
	unsigned short reserved9;
	unsigned short LDTR;
	unsigned short reserved10;
	unsigned short T;
	unsigned short IOPB;
} __attribute__((packed));

struct Page_Table {
	unsigned char p : 1;
	unsigned char rw : 1;
	unsigned char us : 1;
	unsigned char pwt : 1;
	unsigned char pcd : 1;
	unsigned char a : 1;
	unsigned char d : 1;
	unsigned char pat : 1;
	unsigned char g : 1;
	unsigned char avl : 3;
	unsigned int pageTableBase : 20;
} __attribute__((packed));

struct Page_Directory {
	unsigned char p : 1;
	unsigned char rw : 1;
	unsigned char us : 1;
	unsigned char pwt : 1;
	unsigned char pcd : 1;
	unsigned char a : 1;
	unsigned char d : 1;
	unsigned char reserved : 1;
	unsigned char g : 1;
	unsigned char avl : 3;
	unsigned int pageTableBase : 20;
} __attribute__((packed));
unsigned int getMemorySize();

typedef struct cpuid_regs {
	unsigned int eax;
	unsigned int ebx;
	unsigned int ecx;
	unsigned int edx;
} __attribute__((packed)) cpuid_regs_t;

cpuid_regs_t cpuid(int functionNumber);


//void cpuid();
char *getCpuidVendorsign();
int getCpuidFamily();
int getCpuidModel();
int getCpuidFamilyEx();
int getCpuidModelEx();
int getCpuidStepping();
int getCPUSpeed();
unsigned char getSecond();
unsigned char getMinute();
unsigned char getHour();
unsigned char getDay();
unsigned char getMonth();
unsigned char getYear();
void ltr(unsigned short descriptorNumber);
unsigned long long rdtsc();
//void sleep(int seconds);
unsigned long long getTimeOfDay();
unsigned int getFreeLinearAddress(unsigned size);
void freeLinearAddress(unsigned ptr);
unsigned int getFreePage(unsigned size);
unsigned int getFreePageForUpper1GB(unsigned size);
unsigned int getPhysicalAddress(unsigned int linearAddress);
unsigned int getPhysicalAddressByPD(struct Page_Directory *pageDirectories, int *linearAddress);
void *kmalloc(unsigned size);
//void *kmalloc(unsigned int virtualAddress, unsigned size);
void *kmalloc2(unsigned int virtualAddress, unsigned int physicalAddress, unsigned size);
void *kmalloc_upper1GB(unsigned size);
unsigned int getPhysicallAddress(void *virtualAddress);
unsigned int getNumberOfPage(void *virtualAddress);
void setDescriptor(struct Descriptor *descriptor, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType);
void setGDTR(char *gdt, unsigned short numberOfGDTDescriptor);
void free(void *ptr);

unsigned int getPageTableEntryAddress(struct Page_Directory *pageDirectories, unsigned int linearAddress);

void setGDTDescriptor(struct Descriptor *descriptor, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType);
int getNumberOfGDTDescriptor();
int getGDTLimit();
struct Descriptor * getGDT();

struct VirutalAddressBlock {
	unsigned int offset;
	unsigned int size;
} __attribute__((packed));

void setIDTDescriptor(unsigned char *idtDescriptor, unsigned long offset, unsigned short selector, unsigned char type, unsigned char DPL, int P);
void setIDTTaskGateDescriptor(unsigned char *idtDescriptor, unsigned short selector, unsigned char DPL, int P);


inline void __native_flush_tlb_single(unsigned long addr) {
    //__asm __volatile__("XCHG %BX, %BX");
    asm volatile("invlpg (%0)" ::"r" (addr) : "memory");

    /*
    __asm __volatile__(" \
    movl	%cr3,%eax \n\
    movl	%eax,%cr3 \n\
    ");
     */
}

#endif
