#include "testingLibrary.h"
#define BOCHS_PAUSE __asm__ __volatile__("XCHG %BX, %BX");

int myAdd(int x){
	BOCHS_PAUSE
	addOne(x);
	return addTwo(x);
}

int main(){
		myAdd(123);
		return 1980;
}
