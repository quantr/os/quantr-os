#include <stdio.h>
#include <stdlib.h>
#include <libelf/libelf.h>

void dumphex(char *buffer, int len) {
	int x;
	for (x = 0; x < len; x++) {
		printf("%x ", buffer[x]);
	}
	printf("\n");
}

int main(int argc, char *argv[]) {
	FILE * pFile;
	long lSize;
	char * buffer;
	int bufferLength = -1;
	size_t result;
	pFile = fopen(argv[1], "rb");
	if (pFile == NULL) {
		printf("Library not found\n");
		return 1;
	}

	// obtain file size:
	fseek(pFile, 0, SEEK_END);
	lSize = ftell(pFile);
	rewind(pFile);

	// allocate memory to contain the whole file:
	buffer = (char*) malloc(sizeof(char) * lSize);
	bufferLength = sizeof(char) * lSize;
	if (buffer == NULL) {
		printf("Memory error\n");
		return;
	}

	// copy the file into the buffer:
	printf("LLSIZE=%d\n", lSize);
	result = fread(buffer, 1, lSize, pFile);
	printf("buffer addr 2=%x\n", buffer);
	if (result != lSize) {
		printf("Reading error\n");
		return;
	}
	/* the whole file is now loaded in the memory buffer. */

	// terminate
	fclose(pFile);

	Elf32_Ehdr *header = (Elf32_Ehdr *) buffer;
	printf("ELF header size = %d\n", sizeof(Elf32_Ehdr));
	printf("e_ident=");
	dumphex((char *) header->e_ident, EI_NIDENT);

	printf("e_type=%x\n", header->e_type);
	printf("e_machine=%x\n", header->e_machine);
	printf("e_version=%x\n", header->e_version);
	printf("e_entry=%x\n", header->e_entry);
	printf("e_phoff=%x\n", header->e_phoff);
	printf("e_shoff=%x\n", header->e_shoff);
	printf("e_flags=%x\n", header->e_flags);
	printf("e_ehsize=%x\n", header->e_ehsize);
	printf("e_phentsize=%x\n", header->e_phentsize);
	printf("e_phnum=%x\n", header->e_phnum);
	printf("e_shentsize=%x\n", header->e_shentsize);
	printf("e_shnum=%x\n", header->e_shnum);
	printf("e_shstrndx=%x\n", header->e_shstrndx);

	printf("----------- sections -----------\n");
	Elf32_Shdr *sections = (Elf32_Shdr *) (buffer + header->e_shoff);
	int stringTableOffset = sections[header->e_shstrndx].sh_offset;

	int symbolTableAddress = -1;
	int symbolTableSize = -1;
	int dynamicSymbolTableAddress = -1;
	int dynamicSymbolTableSize = -1;

	int stringTableAddress = -1;
	int dynamicStringTableAddress = -1;

	int shStringTableAddress=-1;

	int textSectionIndex = -1;

	int rel_sectionAddress[100];
	int rel_sectionSize[100];
	int numOf_rel_sectionAddress = 0;
	int rel_shLink[100];
	int rel_shOffset[100];

	int rela_sectionAddress[100];
	int rela_sectionSize[100];
	int numOf_rela_sectionAddress = 0;
	int rela_shLink[100];
	int rela_shOffset[100];
	int numberOfSymbolTable = 0;

	if (stringTableOffset != -1) {
		int x;
		for (x = 0; x < header->e_shnum; x++) {
			char *sectionName = (char *) buffer + stringTableOffset
					+ sections[x].sh_name;
			printf("%d, sh_name=%x, %20s\t\t", x, stringTableOffset
					+ sections[x].sh_name, sectionName); /* Section name (string tbl index) */
			printf("sh_type=%x\t", sections[x].sh_type); /* Section type */
			printf("sh_flags=%x\t", sections[x].sh_flags); /* Section flags */
			printf("sh_addr=%x\t", sections[x].sh_addr); /* Section virtual addr at execution */
			printf("sh_offset=%x\t", sections[x].sh_offset); /* Section file offset */
			printf("sh_size=%x\t", sections[x].sh_size); /* Section size in bytes */
			printf("sh_link=%x\t", sections[x].sh_link); /* Link to another section */
			printf("sh_info=%x\t", sections[x].sh_info); /* Additional section information */
			printf("sh_addralign=%x\t", sections[x].sh_addralign); /* Section alignment */
			printf("sh_entsize=%x\t", sections[x].sh_entsize); /* Entry size if section holds table */
			printf("\n");

			if (strcmp(sectionName, ".text") == 0) {
				textSectionIndex = x;
			} else if (strcmp(sectionName, ".symtab") == 0) {
				symbolTableAddress = sections[x].sh_offset;
				symbolTableSize = sections[x].sh_size;
				numberOfSymbolTable++;
			} else if (strcmp(sectionName, ".dynsym") == 0) {
				dynamicSymbolTableAddress
						= sections[x].sh_offset;
				dynamicSymbolTableSize = sections[x].sh_size;
			} else if (strcmp(sectionName, ".strtab") == 0) {
				stringTableAddress = sections[x].sh_offset;
			} else if (strcmp(sectionName, ".dynstr") == 0) {
				dynamicStringTableAddress
						= sections[x].sh_offset;
			} else if (strcmp(sectionName, ".shstrtab") == 0) {
							shStringTableAddress
									= sections[x].sh_offset;
			} else if (sections[x].sh_type == SHT_RELA) {
				printf("SHT_REA-------------------\n");
				rela_sectionAddress[numOf_rela_sectionAddress]
						= sections[x].sh_offset;
				rela_sectionSize[numOf_rela_sectionAddress]
						= sections[x].sh_size;
				rela_shLink[numOf_rela_sectionAddress]
						= sections[x].sh_link;
				rela_shOffset[numOf_rela_sectionAddress]
						= sections[x].sh_offset;
				numOf_rela_sectionAddress++;
			} else if (sections[x].sh_type == SHT_REL) {
				printf("SHT_RE-------------------\n");
				rel_sectionAddress[numOf_rel_sectionAddress]
						= sections[x].sh_offset;
				rel_sectionSize[numOf_rel_sectionAddress]
						= sections[x].sh_size;
				rel_shLink[numOf_rel_sectionAddress]
						= sections[x].sh_link;
				rel_shOffset[numOf_rel_sectionAddress]
						= sections[x].sh_offset;
				numOf_rel_sectionAddress++;
			}
		}
	}

	printf("numberOfSymbolTable=%d\n", numberOfSymbolTable);
	printf("symbolTableAddress=%x\n", symbolTableAddress);
	printf("symbolStringTableAddress=%x\n", stringTableAddress);
	if (symbolTableAddress != -1 && symbolTableSize != -1) {
		printf("----------- symbols -----------\n");
		int x;
		Elf32_Sym *symbol = (Elf32_Sym *) (buffer + symbolTableAddress);
		for (x = 0; x < symbolTableSize / sizeof(Elf32_Sym); x++) {
			char *symbolName = (char *) (buffer
					+ stringTableAddress
					+ symbol[x].st_name);
			printf("%d) st_name=%8x\t%20s\t", x, symbol[x].st_name,
					symbolName); /* Symbol name (string tbl index) */
			printf("st_value=%10x\t", symbol[x].st_value); /* Symbol value */
			printf("st_size=%x\t", symbol[x].st_size); /* Symbol size */
			printf("st_info=%x\t", symbol[x].st_info); /* Symbol type and binding */
			printf("st_other=%x\t", symbol[x].st_other); /* Symbol visibility */
			printf("st_shndx=%x\t", symbol[x].st_shndx); /* Section index */
			printf("\n");
		}
	}

	if (dynamicSymbolTableAddress != -1 && dynamicSymbolTableSize != -1) {
		printf("----------- dynamic symbols -----------\n");
		int x;
		Elf32_Sym *symbol = (Elf32_Sym *) (buffer
				+ dynamicSymbolTableAddress);
		for (x = 0; x < dynamicSymbolTableSize / sizeof(Elf32_Sym); x++) {
			char *symbolName = (char *) (buffer
					+ dynamicStringTableAddress
					+ symbol[x].st_name);
			printf("%d) st_name=%8x\t%20s\t", x, symbol[x].st_name,
					symbolName); /* Symbol name (string tbl index) */
			printf("st_value=%10x\t", symbol[x].st_value); /* Symbol value */
			printf("st_size=%x\t", symbol[x].st_size); /* Symbol size */
			printf("st_info=%x\t", symbol[x].st_info); /* Symbol type and binding */
			printf("st_other=%x\t", symbol[x].st_other); /* Symbol visibility */
			printf("st_shndx=%x\t", symbol[x].st_shndx); /* Section index */
			printf("\n");
		}
	}

	printf("----------- Program header -----------\n");
	int PT_Dynamic_p_offset = -1;
	int PT_Dynamic_p_filesz = -1;
	Elf32_Phdr *headers = (Elf32_Phdr *) (buffer + header->e_phoff);
	int textHeaderIndex = -1;
	int x;
	int y;
	int first_PI_LOAD_address = -1;
	for (x = 0; x < header->e_phnum; x++) {
		//char *sectionName=(char *)buffer+stringTableOffset+sections[x].sh_name;
		printf("%d), p_type=%10x\t", x, headers[x].p_type); /* Segment type */

		switch (headers[x].p_type) {
		case PT_NULL:
			printf("%20s", "PT_NULL");
			break;
		case PT_LOAD:
			printf("%20s", "PT_LOAD");
			break;
		case PT_DYNAMIC:
			printf("%20s", "PT_DYNAMIC");
			PT_Dynamic_p_offset = headers[x].p_offset;
			PT_Dynamic_p_filesz = headers[x].p_filesz;
			break;
		case PT_INTERP:
			printf("%20s", "PT_INTERP");
			break;
		case PT_NOTE:
			printf("%20s", "PT_NOTE");
			break;
		case PT_SHLIB:
			printf("%20s", "PT_SHLIB");
			break;
		case PT_PHDR:
			printf("%20s", "PT_TLS");
			break;
		case PT_TLS:
			printf("%20s", "PT_TLS");
			break;
		case PT_NUM:
			printf("%20s", "PT_NUM");
			break;
		case PT_LOOS:
			printf("%20s", "PT_LOOS");
			break;
		case PT_LOSUNW:
			printf("%20s", "PT_LOSUNW");
			break;
		case PT_HIOS:
			printf("%20s", "PT_HIOS");
			break;
		case PT_LOPROC:
			printf("%20s", "PT_LOPROC");
			break;
		case PT_HIPROC:
			printf("%20s", "PT_HIPROC");
			break;
		default:
			printf("%20s", "unknown");
		}
		printf("\t");

		printf("p_offset=%x\t\t", headers[x].p_offset); /* Segment file offset */
		printf("p_vaddr=%x\t", headers[x].p_vaddr); /* Segment virtual address */
		if (first_PI_LOAD_address == -1 && headers[x].p_type == PT_LOAD) {
			first_PI_LOAD_address = headers[x].p_vaddr;
		}
		printf("p_paddr=%x\t", headers[x].p_paddr); /* Segment physical address */
		printf("p_filesz=%x\t", headers[x].p_filesz); /* Segment size in file */
		printf("p_memsz=%x\t", headers[x].p_memsz); /* Segment size in memory */
		printf("p_flags=%x\t", headers[x].p_flags); /* Segment flags */

		if (headers[x].p_flags & PF_X) {
			printf("X");
		}
		if (headers[x].p_flags & PF_W) {
			printf("W");
		}
		if (headers[x].p_flags & PF_R) {
			printf("R");
		}
		if (headers[x].p_flags & PF_MASKOS) {
			printf(",PFMASKOS");
		}
		if (headers[x].p_flags & PF_MASKPROC) {
			printf(",PF_MASKPROC");
		}
		printf("\t");

		printf("p_align=%x\t", headers[x].p_align); /* Segment alignment */
		printf("\n");

		if (headers[x].p_type == PT_LOAD && headers[x].p_flags & PF_X
				&& headers[x].p_flags & PF_R) {
			textHeaderIndex = x;
		}
	}

	// dump PT_Dynamic program header
	if (PT_Dynamic_p_offset != -1 && PT_Dynamic_p_filesz != -1) {
		printf(" PT_Dynamic : \n");
		int x;
		;
		int DT_STRTAB_PTR = -1;

		int DT_NEEDED_PTR[1024];
		for (x = 0; x < 1024; x++) {
			DT_NEEDED_PTR[x] = -1;
		}

		int DT_SONAME_PTR[1024];
		for (x = 0; x < 1024; x++) {
			DT_SONAME_PTR[x] = -1;
		}

		int tempX_DT_NEEDED_PTR;
		int tempX_DT_SONAME_PTR;
		int DT_REL_ADDRESS = -1;
		int DT_RELSZ_VAL = -1;

		for (x = 0, tempX_DT_NEEDED_PTR = 0, tempX_DT_SONAME_PTR = 0; x
				< PT_Dynamic_p_filesz / sizeof(Elf32_Dyn); x++) {
			Elf32_Dyn *dyn = (Elf32_Dyn *) (buffer
					+ PT_Dynamic_p_offset + (x
					* sizeof(Elf32_Dyn)));
			printf("%x\td_tag=%.10x", dyn, dyn->d_tag);

			switch (dyn->d_tag) {
			case DT_NULL:
				/* Marks end of dynamic section */
				printf("\tDT_NULL\t\t");
				break;
			case DT_NEEDED:
				/* Name of needed library */
				printf("\tDT_NEEDED\t");
				DT_NEEDED_PTR[tempX_DT_NEEDED_PTR++]
						= dyn->d_un.d_ptr;
				break;
			case DT_PLTRELSZ:
				/* Size in bytes of PLT relocs */
				printf("\tDT_PLTRELSZ\t");
				break;
			case DT_PLTGOT:
				/* Processor defined value */
				printf("\tDT_PLTGOT\t");
				break;
			case DT_HASH:
				/* Address of symbol hash table */
				printf("\tDT_PLTGOT\t");
				break;
			case DT_STRTAB:
				/* Address of string table */
				printf("\tDT_STRTAB\t");
				DT_STRTAB_PTR = dyn->d_un.d_ptr;
				break;
			case DT_SYMTAB:
				/* Address of symbol table */
				printf("\tDT_SYMTAB\t");
				break;
			case DT_RELA:
				/* Address of Rela relocs */
				printf("\tDT_RELA\t");
				break;
			case DT_RELASZ:
				/* Total size of Rela relocs */
				printf("\tDT_RELA\t");
				break;
			case DT_RELAENT:
				/* Size of one Rela reloc */
				printf("\tDT_RELAENT\t");
				break;
			case DT_STRSZ:
				/* Size of string table */
				printf("\tDT_STRSZ\t");
				break;
			case DT_SYMENT:
				/* Size of one symbol table entry */
				printf("\tDT_SYMENT\t");
				break;
			case DT_INIT:
				/* Address of init function */
				printf("\tDT_INIT\t\t");
				break;
			case DT_FINI:
				/* Address of termination function */
				printf("\tDT_FINI\t\t");
				break;
			case DT_SONAME:
				/* Name of shared object */
				printf("\tDT_SONAME\t");
				DT_SONAME_PTR[tempX_DT_SONAME_PTR++]
						= dyn->d_un.d_ptr;
				break;
			case DT_RPATH:
				/* Library search path (deprecated) */
				printf("\tDT_RPATH\t");
				break;
			case DT_SYMBOLIC:
				/* Start symbol search here */
				printf("\tDT_SYMBOLIC\t");
				break;
			case DT_REL:
				/* Address of Rel relocs */
				printf("\tDT_REL\t\t");
				break;
			case DT_RELSZ:
				/* Total size of Rel relocs */
				printf("\tDT_RELSZ\t");
				break;
			case DT_RELENT:
				/* Size of one Rel reloc */
				printf("\tDT_RELENT\t");
				break;
			case DT_PLTREL:
				/* Type of reloc in PLT */
				printf("\tDT_PLTREL\t");
				break;
			case DT_DEBUG:
				/* For debugging; unspecified */
				printf("\tDT_DEBUG\t");
				break;
			case DT_TEXTREL:
				/* Reloc might modify .text */
				printf("\tDT_TEXTREL\t");
				break;
			case DT_JMPREL:
				/* Address of PLT relocs */
				printf("\tDT_JMPREL\t");
				break;
			case DT_BIND_NOW:
				/* Process relocations of object */
				printf("\tDT_BIND_NOW\t");
				break;
			case DT_INIT_ARRAY:
				/* Array with addresses of init fct */
				printf("\tDT_INIT_ARRAY\t");
				break;
			case DT_FINI_ARRAY:
				/* Array with addresses of fini fct */
				printf("\tDT_FINI_ARRAY\t");
				break;
			case DT_INIT_ARRAYSZ:
				/* Size in bytes of DT_INIT_ARRAY */
				printf("\tDT_INIT_ARRAYSZ\t");
				break;
			case DT_FINI_ARRAYSZ:
				/* Size in bytes of DT_FINI_ARRAY */
				printf("\tDT_FINI_ARRAYSZ\t");
				break;
			case DT_RUNPATH:
				/* Library search path */
				printf("\tDT_RUNPATH\t");
				break;
			case DT_FLAGS:
				/* Flags for the object being loaded */
				printf("\tDT_FLAGS\t");
				break;
			case DT_PREINIT_ARRAYSZ:
				/* size in bytes of DT_PREINIT_ARRAY */
				printf("\tDT_PREINIT_ARRAYSZ\t");
				break;
			case DT_NUM:
				/* Number used */
				printf("\tDT_NUM\t");
				break;
			case DT_LOOS:
				/* Start of OS-specific */
				printf("\tDT_LOOS\t");
				break;
			case DT_HIOS:
				/* End of OS-specific */
				printf("\tDT_HIOS\t");
				break;
			case DT_LOPROC:
				/* Start of processor-specific */
				printf("\tDT_LOPROC\t");
				break;
			case DT_HIPROC:
				/* End of processor-specific */
				printf("\tDT_HIPROC\t");
				break;
			default:
				printf("\t\t\t");
			}

			if (dyn->d_tag == DT_REL) {
				DT_REL_ADDRESS = dyn->d_un.d_val;
			}

			if (dyn->d_tag == DT_RELSZ) {
				DT_RELSZ_VAL = dyn->d_un.d_val;
			}

			printf("td_val=%.10x\td_ptr=%.10x\n", dyn->d_un.d_val,
					dyn->d_un.d_ptr);
		}

		// dump DT_NEEDED
		printf("------------ dump DT NEEDED ------------\n");
		for (x = 0; x < 1024; x++) {
			if (DT_NEEDED_PTR[x] == -1) {
				break;
			}
			printf("%s\n", buffer + DT_STRTAB_PTR
					+ DT_NEEDED_PTR[x]
					- first_PI_LOAD_address);
		}
		// end dump DT_NEEDED

		// dump DT_SONAME
		printf("------------ dump DT_SONAME ------------\n");
		for (x = 0; x < 1024; x++) {
			if (DT_SONAME_PTR[x] == -1) {
				break;
			}
			printf("%s\n", buffer + DT_STRTAB_PTR
					+ DT_SONAME_PTR[x]);
		}
		// end dump DT_SONAME

	}
	// end dump PT_Dynamic program header

	// dump SHT_RELA
	for (y = 0; y < numOf_rela_sectionAddress; y++) {
		printf("------------ dump SHT_RELA ------------\n");
		for (x = 0; x < rela_sectionSize[y] / sizeof(Elf32_Rela); x++) {
			Elf32_Rela *elf32_rela = (Elf32_Rela *) (buffer
					+ rela_sectionAddress[y] + x
					* sizeof(Elf32_Rel));
			printf(
					"%d\telf32_rela->r_offset=%x\telf32_rela->r_info=%x\telf32_rela->r_addend=%x, ELF32_R_SYM=%x\n",
					x, elf32_rela->r_offset,
					elf32_rela->r_info,
					ELF32_R_SYM(elf32_rela->r_info));
		}
	}
	// end dump SHT_RELA

	// dump SHT_REL
	for (y = 0; y < numOf_rel_sectionAddress; y++) {
		printf("------------ dump SHT_REL ------------\n");
		for (x = 0; x < rel_sectionSize[y] / sizeof(Elf32_Rel); x++) {
			Elf32_Rel *elf32_rel = (Elf32_Rel *) (buffer
					+ rel_sectionAddress[y] + x
					* sizeof(Elf32_Rel));
			printf(
					"%d\telf32_rel->r_offset=%x\telf32_rel->r_info=%x, ELF32_R_SYM=%x  , ELF32_R_TYPE=%x, ELF32_R_INFO=%x",
					x,
					elf32_rel->r_offset,
					elf32_rel->r_info,
					ELF32_R_SYM(elf32_rel->r_info),
					ELF32_R_TYPE(elf32_rel->r_info),
					ELF32_R_INFO(ELF32_R_SYM(elf32_rel->r_info), ELF32_R_TYPE(elf32_rel->r_info)));

			int index;
			int numberOfString = 0;

			for (index = dynamicStringTableAddress + 1; index
					< bufferLength && numberOfString
					< ELF32_R_SYM(elf32_rel->r_info); index++) {
				if (buffer[index] == '\0') {
					numberOfString++;
				}
			}
			printf(", name=%s", buffer + index);
			printf("\n");
		}
	}
	// end dump SHT_REL

	// load section to memory
	if (textSectionIndex != -1 && textHeaderIndex != -1) {
		printf("load file offset=%x to memory %x and jump to %x\n",
				sections[textSectionIndex].sh_offset,
				headers[textHeaderIndex].p_vaddr,
				sections[textSectionIndex].sh_addr);
	}

	free(buffer);
}
