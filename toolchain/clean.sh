#!/bin/bash

echo "rm -fr binutils-2.25";
rm -fr binutils-2.25
echo "rm -fr build-binutils";
rm -fr build-binutils
echo "rm -fr build-gcc";
rm -fr build-gcc
echo "rm -fr build-newlib";
rm -fr build-newlib
echo "rm -fr gcc-5.4.0";
rm -fr gcc-5.4.0

a=`command -v sudo`
if [ "$a" = '' ]; then
	echo "rm -fr /toolchain";
	rm -fr /toolchain
else
	echo "sudo rm -fr /toolchain";
	sudo rm -fr /toolchain
fi
echo "distclean grub"
cd grub
make distclean

