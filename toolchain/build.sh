#!/bin/bash

############################################################################################################################################################
#1. Need to build iconv manually, because macport's libiconv doesn't support x86-64, steps are:
#wget http://ftp.gnu.org/gnu/libiconv/libiconv-1.14.tar.gz
#tar zxvf libiconv-1.14.tar.gz
#cd libiconv-1.14
#CFLAGS='-arch i386 -arch x86_64' CCFLAGS='-arch i386 -arch x86_64' CXXFLAGS='-arch i386 -arch x86_64' ./configure
#sudo make install
#
#2. don't remove build-newlib and newlib-2.1.0.tar.gz directory, because i changed something to make the cross compilation works.
#
#3. don't remove build-libelf and libelf-0.8.13.tar directory, since it can't be built by cross compiler, i followed this (http://blog.csdn.net/cmk128/article/details/8074127) to make it compile by i586-peter-elf-gcc.
#
#4. gcc need newlib source directory (Ubuntu 14.04.2 LTS), so -with-headers=../newlib-2.1.0/newlib/libc/include/


export PREFIX=/toolchain
export TARGET=i586-peter-elf

sudo rm -fr /toolchain
sudo mkdir /toolchain
sudo chmod 777 /toolchain

mkdir build-binutils build-gcc build-newlib

tar jxvf binutils-2.25.tar.bz2
cd build-binutils
../binutils-2.25/configure --target=$TARGET --prefix=$PREFIX --disable-nls CFLAGS='-Wno-error=unused-variable -Wno-error=unused-function -Wno-error=deprecated-declarations'
make all install
cd ..

#tar jxvf gcc-4.9.0.tar.bz2
#export PATH=$PATH:$PREFIX/bin
#cd build-gcc
#../gcc-4.9.0/configure --target=$TARGET --prefix=$PREFIX --disable-nls --enable-languages=c,c++ --without-headers --with-newlib --with-gmp=/opt/local #-with-headers=../newlib-2.1.0/newlib/libc/include/ #CC=gcc-mp-5 CXX=g++-mp-5
#make all-gcc all-target-libgcc install-gcc install-target-libgcc

tar jxvf gcc-5.4.0.tar.bz2
export PATH=$PATH:$PREFIX/bin
cd build-gcc
#CC=gcc-mp-5 CXX=g++-mp-5 ../gcc-5.4.0/configure --target=$TARGET --prefix=$PREFIX --disable-nls --enable-languages=c,c++ --without-headers --with-newlib --with-gmp=/opt/local
ln -s ../newlib-2.1.0/newlib ../gcc-5.4.0/newlib
ln -s ../newlib-2.1.0/libgloss ../gcc-5.4.0/libgloss
../gcc-5.4.0/configure --target=$TARGET --prefix=$PREFIX --disable-nls --enable-languages=c,c++ --without-headers --with-gmp=/opt/local --with-newlib --enable-host-shared
make all-gcc all-target-libgcc all-target-libstdc++-v3 install-gcc install-target-libgcc install-target-libstdc++-v3
cd ..

#tar zxvf newlib-2.1.0.tar.gz
cd build-newlib
../newlib-2.1.0/configure --target=$TARGET --prefix=$PREFIX
make all install
cd ..

#tar xvf libelf-0.8.13.tar
cd build-libelf/
# ../libelf-0.8.13/configure --prefix=$PREFIX --enable-debug
make clean all install
cd ..

#git clone git://git.savannah.gnu.org/grub.git
cd grub
make distclean
./autogen.sh # no need to run autogen because when i unzip grub2, it already has configure if download the release archive, but not git clone
./configure --prefix=$PREFIX --target=$TARGET
make all install
cd ..

echo "Toolchain is built"
