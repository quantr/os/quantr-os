#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include "../../../../../../library/libpeter/vfs/pfs/PFS_File.h"
#include "../../../../../../kernel/header.h"

int files[1024];
int filePointer = 3;

int _read(int file, char * ptr, int len) {
    if (file > 2) {
        return vfs_fread(ptr, len, 1, files[file - 3]);
    } else if (file == 0) {
        /*for (int x = 0; x < len; x++) {
         char c = getChar();
         if (c == '\n') {
         ptr[x] = -1;
         return x;
         }
         ptr[x] = c;
         }*/
        ptr[0] = getChar();
        return 1;
        //return len;
    } else {
        volatile int x = 0;
        while (1) {
            x = x + 23;
        }
    }
}

int _lseek(int file, int ptr, int dir) {
    return vfs_fseek(files[file - 3], ptr, dir);
}

int _write(int file, char * ptr, int len) {
    if (file == 1) {
        outStringToDisplay_len(ptr, 0xf, len);
        return 0;
    } else {
        volatile int x = 0;
        while (1) {
            x = x + 3;
        }

        return -1;
    }
}

int _open(const char * path, int flags) {
    FILE *file = vfs_fopen(path, flags);
	printf("\t\t\t_open=%x\n", file);
    if (file == NULL) {
        return NULL;
    } else {
        files[filePointer - 3] = file;
        int x = filePointer;
        filePointer++;
        return x;
    }
}

int _close(int file) {
    vfs_fclose(files[file - 3]);
}

void _exit(int n) {
    volatile int x = 0;
    while (1) {
        x = x + 6;
    }
}

#define HEAPSIZE 0xfffff
unsigned char heap[HEAPSIZE];

void * _sbrk(int incr) {
    //return KERNEL_SBRK_LIMIT;
    static unsigned char *heap_end;
    unsigned char *prev_heap_end;

    /* initialize */
    if (heap_end == 0)
        heap_end = heap;

    prev_heap_end = heap_end;

    if (heap_end + incr - heap > HEAPSIZE) {
        /* heap overflow - announce on stderr */
        write(2, "Heap overflow!\n", 15);
        abort();

        volatile int x = 0;
        while (1) {
            x = x + 0x52;
        }
    }

    heap_end += incr;

    return (caddr_t) prev_heap_end;
}

int _fstat(file, st)
int file;
struct stat * st;
{
    if (file > 2) {
        PFS_File *pfs_file = files[file - 3];
        st->st_blksize = 4096;
        st->st_ino = pfs_file->blockNumber;
        st->st_size = pfs_file->filesize;
    } /*else if (file == 0) {
	 st->st_size = 1;
	 }*/
    return 0;
}

int _unlink() {
    volatile int x = 0;
    while (1) {
        x = x + 9;
    }
    return -1;
}

int _isatty(int fd) {
    if (fd == 0) {
        return 1;
    }
    volatile int x = 0;
    while (1) {
        x = x + 10;
    }
    return 0;
}

int _raise() {
    volatile int x = 0;
    while (1) {
        x = x + 11;
    }
    return 0;
}

int _times() {
    volatile int x = 0;
    while (1) {
        x = x + 12;
    }
    return 0;
}

int _kill(int pid, int sig) {
    volatile int x = 0;
    while (1) {
        x = x + 13;
    }
    return 0;
}

int _getpid(void) {
    volatile int x = 0;
    while (1) {
        x = x + 14;
    }
    return 0;
}

void * malloc(size_t size) {
    return kmalloc(size);
}

void * realloc(void *ptr, size_t size) {
    int x = 1;
    while (1) {
        x += 2015;
    }
    return NULL;
}

void * _malloc_r(struct _reent *ptr, size_t size) {
    return kmalloc(size);
}

void free(void *addr) {
    kfree(addr);
}

void _free_r(struct _reent *ptr, void *addr) {
    kfree(addr);
}

void * _calloc_r(struct _reent *ptr, size_t size, size_t len) {
    volatile int x = 0;
    while (1) {
        x = x + 15;
    }
    return NULL;
}

void * _realloc_r(struct _reent *ptr, void *old, size_t newlen) {
    volatile int x = 0;
    while (1) {
        x = x + 16;
    }
    return NULL;
}
