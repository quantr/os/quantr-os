// we are using this dirent.h, peter

typedef struct dirent{
	int pfsDirectory;
	int d_off;
	char d_name[256];
} DIR;

// copy from library/libpeter/dirent.c
DIR *opendir(const char *filepath);
struct dirent * readdir(DIR * dir);
// end copy from library/libpeter/dirent.c
