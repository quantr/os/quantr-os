BEGIN { 
	separator=" --- "
}
{
	if (NF==4 && substr($1,1,1) == "." && substr($1,1,2) != ".."){
		printf $1;
		printf separator;
		printf $2;
		printf separator;
		printf $3;
		printf separator;
		printf $4;
		printf separator;
		printf "\n";
	}
}
END {}
