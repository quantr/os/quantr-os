#!/bin/bash

if [ $# -eq 1 ]; then
	path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	java -jar $path/peter-dwarf-*.jar $1 functions
else
	echo "usage: ./findAllFunctionNames.sh <elf file>"
fi

