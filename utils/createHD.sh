#!/bin/bash

base=/toolchain

rm -fr hd.img

os=`uname`

if [ "$os" = "Darwin" ]; then
	tempDeviceName=`hdiutil info|grep FDisk|cut -d$'\t' -f1`
	if [ "$tempDeviceName" != '' ]; then
		hdiutil eject $tempDeviceName
	fi
	commands=( `which dd` `which kpartx` $base/bin/grub-mkimage $base/sbin/grub-bios-setup )
	error="false"
	for c in ${commands[@]}; do
		if [ ! -x $c ]; then
		    echo "************************************************************";
		    echo "$c not found!";
		    echo "************************************************************";
		    error="true";
		fi
	done

	if [ "$error" == "true" ]; then
		exit;
	fi

	dd if=/dev/zero of=hd.img count=3072 bs=10240
	deviceName=`hdiutil attach -imagekey diskimage-class=CRawDiskImage -nomount hd.img`
	deviceName=${deviceName//[[:space:]]/}
	echo "deviceName="$deviceName

	umount temp
	rm -fr temp
	mkdir temp
	# create partition
	diskutil partitionDisk $deviceName MBRFormat "MS-DOS FAT32" "BOOT" "20M" "MS-DOS FAT32" "PETER" "10M"
	# end create partition
	newfs_msdos $deviceName"s1"
	mount -t msdos $deviceName"s1" temp
	mkdir -p temp/boot/grub
	mkdir -p temp/boot/grub/i386-pc/
	cp grub.cfg temp/boot/grub/
	echo "cp -frv $base/lib/grub/i386-pc/*.mod temp/boot/grub/i386-pc/"
	cp -frv $base/lib/grub/i386-pc/*.mod temp/boot/grub/i386-pc/
#	cp $base/lib/grub/i386-pc/boot.mod temp/boot/grub/i386-pc/
#	cp $base/lib/grub/i386-pc/multiboot.mod temp/boot/grub/i386-pc/
#	cp $base/lib/grub/i386-pc/help.mod temp/boot/grub/i386-pc/
#	cp $base/lib/grub/i386-pc/configfile.mod temp/boot/grub/i386-pc/
#	cp $base/lib/grub/i386-pc/net.mod temp/boot/grub/i386-pc/
#	cp $base/lib/grub/i386-pc/normal.mod temp/boot/grub/i386-pc/
#	cp $base/lib/grub/i386-pc/gettext.mod temp/boot/grub/i386-pc/
#	cp $base/lib/grub/i386-pc/priority_queue.mod temp/boot/grub/i386-pc/
	cp kernel/kernel temp/boot/

	# setup grub
	#echo "(hd0) /dev/hd0" > temp/boot/grub/device.map


	echo "set prefix=(hd0,msdos1)/boot/grub" > mycfg.cfg
	$base/bin/grub-mkimage --config=mycfg.cfg --directory '/toolchain/lib/grub/i386-pc' --prefix '/boot/grub' --output 'temp/boot/grub/i386-pc/core.img' --format 'i386-pc' --compression 'auto' 'fat' 'part_msdos' 'biosdisk' 'search_fs_uuid'
	ls -l temp/boot/grub/i386-pc/core.img
	cp $base/lib/grub/i386-pc/boot.img temp/boot/grub/i386-pc/
	#$base/sbin/grub-bios-setup --verbose --directory='temp/boot/grub/i386-pc' -c core.img -b boot.img --device-map='temp/boot/grub/device.map' $deviceName
	$base/sbin/grub-bios-setup --verbose --directory='temp/boot/grub/i386-pc' -c core.img -b boot.img ${deviceName/disk/rdisk}

#	sudo $base/sbin/grub-install --boot-directory=temp/boot -v $deviceName
	# end setup grub

	umount temp
	hdiutil eject $deviceName
	sudo rm -fr temp

#	host='192.168.56.101'
#
#	#fail mean ok, crazy
#	count=`ping -W 200 -c 1 $host | grep icmp | wc -l`
#	if [ "$count" -eq "1" ]; then
#	  echo "using $host"
#	else
#	  echo "using http://createHDWeb.kingofcoders.com"
#	  host="createHDWeb.kingofcoders.com"
#	fi
#
#	tar cjvf kernel.tar.bz2 -C kernel kernel
#	echo "uploading to $host";
#	curl http://$host --form grub=@grub.cfg --form kernel=@kernel.tar.bz2
#	rm -fr kernel.tar.bz2
#	echo "downloading http://$host/hd.img.tar.bz2";
#	wget http://$host/hd.img.tar.bz2
#	tar zxvf hd.img.tar.bz2
#	rm -fr hd.img.tar.bz2
else
	commands=( `which dd` `which kpartx` $base/bin/grub-mkimage $base/sbin/grub-bios-setup )
	error="false"
	for c in ${commands[@]}; do
		if [ ! -x $c ]; then
		    echo "************************************************************";
		    echo "$c not found!";
		    echo "************************************************************";
		    error="true";
		fi
	done
	if [ "$error" == "true" ]; then
		exit;
	fi

	dd if=/dev/zero of=hd.img count=3072 bs=10240
	kpartx -d hd.img
	umount temp
	rm -fr temp
	mkdir temp
	./utils/createPartition.sh

	# create partition for PFS, it is in second partition
	./utils/pt.sh -c hd.img 1 status=80 chs=2,ac,2b partitionType=fa lastchs=3,50,d lba=43008 noOfSector=2800
	kpartx -a hd.img
	sleep 1
	name=`kpartx -l hd.img | head -1|cut -d : -f 1`
	echo "name=$name"
	shortname=`kpartx -l hd.img | head -1|cut -d ' ' -f 5`
	echo "shortname=$shortname"
	mkfs.vfat /dev/mapper/$name
	mount /dev/mapper/$name temp
	mkdir -p temp/boot/grub
	cp grub.cfg temp/boot/grub/
	cp kernel/kernel temp/boot/

	# setup grub
	echo "(hd0) /dev/loop0" > device.map
	echo "set prefix=(hd0,msdos1)/boot/grub" > mycfg.cfg

	$base/bin/grub-mkimage --config=mycfg.cfg -p /boot -O i386-pc -o temp/boot/grub/core.img biosdisk part_msdos fat vbe vga echo configfile multiboot multiboot2
	cp $base/lib/grub/i386-pc/boot.img temp/boot/grub/

	$base/sbin/grub-bios-setup --no-rs-codes -m device.map -c ../../`pwd`/temp/boot/grub/core.img -b ../../toolchain/lib/grub/i386-pc/boot.img $shortname
	# end setup grub

	sleep 1
	umount temp
	sleep 1
	kpartx -d hd.img
	rm -fr temp
	rm -fr device.map
	rm -fr mycfg.cfg
fi

# checking
if [ "$os" = "Darwin" ]; then
	filesize=`stat -f %z hd.img`
else
	filesize=`stat --printf="%s" hd.img`
fi
if [ ! $filesize = 31457280 ]; then
	echo "***********************************************************************************************";
	echo "hd.img error, wrong size";
	echo "***********************************************************************************************";
fi
if [ ! -f "hd.img" ]; then
	echo "***********************************************************************************************";
	echo "hd.img error, it does not exist";
	echo "***********************************************************************************************";
fi
