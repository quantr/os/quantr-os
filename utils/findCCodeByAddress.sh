#!/bin/bash

if [ $# -eq 1 ]; then
	path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	java -jar $path/peter-dwarf-*.jar $path/../kernel/kernel findCCode $1
else
	echo "usage: ./findFunctionByAddress.sh <address>"
fi
