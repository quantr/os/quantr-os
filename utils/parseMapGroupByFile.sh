BEGIN {
	separator=" --- "
	lastFile=""
#	size=0
}
{
	#printf "\t\t\tPETER="file"="$0"=="NF"\n";
	if (NF==4 && substr($1,1,1) == "." && substr($1,1,2) != ".."){
		if (index($4, "(")==0){
			file=$4
		}else{
			file=substr($4, 0, index($4, "(")-1)
		}
		if (lastFile!=file){
			lastFile=file;
			printf $1;
			printf separator;
			printf $2;
			printf separator;
			printf $3;
			printf separator;
			printf file;
			printf separator;
			printf "\n";
			size=0
		}
	}
	#	num=$3+1
	#	if (num ~ /^[0-9]+$/ ){
	#		size+=num
	#		printf "size="size"\n";
	#	}
}
END {}
