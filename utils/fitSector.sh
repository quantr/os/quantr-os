#!/bin/bash
# make the filesize % 512 == 0
if [ $# == 1 ]; then
	bootimage_size=$(ls -l $1 | awk '{print $5}')
	bootimage_sector=$(expr $bootimage_size / 512)
	oldSize=$(expr $bootimage_sector \* 512)
	diffBytes=$(expr $bootimage_size - $oldSize)
	
	if [ $diffBytes != 0 ]; then
		realSize=$(expr $bootimage_sector + 1)
		realSize=$(expr $realSize \* 512)
		diffBytes=$(expr $realSize - $bootimage_size)
		dd if=/dev/zero of=temp count=1 bs=$diffBytes 2> /dev/null
		cat $1 temp > temp2
		mv temp2 $1
		rm -fr temp
	fi
else
	echo	"missing parameter";
	echo	"usage : ./fitSector.sh <filename>"
fi
