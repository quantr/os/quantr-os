find . -type f -name 'Makefile' | while read a; do
	sed 's/-O3/-O6 -fno-exceptions/g' $a > tmp && mv tmp $a
done
