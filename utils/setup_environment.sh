#!/bin/bash

if [ $# = 0 ]; then
    C_COMPILER_LOCATION=$(which gcc)
    CPP_COMPILER_LOCATION=$(which g++)
    LINKER_LOCATION=$(which ld)
	#LIBGCC_LOCATION=$(find /usr/lib/ -name "libgcc.a" | grep -v ming | tail -1)
	LIBGCC_LOCATION=$(gcc --print-file-name=libgcc.a)
elif [ $# = 1 ]; then
	# gcc
	gccArr=( `find $1 -name "*gcc*" | tr "," "\n"` )
	for ((x=0;x<${#gccArr[@]};x++)); do
		echo $x") "${gccArr[$x]};
	done
	echo -n "please select gcc ?"
	read opt
	C_COMPILER_LOCATION=${gccArr[$opt]}

	# g++
	gppArr=( `find $1 -name "*g++*" | tr "," "\n"` )
	echo
	if [ "$gpp" != "" ]; then
        for ((x=0;x<${#gppArr[@]};x++)); do
                echo $x") "${gppArr[$x]};
        done
        echo -n "please select g++ ?"
        read opt
        CPP_COMPILER_LOCATION=${gppArr[$opt]}
	fi

	# ld
        echo
        ldArr=( `find $1 -name "*ld*" | tr "," "\n"` )
        for ((x=0;x<${#ldArr[@]};x++)); do
                echo $x") "${ldArr[$x]};
        done
        echo -n "please select ld ?"
        read opt
        LINKER_LOCATION=${ldArr[$opt]}

        # libgcc.a
        libGcc=( `find $1 -name "*libgcc.a" | tr "," "\n"` )
        for ((x=0;x<${#libGcc[@]};x++)); do
                echo $x") "${libGcc[$x]};
        done
        echo
        echo -n "please select g++?"
        read opt
        LIBGCC_LOCATION=${libGcc[$opt]}
elif [ $# = 4 ]; then
        C_COMPILER_LOCATION=$1
        CPP_COMPILER_LOCATION=$2
        LINKER_LOCATION=$3
	LIBGCC_LOCATION=$4
else
	echo "usage :
		auto detect compiler and linker path : ./setup_environment
		specific a search path               : ./setup_environment <path to search>
		specific compiler and linker path    : ./setup_environment <gcc_path> <g++_path> <ld_path> <libgcc.a>"
	exit
fi

echo "C_COMPILER_LOCATION="$C_COMPILER_LOCATION
echo "CPP_COMPILER_LOCATION="$CPP_COMPILER_LOCATION
echo "LINKER_LOCATION="$LINKER_LOCATION
echo "LIBGCC_LOCATION="$LIBGCC_LOCATION
echo "ok? (y/n)"
read a;
if [ $a = "y" ]; then
	echo "processing..."
else
	exit;
fi

chmod +x utils/gen_source
chmod +x utils/report
chmod +x utils/insertData
chmod +x backup
chmod +x app/app_print/make_app_print
chmod +x utils/fitSector.sh

echo 'libgcc is '$LIBGCC_LOCATION
#find . -name "ls_*[big|small|plibc]" | while read x; do
#	echo "changing libgcc : "$x;
#	#ori=$(grep $x | sed 's/^.*{//' | sed 's/ (.*$//')
#	rm -fr temp
#	sed s@/usr/.*/libgcc.a@$LIBGCC_LOCATION@g "$x" > temp;
#	mv temp "$x";
#	rm -fr temp;
#done
sed s@/usr/.*/libgcc.a@$LIBGCC_LOCATION@g global_linker_script.ls > temp;
mv temp global_linker_script.ls;
rm -fr temp;

find . -name "*.cpp" -exec dos2unix {} \;
find . -name "make_interrupt*" -exec chmod +x {} \;
find . -name "gen_header" -exec chmod +x {} \;
find . -name "make_kernel" -exec chmod +x {} \;

find . -name "Makefile" | while read x; do
        echo "changing compiler path in Makefile : "$x;
        sed s@CC=.*@CC=$C_COMPILER_LOCATION@g "$x"
        sed s@CPP=.*@CPP=$CPP_COMPILER_LOCATION@g "$x"
        sed s@LD=.*@LD=$LINKER_LOCATION@g "$x"
done
#find . -name "*.nouse" -exec {} \;

