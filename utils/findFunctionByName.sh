#!/bin/bash

find /toolchain -name "*.[ao]" | while read a; do
	temp=`i586-peter-elf-nm $a | grep -i 'T '$1'$' | wc -l`
	if [ $temp -gt 0 ]; then
		echo $a
	fi
done
