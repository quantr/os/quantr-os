#!/bin/bash

if [ $# -eq 3 ]; then
	path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	java -jar $path/peter-dwarf-*.jar $1 findAddress $2 $3
else
	echo "usage: ./findAddress.sh <elf file> <filename> <line number>"
fi

