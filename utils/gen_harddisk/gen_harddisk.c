#include<stdio.h>
#include<stdlib.h>

int main(int argc,char **argv)
{
    if (argc > 2) {
	FILE *fp;
	if ((fp = fopen(argv[1], "wb+")) != NULL) {
		char c[2]={'0','\0'};
		int x;
		int size;
		sscanf(argv[2],"%d",&size);
		for (x=0;x<size*1024*1024;x++){
	    		fwrite(c, sizeof(char), 1, fp);
			if (c[0]!='9'){
				c[0]++;
			}else{
				c[0]='0';
			}
		}
	} else {
	    return 1;
	}
	fclose(fp);
	return 0;
    } else {
	printf("./gen_harddisk <filename> <size in MB>\n");
	return 2;
    }
}
