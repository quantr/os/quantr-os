#!/bin/bash

if [ $# != 4 ]; then
	echo "Usage : insertData.sh source dest <source skip position> <dest seek position>"
	exit;
fi

source_size=$(ls -l $1 | awk {'print $5'})
#dest_size=$(ls -l $2 | awk {'print $5'})
total_seek_size=$((source_size + $4))
#dd if=$2 of=insertData_temp skip=$total_seek_size ibs=1 obs=1MB > /dev/null
if [ $4 = 0 ]; then
	rm -fr first
	touch first
else
	echo "dd if=$2 of=first bs=$4 count=1";
	dd if=$2 of=first bs=$4 count=1
fi

#### back #####
echo "dd if=$2 of=back skip=1 bs=$total_seek_size";
dd if=$2 of=back skip=1 bs=$total_seek_size

cat first $1 back > $2

rm -fr first back
