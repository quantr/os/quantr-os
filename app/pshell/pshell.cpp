#include "pshell.h"

extern "C" {
#include <stdio.h>
#include <vfs/pfs/PFS_Directory.h>
#include <fcntl.h>
#include <system.h>
#include <stdbool.h>
#include <stdlib.h>
#include <io.h>
#include <dirent.h>
#include <ctype.h>
#include <vfs/pfs/pfs.h>
#include <vfs/pfs/PFS_File.h>
#include <gelf.h>
#include <stdint.h>
#include <unistd.h>
#include <display.h>
#include <string.h>
}

#include "../../kernel/PeterLoader.h"

#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

char currentDirectory[4096];
char currentUsername[4096];
char commandLineHistory[100][100];

#define PRINT_FMT   "    %-20s 0x%x\n"
#define PRINT_FMT2   "    %-20s 0x%x"
#define PRINT_FIELD(N)  printf(PRINT_FMT ,#N, (uintmax_t) ehdr.N);
#define PRINT_FIELD2(N) printf(PRINT_FMT2, #N, (uintmax_t) phdr.N);
#define NL()   printf("\n");

void loadELFExecutable(char *filePath, bool buildDescriptor, struct Page_Directory *pageDirectory, int *baseAddress);

unordered_map<string, int> loadedLibrariesPhysicalAddress;

void resolver() {
	int x = 11;
	while (1) {
		x += 0x1243;
	}
}

int testVarFunc() {
	int var1 = 1980;
	for (int y = 0; y < var1; y++) {
		int var2 = var1 + y*y;
	}
	int var4 = 2204;
	for (int y2 = 0; y2 < var4; y2++) {
		int var3 = var1 + y2*y2;
	}
	int var5 = 1234;
	var5 = var1*var4;
	return var5;
}

int login() {
	// read password file
	//File passwordFile = pfs.getFile("root/password");

	FILE *file = fopen("/config/passwords", "r");

	if (file != NULL) {
		//obtain file size
		fseek(file, 0, SEEK_END);
		//PFS_File *f=file->_file;
		long fileSize = ftell(file);


		rewind(file);
		char *buffer = (char *) malloc(fileSize);
		fread(buffer, 1, fileSize, file);
		fclose(file);
		//__asm __volatile__("XCHG %BX, %BX");
		printf("username :");
		fflush(stdout);
		char username[101];

		gets(username);

		printf("password :");
		fflush(stdout);
		char password[101];
		getlinePassword(password, 100, "*");

		// check the password
		char *cp = strdup(buffer);
		char delimiter[] = "\n";
		char *line;
		line = strtok(cp, delimiter);
		while (line != NULL) {
			int positionOfDelimiter = strchr(line, ':') - line;
			// check username
			if (strncmp(line, username, positionOfDelimiter) == 0 && strlen(username) == positionOfDelimiter) {
				// check password
				if (strcmp(line + positionOfDelimiter + 1, password) == 0) {
					strcpy(currentUsername, username);
					return 1;
				} else {
					free(line);
					break;
				}
			}
			free(line);
			line = strtok(NULL, delimiter);
		}
		return 0;
	} else {
		printf("Unable to open /config/passwords. Operating System Stop !!\n");
		while (1)
			;
		return 0;
	}
	return -1;
}

void dumphex(char *buffer, int len) {
	int x;
	for (x = 0; x < len; x++) {
		printf("%x ", buffer[x]);
	}
	printf("\n");
}

void print_ptype(size_t pt) {
	printf(" ");
	switch (pt) {
		case 0:
			printf("PT_NULL");
			break;
		case 1:
			printf("PT_LOAD");
			break;
		case 2:
			printf("PT_DYNAMIC");
			break;
		case 3:
			printf("PT_INTERP");
			break;
		case 4:
			printf("PT_NOTE");
			break;
		case 5:
			printf("PT_SHLIB");
			break;
		case 6:
			printf("PT_PHDR");
			break;
		case 7:
			printf("PT_TLS");
			break;
		case 8:
			printf("PT_NUM");
			break;
		default:
			printf("unknown");
			break;
	}
}

/*void mapPageIntoPageTables(struct Page_Directory *pageDirectories, int virtualAddress) {
	int pageDirectoryNo = virtualAddress >> 22;
	int ptNo = (virtualAddress >> 12) & 0x3ff;
	struct Page_Table *pageTables;
	struct Page_Directory *pageDirectory = &pageDirectories[pageDirectoryNo];
	if (pageDirectory->p == 0) {
		//pageTables = (struct Page_Table *) kmalloc();
		int sizeOfWholePageTables = 1024 * sizeof (struct Page_Table);
		pageTables = (struct Page_Table *) getFreePage(sizeOfWholePageTables);
		memset(pageTables, 0, sizeOfWholePageTables);
		pageDirectory->p = 1;
		pageDirectory->rw = 1;
		pageDirectory->pageTableBase = (unsigned int) pageTables >> 12;

		//map yourself
		printf("pageDirectories=%x\n", pageDirectories);
		printf("pageTables=%x\n", pageTables);
		int pageDirectoryNoTemp = (unsigned int) pageTables >> 22;
		int ptNoTemp = ((unsigned int) pageTables >> 12) & 0x3ff;
		printf("%d, %d\n", pageDirectoryNoTemp, ptNoTemp);
		struct Page_Directory *pageDirectoryTemp = &pageDirectories[pageDirectoryNoTemp];
		if (pageDirectoryTemp->p == 0) {
			printf("Error, page table itself not exist\n");
			while (1);
		} else {
			pageDirectoryTemp[pageDirectoryNoTemp].pageTableBase = (unsigned int) pageTables >> 12;
			struct Page_Table *pageTableTemp = (struct Page_Table *) (pageDirectoryTemp[pageDirectoryNoTemp].pageTableBase << 12);
			pageTableTemp[ptNoTemp].pageTableBase = (unsigned int) pageTables >> 12;
			pageTableTemp[ptNoTemp].p = 1;
			pageTableTemp[ptNoTemp].rw = 1;
		}
		//end map yourself
	} else {
		pageTables = (struct Page_Table *) (pageDirectory->pageTableBase << 12);
	}
	printf("x=%08x, %08x\n", virtualAddress, getPhysicalAddress(virtualAddress));
	pageTables[ptNo].pageTableBase = (unsigned int) virtualAddress >> 12;
	pageTables[ptNo].p = 1;
	pageTables[ptNo].rw = 1;
	//printf("??? %x,%x,%x\n",x,getPhysicalAddress(x), ptNo);
}*/

void mapAddressIntoPageTables(struct Page_Directory *pageDirectories, unsigned int virtualAddress, unsigned int physicalAddress) {
	//printf("map %x -> %x\n", virtualAddress, physicalAddress);
	virtualAddress = virtualAddress & 0xfffff000;
	physicalAddress = physicalAddress & 0xfffff000;
	unsigned int pageDirectoryNo = virtualAddress >> 22;
	unsigned int ptNo = (virtualAddress >> 12) & 0x3ff;
	//printf(" + virtualAddress=%x > %x, pdNo=%u, ptNo=%d, pd=%x p=%x\n", virtualAddress, physicalAddress, pageDirectoryNo, ptNo, pageDirectories, getPhysicalAddress(pageDirectories));

	struct Page_Table *pageTables;
	struct Page_Directory *pageDirectory = &pageDirectories[pageDirectoryNo];
	__native_flush_tlb_single(pageDirectory);

	//printf("  >> pageDirectory=%x, %x\n", pageDirectory, getPhysicalAddress(pageDirectory));
	//if (virtualAddress < KERNEL_RESERVED_MEMORY_FOR_UPPER1GB || virtualAddress > KERNEL_RESERVED_MEMORY_FOR_UPPER1GB_END) {
	//printf("         map v(%x) > p(%x), pd(%x)\n", virtualAddress, physicalAddress, pageDirectories);
	//}
	//	if (virtualAddress == KERNEL_RESERVED_MEMORY) {
	//		printf("++ 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, %d, %d, %d\n", pageDirectories, getPhysicalAddress(pageDirectories), pageDirectory, getPhysicalAddress(pageDirectory), virtualAddress, physicalAddress, pageDirectory->p, pageDirectoryNo, ptNo);
	//	}
	if (pageDirectory->p == 0) {
		printf("\t\t\tpageDirectory->p == 0\n");
		//pageTables = (struct Page_Table *) kmalloc();
		unsigned int sizeOfWholePageTables = 1024 * sizeof (struct Page_Table);
		pageTables = (struct Page_Table *) getFreePage(sizeOfWholePageTables);
		//printf("     pageTables=%x\n", pageTables);
		memset(pageTables, 0, sizeOfWholePageTables);
		pageDirectory->p = 1;
		pageDirectory->rw = 1;
		pageDirectory->pageTableBase = (unsigned int) pageTables >> 12;
		//		if (virtualAddress == KERNEL_RESERVED_MEMORY) {
		//			printf("--     0x%x, 0x%x\n", pageDirectory->pageTableBase, pageTables);
		//		}

		//map yourself
		unsigned int pageDirectoryNoTemp = (unsigned int) pageTables >> 22;
		struct Page_Directory *pageDirectoryTemp = &pageDirectories[pageDirectoryNoTemp];
		if ((char) pageDirectoryTemp->p == 0) {
			mapAddressIntoPageTables(pageDirectories, pageTables, pageTables);
			/*printf("  hey pageTables=%x\n", pageTables);
			pageDirectoryTemp[pageDirectoryNoTemp].pageTableBase = (int) pageTables >> 12;
			pageDirectoryTemp[pageDirectoryNoTemp].p = 1;

			//printf("base=%x\n", (pageDirectoryTemp[pageDirectoryNoTemp].pageTableBase << 12));
			struct Page_Table *pageTableTemp = (struct Page_Table *) (pageDirectoryTemp[pageDirectoryNoTemp].pageTableBase << 12);
			//printf("base2=%x\n", &(pageTableTemp[ptNoTemp]));
			pageTableTemp[ptNoTemp].pageTableBase = (int) pageTables >> 12;
			pageTableTemp[ptNoTemp].p = 1;
			pageTableTemp[ptNoTemp].rw = 1;*/
			//printf("             FUCK 3) 0x1b15000=%x\n", getPhysicalAddressByPD(pageDirectories, 0x1b15000));
		}
		//end map yourself
	} else {
		//		if (virtualAddress == KERNEL_RESERVED_MEMORY) {
		//			BOCHS_PAUSE
		//		}
		//printf("         FUCK p=1) 0x1b15000=%x, %x, %d, %x\n", getPhysicalAddressByPD(pageDirectories, 0x1b15000), pageTables, ptNo, getPageTableEntryAddress(KERNEL_PDs, pageTables));
		//printf("\t\t\t exist pageDirectory=%x, %x\n", pageDirectory, getPhysicalAddress(pageDirectory));
		pageTables = (struct Page_Table *) (pageDirectory->pageTableBase << 12);
		//printf("\t\t\t exist pageTables=%x, %x\n", pageTables, getPhysicalAddress(pageTables));

	}
	pageTables[ptNo].pageTableBase = (int) physicalAddress >> 12;
	//printf("\t\t\twrite %x\n", &(pageTables[ptNo]));
	pageTables[ptNo].p = 1;
	pageTables[ptNo].rw = 1;

	//if (virtualAddress == KERNEL_RESERVED_MEMORY) {
	//while (1);
	//}
	__native_flush_tlb_single(virtualAddress);
	//printf("         FUCK 1) 0x1b15000=%x, %x %x\n", getPhysicalAddressByPD(pageDirectories, 0x1b15000), pageDirectories, getPageTableEntryAddress(pageDirectories, 0x1b15000));
}

void loadLibrary(string libraryName, struct Page_Directory *pageDirectories, int *baseAddress) {
	printf("------------------------ loadLibrary %s -------\n", libraryName.c_str());
	loadedLibrariesPhysicalAddress[libraryName] = 123; //(int)baseAddress;
	string filePath = "/lib/" + libraryName;
	//loadELF(path.c_str(), false, pageDirectories, baseAddress, false);
	FILE *file = fopen(filePath.c_str(), "r");
	fseek(file, 0, SEEK_END);
	long fileSize = ftell(file);
	rewind(file);
	char *buffer = (char *) malloc(fileSize);
	fread(buffer, 1, fileSize, file);
	fclose(file);
	printf("fileSize=%d\n", fileSize);

	Elf *e;
	size_t n;
	GElf_Ehdr ehdr;
	GElf_Phdr phdr;
	Elf_Data *edata = NULL;
	Elf_Scn *scn;
	GElf_Shdr shdr;
	int fd;
	size_t shstrndx;
	printf("filePath=%s\n", filePath.c_str());
	if ((fd = open(filePath.c_str(), O_RDONLY, 0)) < 0) {
		printf("open %s failed\n", filePath);
		while (1);
	}

	if ((e = elf_begin(fd, ELF_C_READ, NULL)) == NULL) {
		printf("elf_begin() failed: %s\n", elf_errmsg(-1));
		while (1);
	}

	if (elf_getphdrnum(e, &n) != 0) {
		printf("elf_getphdrnum() failed: %s.\n", elf_errmsg(-1));
		while (1);
	}

	if (elf_getshdrstrndx(e, &shstrndx) != 0) {
		printf("getshdrstrndx() failed: %s.", elf_errmsg(-1));
		while (1);
	}

	// all PT_LOAD segments
	vector<GElf_Phdr> elfHeaders;
	for (int i = 0; i < n; i++) {
		if (gelf_getphdr(e, i, &phdr) != &phdr) {
			printf("getphdr() failed: %s.\n", elf_errmsg(-1));
			continue;
		}
		elfHeaders.push_back(phdr);
	}

	int totalSizeOfPTLoad = 0;
	for (vector<GElf_Phdr>::iterator it = elfHeaders.begin(); it != elfHeaders.end(); ++it) {
		if (it->p_type == PT_LOAD) {
			totalSizeOfPTLoad += it->p_memsz;
			printf("loading section %x, %x\n", it->p_vaddr, it->p_memsz);

			for (int index = 0; index < it->p_filesz; index += 4096) {
				int virtualAddressPageOffset = it->p_vaddr & 0xfff;
				char *tempBuffer = (char *) kmalloc(4096);

				int copySize = it->p_filesz - index;
				if (copySize > 4096) {
					copySize = 4096;
				}

				// we need tempBuffer + virtualAddressPageOffset in src field of memcpy, because mapping is 0xfff aligned
				memcpy(tempBuffer + virtualAddressPageOffset, buffer + it->p_offset + index, copySize);
				printf("baseAddress + it->p_vaddr + index = 0x%x\n", baseAddress + it->p_vaddr + index);
				mapAddressIntoPageTables(pageDirectories, baseAddress + it->p_vaddr + index, getPhysicalAddress(tempBuffer));
				//vaddrs.push_back((unsigned int) phdr.p_vaddr + index);
				//loadedPhysicalAddresses.push_back(getPhysicalAddress(tempBuffer));

			}
			//printf("LOAD %x - %x > %x - %x\n", it->p_offset, it->p_filesz, it->p_vaddr, it->p_paddr);
		}
	}

	// find .dynstr
	int dynstrIndex = -1;
	int x = 1;
	scn = NULL;
	while ((scn = elf_nextscn(e, scn)) != NULL) {
		if (gelf_getshdr(scn, &shdr) != &shdr) {
			printf("getshdr ()  failed : %s.", elf_errmsg(-1));
			while (1);
		}
		char *sectionName = elf_strptr(e, shstrndx, shdr.sh_name);
		if (strcmp(sectionName, ".dynstr") == 0) {
			dynstrIndex = x;
			break;
		}
		x++;
	}
	printf(".dynstr=%d\n", x);
	// end find .dynstr


	// load up all symbols to data structure
	edata = NULL;
	scn = NULL;
	while ((scn = elf_nextscn(e, scn)) != NULL) {
		if (gelf_getshdr(scn, &shdr) != &shdr) {
			printf("getshdr ()  failed : %s.", elf_errmsg(-1));
			while (1);
		}
		char *sectionName = elf_strptr(e, shstrndx, shdr.sh_name);
		printf(">>> %s, %x\n", sectionName, shdr.sh_type);
		if (shdr.sh_type == SHT_DYNSYM) {
			edata = elf_getdata(scn, edata);
			if (edata == NULL) {
				break;
			}
			for (int x = 0; x < edata->d_size; x += sizeof (Elf32_Sym)) {
				Elf32_Sym *dyn = edata->d_buf + x;
				char *name = elf_strptr(e, dynstrIndex, dyn->st_name);
				printf("    name=%s, off=%x\n", name, dyn->st_value);
			}
		}
	}

	// end load up all symbols to data structure
	*baseAddress += totalSizeOfPTLoad;
	// end all PT_LOAD segments
}

void loadELFExecutable(char *filePath, bool buildDescriptor, struct Page_Directory *pageDirectories) {
	printf("------------------ loadELF %s -------\n", filePath);
	int fd;
	Elf *e;
	char *id, bytes[5];
	size_t n;
	GElf_Ehdr ehdr;
	GElf_Phdr phdr;
	size_t shstrndx;

	printf("elf_version(EV_CURRENT)=%d\n", elf_version(EV_CURRENT));

	if (elf_version(EV_CURRENT) == EV_NONE) {
		printf("ELF initialization failed: %s\n", elf_errmsg(-1));
	}

	if ((fd = open(filePath, O_RDONLY, 0)) < 0) {
		printf("open %s failed\n", filePath);
	}
	printf("peter fd=%x\n", fd);

	if ((e = elf_begin(fd, ELF_C_READ, NULL)) == NULL) {
		printf("elf_begin() failed: %s\n", elf_errmsg(-1));
	}
	if (elf_kind(e) != ELF_K_ELF) {
		printf("\"%s\" is not an ELF object\n", filePath);
	}
	if (gelf_getehdr(e, &ehdr) == NULL) {
		printf("getehdr() failed: %s\n", elf_errmsg(-1));
	}
	int elfClass = gelf_getclass(e);
	if ((elfClass = gelf_getclass(e)) == ELFCLASSNONE) {
		printf("getclass() failed: %s\n", elf_errmsg(-1));
	}
	printf("%s: %d-bit ELF object\n", filePath, elfClass == ELFCLASS32 ? 32 : 64);
	if ((id = elf_getident(e, NULL)) == NULL) {
		printf("getident() failed: %s.", elf_errmsg(-1));
	}
	/*printf("%3s e_ident[0..%1d]%7s", " ", EI_ABIVERSION, " ");
	for (int i = 0; i <= EI_ABIVERSION; i++) {
		printf(" 0x%x", id[i]);
	}*/
	int e_class = id[EI_CLASS];
	if (elf_getshdrstrndx(e, &shstrndx) != 0) {
		printf("getshdrstrndx() failed: %s.", elf_errmsg(-1));
	}

	/*PRINT_FIELD(e_type);
	PRINT_FIELD(e_machine);
	PRINT_FIELD(e_version);
	PRINT_FIELD(e_entry);
	PRINT_FIELD(e_phoff);
	PRINT_FIELD(e_shoff);
	PRINT_FIELD(e_flags);
	PRINT_FIELD(e_ehsize);
	PRINT_FIELD(e_phentsize);
	PRINT_FIELD(e_shentsize);*/

	//	if (elf_getshdrnum(e, &n) != 0) {
	//		printf("getshdrnum() failed: %s.\n", elf_errmsg(-1));
	//	}
	//	printf(PRINT_FMT, "(shnum)", (uintmax_t) n);
	/*if (elf_getshdrstrndx(e, &n) != 0) {
		printf("getshdrstrndx() failed: %s.\n", elf_errmsg(-1));
	}*/
	//	printf(PRINT_FMT, "(shstrndx)", (uintmax_t) n);

	if (elf_getphdrnum(e, &n) != 0) {
		printf("elf_getphdrnum() failed: %s.\n", elf_errmsg(-1));
	}
	//printf(PRINT_FMT, "(phnum)", (uintmax_t) n);

	FILE *file = fopen(filePath, "r");
	fseek(file, 0, SEEK_END);
	long fileSize = ftell(file);
	rewind(file);
	char *buffer = (char *) malloc(fileSize);
	fread(buffer, 1, fileSize, file);
	fclose(file);

	//for (int i = 0; i < n; i++) {
	/*if (gelf_getphdr(e, i, &phdr) != &phdr) {
		printf("getphdr() failed: %s.\n", elf_errmsg(-1));
		continue;
	}*/

	/*printf("PHDR %d:\n", i);
	PRINT_FIELD2(p_type);
	print_ptype(phdr.p_type);
	NL();
	PRINT_FIELD2(p_offset);
	NL();
	PRINT_FIELD2(p_vaddr);
	NL();
	PRINT_FIELD2(p_paddr);
	NL();
	PRINT_FIELD2(p_filesz);
	NL();
	PRINT_FIELD2(p_memsz);
	NL();
	PRINT_FIELD2(p_flags);
	printf(" [");
	if (phdr.p_flags & PF_X)
		printf(" execute");
	if (phdr.p_flags & PF_R)
		printf(" read");
	if (phdr.p_flags & PF_W)
		printf(" write");
	printf(" ]");
	NL();
	PRINT_FIELD2(p_align);
	NL();

	if (phdr.p_type == 1) {
		printf("%x\n%x\n", phdr.p_offset, ehdr.e_ehsize);
		for (int x = 0x74; x < 0x80; x++) {
			printf("%x ", buffer[x] & 0xff);
		}
		printf("\n");
	}*/
	//}

	unsigned short ldtSelector;
	if (buildDescriptor) {
		// LDT
		char *ldtTable = (char *) kmalloc(4096);
		//printf("ldtTable=%x,%x\n", ldtTable, getPhysicalAddress(ldtTable));

		//printf("prepare LDT\n");
		//setDescriptor(ldtTable, 0, 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
		//setDescriptor(ldtTable + 8, 1, 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 2);
		setDescriptor((struct Descriptor *) ldtTable, 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
		setDescriptor((struct Descriptor *) (ldtTable + 8), 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 3);
		// set LDT into GDT
		struct Descriptor *gdt = getGDT();
		//printf("getNumberOfGDTDescriptor=%d\n", getNumberOfGDTDescriptor());
		setDescriptor((struct Descriptor *) &gdt[getNumberOfGDTDescriptor()], getPhysicalAddress(ldtTable), 2 * 8, 1, 1, 0, 0, 1, 0, 0, 2);
		setGDTR((char *) gdt, getNumberOfGDTDescriptor() + 1);
		ldtSelector = ((getNumberOfGDTDescriptor() - 1) << 3);
		// end set LDT into GDT

		// build page directories
		unsigned int numberOfPageTable = ((unsigned long long) 4 * 1024 * 1024 * 1024) / PAGE_SIZE;
		unsigned int numberOfPageDirectory = numberOfPageTable / 1024;
		pageDirectories = (struct Page_Directory *) malloc(numberOfPageDirectory * sizeof (struct Page_Directory));
		printf("f pageDirectories=0x%x, 0x%x, size=%x\n", pageDirectories, getPhysicalAddress(pageDirectories), numberOfPageDirectory * sizeof (struct Page_Directory));
		memset(pageDirectories, 0, numberOfPageDirectory * sizeof (struct Page_Directory));
		for (int x = 0; x < numberOfPageDirectory; x++) {
			pageDirectories[x].p = 0;
			pageDirectories[x].rw = 0;
			pageDirectories[x].us = 0;
			pageDirectories[x].pwt = 0;
			pageDirectories[x].pcd = 0;
			pageDirectories[x].a = 0;
			pageDirectories[x].d = 0;
			pageDirectories[x].reserved = 0;
			pageDirectories[x].g = 0;
			pageDirectories[x].avl = 0;
			struct Page_Table *pageTables = (struct Page_Table *) kmalloc(1024 * sizeof (struct Page_Table));
			memset(pageTables, 0, 1024 * sizeof (struct Page_Table));
			pageDirectories[x].pageTableBase = (int) pageTables >> 12;
			pageDirectories[x].pageTableBase = 0;
		}
		// end build page directories
		// map LDT
		mapAddressIntoPageTables(pageDirectories, ldtTable, getPhysicalAddress(ldtTable));
		// end map LDT
		// end LDT


		// map stack
		char *stack = (char *) kmalloc(8192);
		mapAddressIntoPageTables(pageDirectories, 0xa000000, getPhysicalAddress(stack));
		mapAddressIntoPageTables(pageDirectories, 0xa001000, getPhysicalAddress(stack) + 4096);
		// end map stack

		// map KERNEL_RESERVED_MEMORY
		for (int i = KERNEL_RESERVED_MEMORY; i <= KERNEL_RESERVED_MEMORY_END; i += 4096) {
			mapAddressIntoPageTables(pageDirectories, i, i);
		}
		// end map KERNEL_RESERVED_MEMORY

		// map KERNEL_RESERVED_MEMORY_FOR_UPPER1GB
		for (int i = KERNEL_RESERVED_MEMORY_FOR_UPPER1GB; i <= KERNEL_RESERVED_MEMORY_FOR_UPPER1GB_END; i += 4096) {
			mapAddressIntoPageTables(pageDirectories, i, i);
		}
		// end map KERNEL_RESERVED_MEMORY_FOR_UPPER1GB

		//printf("loader=0x%x\n", loader);
		//unsigned int loaderAddress = 0xc0000000 + ((unsigned int) loader & 0xfff);
		// + ((unsigned int) loader & 0xfff);
		//mapAddressIntoPageTables(pageDirectories, 0xc0000000, loader);
		//mapAddressIntoPageTables(pageDirectories, loader, loader);
		for (int i = KERNEL; i <= KERNEL_END; i += 4096) {
			mapAddressIntoPageTables(pageDirectories, i, i);
		}

		mapAddressIntoPageTables(pageDirectories, VIDEO_MEMORY_VIRTUAL_ADDRESS, VIDEOMEMORYPHYSICALADDRESS);
	}

	// all PT_LOAD segments
	vector<GElf_Phdr> elfHeaders;
	for (int i = 0; i < n; i++) {
		if (gelf_getphdr(e, i, &phdr) != &phdr) {
			printf("getphdr() failed: %s.\n", elf_errmsg(-1));
			continue;
		}
		elfHeaders.push_back(phdr);
	}

	// find .got.plt
	GElf_Shdr shdr;
	Elf_Scn *scn = NULL;
	int gotIndex = -1;
	int tempX = 1;
	unsigned int pltGotAddress;
	while ((scn = elf_nextscn(e, scn)) != NULL) {
		if (gelf_getshdr(scn, &shdr) != &shdr) {
			printf("getshdr() failed : %s.", elf_errmsg(-1));
		}
		char *sectionName = elf_strptr(e, shstrndx, shdr.sh_name);
		//printf("sec=%s\n", sectionName);
		if (strcmp(sectionName, ".got.plt") == 0) {
			gotIndex = tempX;
			Elf_Data *edata = NULL;
			edata = elf_getdata(scn, edata);
			//printf("       .got.plt addr=%x, sh_size=%x\n", shdr.sh_addr, shdr.sh_size);
			//printf("       type=%d\n", edata->d_size);
			pltGotAddress = shdr.sh_addr;
			break;
		}
		tempX++;
	}
	//printf(".gotIndex=%d\n", gotIndex);

	/*
		printf("DIU 1\n");
		for (vector<GElf_Phdr>::iterator it = elfHeaders.begin(); it != elfHeaders.end(); ++it) {
				printf("DIU = %x > %x, %x\n", it->p_vaddr, it->p_offset, it->p_memsz);

		}
		printf("DIU END\n");
	 */
	// end find .got.plt

	// find .dynamic
	scn = NULL;
	int dynstrIndex = -1;
	tempX = 1;
	while ((scn = elf_nextscn(e, scn)) != NULL) {
		if (gelf_getshdr(scn, &shdr) != &shdr) {
			printf("getshdr ()  failed : %s.", elf_errmsg(-1));
		}
		char *sectionName = elf_strptr(e, shstrndx, shdr.sh_name);
		if (strcmp(sectionName, ".dynstr") == 0) {
			dynstrIndex = tempX;
			break;
		}
		tempX++;
	}
	printf(".dynstrIndex=%d\n", dynstrIndex);
	// end find .dynamic

	// find .dynamic
	scn = NULL;
	int dynamicIndex = -1;
	tempX = 0;

	vector<string> neededLibraries;
	while ((scn = elf_nextscn(e, scn)) != NULL) {
		if (gelf_getshdr(scn, &shdr) != &shdr) {
			printf("getshdr() failed : %s.", elf_errmsg(-1));
		}
		char *sectionName = elf_strptr(e, shstrndx, shdr.sh_name);
		if (strcmp(sectionName, ".dynamic") == 0) {
			dynamicIndex = tempX;
			Elf_Data *edata = NULL;
			while (1) {
				edata = elf_getdata(scn, edata);
				if (edata == NULL) {
					break;
				}

				for (int x = 0; x < edata->d_size; x += sizeof (Elf32_Dyn)) {
					Elf32_Dyn *dyn = edata->d_buf + x;
					//printf("dyn->d_tag=%x\n", dyn->d_tag);
					if (dyn->d_tag == DT_NULL) {
						break;
					}
					char *value = elf_strptr(e, dynstrIndex, dyn->d_un.d_val);
					//printf("dyn->d_un.d_val=%x\n", dyn->d_un.d_val);
					//printf("dynstrIndex=%d\n", dynstrIndex);
					//printf("value=%s\n", value);
					if (dyn->d_tag == DT_NEEDED) {
						neededLibraries.push_back(string(value));
					}
				}
			}
			break;
		}

		tempX++;
	}

	printf("end find .dynamic\n");
	int temp = 0;
	int *baseAddress = &temp;
	for (std::vector<string>::iterator it = neededLibraries.begin(); it != neededLibraries.end(); ++it) {
		string libraryName = *it;
		loadLibrary(libraryName, pageDirectories, baseAddress);
	}

	//printf(".dynamicIndex=%d\n", dynamicIndex);
	// end find .dynamic

	//e = elf_begin(fd, ELF_C_READ, NULL);
	//    if (elf_kind(e) != ELF_K_ELF) {
	//        printf("\"%s\" is not an ELF object 2\n", commandLine);
	//    }
	//    if (gelf_getehdr(e, &ehdr) == NULL) {
	//        printf("getehdr() failed: %s\n", elf_errmsg(-1));
	//    }
	char *fuck = kmalloc(10);
	strcpy(fuck, "fuck");
	//printf("+FUCK ++++++++++++=\n");
	mapAddressIntoPageTables(pageDirectories, fuck, getPhysicalAddress(fuck));
	//printf("pageDirectories=%x, %x,  fuck=%x, %x\n", pageDirectories, getPhysicalAddress(pageDirectories), fuck, getPhysicalAddress(fuck));
	//printf("-FUCK ------------=\n");
	BOCHS_PAUSE

	for (vector<GElf_Phdr>::iterator it = elfHeaders.begin(); it != elfHeaders.end(); ++it) {
		if (it->p_type == PT_LOAD) {
			//printf("loading section %x, %x\n", it->p_vaddr, it->p_memsz);

			for (int index = 0; index < it->p_filesz; index += 4096) {
				int virtualAddressPageOffset = it->p_vaddr & 0xfff;
				char *tempBuffer = (char *) kmalloc(4096);
				//printf("tempBuffer=%x, %x\n", tempBuffer, getPhysicalAddress(tempBuffer));
				BOCHS_PAUSE
						int copySize = it->p_filesz - index;
				if (copySize > 4096) {
					copySize = 4096;
				}

				// we need tempBuffer + virtualAddressPageOffset in src field of memcpy, because mapping is 0xfff aligned
				memcpy(tempBuffer + virtualAddressPageOffset, buffer + it->p_offset + index, copySize);
				mapAddressIntoPageTables(pageDirectories, it->p_vaddr + index, getPhysicalAddress(tempBuffer));
				//vaddrs.push_back((unsigned int) phdr.p_vaddr + index);
				//loadedPhysicalAddresses.push_back(getPhysicalAddress(tempBuffer));

				if (it->p_vaddr <= pltGotAddress && pltGotAddress < (it->p_vaddr + it->p_memsz)) {
					//got[2]
					int offset = pltGotAddress - it->p_vaddr;
					//printf("bingo pltGotAddress=%x, %x, virtualAddressPageOffset=%x, offset=%x\n", pltGotAddress, it->p_vaddr, virtualAddressPageOffset, offset);
					int *t = (int *) (tempBuffer + virtualAddressPageOffset + offset + 8);
					//printf("tempBuffer=%x, %x\n", tempBuffer, getPhysicalAddress(tempBuffer));
					//printf("t=%x, %x\n", t, getPhysicalAddress(t));
					unsigned int loaderAddress = loader;
					printf("loader=0x%x\n", loader);
					*t = loaderAddress;

					//got[1]
					t = (int *) (tempBuffer + virtualAddressPageOffset + offset + 4);
					*t = fuck;
				}
			}
			//printf("LOAD %x - %x > %x - %x\n", it->p_offset, it->p_filesz, it->p_vaddr, it->p_paddr);
		}
	}
	// end all PT_LOAD segments

	/*for (int i = 0; i < n; i++) {
		if (gelf_getphdr(e, i, &phdr) != &phdr) {
			printf("getphdr() failed: %s.\n", elf_errmsg(-1));
			continue;
		}
		printf("phdr.p_type=%d\n", phdr.p_type);
		if (phdr.p_type == PT_LOAD) {
			//			if (GOT_PLT_ADDRESS >= phdr.p_vaddr && GOT_PLT_ADDRESS < phdr.p_vaddr + phdr.p_memsz) {
			//				int pAddress = getPhysicalAddress(pageDirectories, phdr.p_vaddr);
			//				int vAddress = getVirtualAddress(pAddress);
			//				vAddress[2] = 0x12345678;
			//				break;
			//			}
		}
	}*/

	// map resolver address

	// end map resolver address

	//__asm __volatile__("XCHG %BX, %BX");
	/*
	for (i = 0; i < n; i++) {
		if (gelf_getphdr(e, i, &phdr) != &phdr) {
			printf("getphdr() failed: %s.\n", elf_errmsg(-1));
			continue;
		}
		if (phdr.p_type == 1) {

			char *segment = buffer + phdr.p_offset;
			char *virtualAddress = phdr.p_vaddr;
			mapAddressIntoPageTables(pageDirectories, virtualAddress, stack);
			mapPageIntoPageTables(pageDirectories, virtualAddress);
		}
	}
	 */
	// end all segments

	int descriptorNo;
	if (buildDescriptor) {
		// build tss
		//unsigned int phyicalPageAddress = getFreePage(4096);
		struct TSS *tss = (struct TSS *) kmalloc_upper1GB(300);
		tss->backLink = 0x18;
		tss->ESP0 = 0xa000000 + 4096 - 4;
		tss->SS0 = 0xc;
		tss->ESP1 = 0xa000000 + 4096 - 4;
		tss->SS1 = 0xc;
		tss->ESP2 = 0xa000000 + 4096 - 4;
		tss->SS2 = 0xc;
		tss->CR3 = getPhysicalAddress(pageDirectories) & 0xfffff000;
		tss->EIP = ehdr.e_entry;
		tss->EFLAGS = 0x6;
		tss->EAX = 0;
		tss->ECX = 0;
		tss->EDX = 0;
		tss->EBX = 0;
		tss->ESP = 0xa000000 + 4096 - 4;
		tss->EBP = 0;
		tss->ESI = 0;
		tss->EDI = 0;
		tss->ES = 0xc;
		tss->CS = 0x4;
		tss->SS = 0xc;
		tss->DS = 0xc;
		tss->FS = 0xc;
		tss->GS = 0xc;
		tss->LDTR = ldtSelector;
		tss->T = 0;
		tss->IOPB = 0;
		// end build tss

		// prepare TSS descriptor in GDT
		printf("prepare TSS descriptor in GDT\n");
		printf("tss is in %x, %x\n", tss, getPhysicalAddress(tss));

		struct Descriptor *gdt = getGDT();

		//setGDTDescriptor(unsigned int number, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType)
		setDescriptor(&gdt[getNumberOfGDTDescriptor()], tss, 0x68, 0, 0, 0, 0, 1, 0, 0, 9);
		//setDescriptor(&GDTTableAddress[3], osTSS, 0x68, 0, 0, 0, 0, 1, 0, 0, 0x9);


		setGDTR((char *) gdt, (unsigned short) getNumberOfGDTDescriptor() + 1);
		descriptorNo = getNumberOfGDTDescriptor() - 1;
		printf("descriptorNo=%x\n", descriptorNo);
		// end prepare TSS descriptor in GDT
	}
	//load NEEDED library


	/*
	// find .dynstr
	int dynstrIndex = -1;
	int tempX = 0;
	GElf_Shdr shdr;
	Elf_Scn *scn;
	while ((scn = elf_nextscn(e, scn)) != NULL) {
		if (gelf_getshdr(scn, &shdr) != &shdr) {
			printf("getshdr ()  failed : %s.", elf_errmsg(-1));
		}
		char *sectionName = elf_strptr(e, shstrndx, shdr.sh_name);
		printf("sec=%s\n", sectionName);
		if (strcmp(sectionName, ".dynstr") == 0) {
			dynstrIndex = tempX;
			break;
		}

		tempX++;
	}
	printf(".dynstr=%d\n", tempX);
	// end find .dynstr

	Elf_Data *edata = NULL;
	scn = NULL;
	while ((scn = elf_nextscn(e, scn)) != NULL) {
		if (gelf_getshdr(scn, &shdr) != &shdr) {
			printf("getshdr ()  failed : %s.", elf_errmsg(-1));
		}
		char *sectionName = elf_strptr(e, shstrndx, shdr.sh_name);
		//printf("%s\n", sectionName);
		if (strcmp(sectionName, ".dynamic") == 0) {
			while (1) {
				edata = elf_getdata(scn, edata);
				if (edata == NULL) {
					break;
				}
				printf("       type=%d  ", edata->d_type);
				for (int x = 0; x < 10; x++) {
					printf("%x ", ((char *) edata->d_buf)[x]);
				}
				printf("sizeof Elf32_Dyn=%d, no of Elf32_Dyn=%d\n", sizeof (Elf32_Dyn), shdr.sh_size / sizeof (Elf32_Dyn));
				if (shdr.sh_type == SHT_DYNAMIC) {
					for (int x = 0; x < edata->d_size; x += sizeof (Elf32_Dyn)) {
						Elf32_Dyn *dyn = (Elf32_Dyn *) edata->d_buf + x;
						if (dyn->d_tag == DT_NULL) {
							break;
						}
						char *value = elf_strptr(e, dynstrIndex, dyn->d_un.d_val);
						if (dyn->d_tag == DT_NEEDED) {
							printf("            NEEDED tag=%x, off=%x, %s\n", dyn->d_tag, dyn->d_un.d_val, value);
							loadLibrary(value);
						}
					}
				}
				printf("\n");
			}
			break;
		}
	}*/
	//end load NEEDED library

	elf_end(e);
	close(fd);

	BOCHS_PAUSE
	if (buildDescriptor) {
		__asm__ __volatile__(
				"movl %0, segment1\n"
				".byte   0xea\n"
				".long   0x0	# offset\n"
				"segment1:\n"
				".word   %0	# segment"
				:
				: "r"(descriptorNo << 3));
	}
}

void lsCurrentDirectory() {
	struct dirent *dir = opendir(currentDirectory);

	struct dirent *de;
	while ((de = readdir(dir)) != NULL) {
		if (!strcmp(".", de->d_name) || !strcmp("..", de->d_name)) {
			continue;
		} else {

			printf("%-16s\n", de->d_name);
		}
	}
	free(dir);
}

void ls(char *commandLine) {
	if (strlen(commandLine) >= 4) {
		char *cp = strdup(commandLine);
		char delimiter[] = " ";
		char *filename = strtok(cp, delimiter);
		filename = strtok(NULL, delimiter);
		free(filename);
		while (filename != NULL) {
			struct dirent *dir = opendir(filename);
			free(filename);
			filename = strtok(NULL, delimiter);
			if (dir == NULL) {
				continue;
			} else {
				struct dirent *de;
				while ((de = readdir(dir)) != NULL) {
					if (!strcmp(".", de->d_name) || !strcmp("..", de->d_name)) {
						continue;
					} else {
						printf("%-16s", de->d_name);
					}
				}
			}
			free(dir);
		}
	}
	printf("\n");
}

void cd(char *directoryPath) {
	char *path = (char *) malloc(strlen(currentDirectory) + 1 + strlen(directoryPath) + 1);
	//memset(path, 0, strlen(currentDirectory) + 1 + strlen(directoryPath) + 1);
	if (strlen(path) + strlen(currentDirectory) < 1024) {
		//__asm __volatile__("XCHG %BX, %BX");
		if (directoryPath[0] == pfs_getSeparator()) {
			path[0] = pfs_getSeparator();
			path[1] = '\0';
		} else {
			strcpy(path, currentDirectory);
			if (currentDirectory[strlen(currentDirectory) - 1] != pfs_getSeparator()) {
				int len = strlen(path);
				path[len] = pfs_getSeparator();
				path[len + 1] = '\0';
			}
			strcat(path + strlen(path), directoryPath);
		}
		//printf("path=%s\n",path);
		DIR *dir = opendir(path);
		if (dir == NULL) {
			printf("directory not exist !!!\n");
		} else {
			PFS_Directory *pfsDirectory = (PFS_Directory *) dir->pfsDirectory;

			pfs_setCurrentDirectoryBlockNo(pfsDirectory->blockNumber);
			strcpy(currentDirectory, path);
			//printf("currentDirectory=%s\n", currentDirectory);
		}

		free(path);
		free(dir);
	} else {

		printf("error : cd(), parmeter *directoryPath too long\n");
	}
	free(path);
}

void pwd() {
	printf("%s\n", currentDirectory);
}

void filesize(char *commandLine) {
	if (strlen(commandLine) >= 10) {
		char *cp = strdup(commandLine);
		char delimiter[] = " ";
		char *filename = strtok(cp, delimiter);
		free(filename);
		filename = strtok(NULL, delimiter);
		while (filename != NULL) {
			FILE *fp = fopen(filename, "r");
			if (fp != NULL) {
				fseek(fp, 0, SEEK_END);
				long fileSize = ftell(fp);
				rewind(fp);
				printf("%s, %u\n", filename, fileSize);
				fclose(fp);
			} else {

				printf("file : %s not found !!\n", filename);
			}

			free(filename);
			filename = strtok(NULL, delimiter);
		}
	}
}

void cat(char *commandLine) {
	if (strlen(commandLine) >= 5) {
		char *cp = strdup(commandLine);
		char delimiter[] = " ";
		char *filename = strtok(cp, delimiter);
		filename = strtok(NULL, delimiter);
		while (filename != NULL) {
			FILE *fp = fopen(filename, "r");
			if (fp != NULL) {
				//fseek(fp, 3903093, SEEK_SET);
				//fseek(fp, 6688305, SEEK_SET);
				//fseek(fp, 10485504, SEEK_SET);
				//fseek(fp, 1814184, SEEK_SET);
				char buffer[1000];
				int x;
				while ((x = fread(buffer, 1, 999, fp)) > 0) {
					if (x == -1) {
						buffer[0] = '\0';
					} else {
						buffer[x] = '\0';
					}
					printf("%s", buffer);
					// check buffer
					/*for (int xx = 0; xx < x; xx++) {
					 if (buffer[xx] < '0' || buffer[xx] > '9') {
					 printf("error, buffer error=%d\n", xx);
					 printf("len=%d", x);
					 while (1);
					 }
					 } */
					// printf("->%d bytes read\n", x);
					// end check buffer
				}
				//printf("final x=%d\n", x);
				printf("\n");
				fclose(fp);
			} else {

				printf("file : %s not found !!\n", filename);
			}

			free(filename);
			filename = strtok(NULL, delimiter);
		}
	}
}

void whoami() {
	printf("%s\n", currentUsername);
}

void turnOnCapsLock() {
	outport(0x64, 0xed); //Tell you want to control LEDs
	outport(0x60, 0x4); // setCaps LED on
}

void test() {
	printf("start test\n");
	printf("end test\n");
}

void printFSCache() {
	//	PFS_Main::getPFSMain()->printFSCache();
}

void welcome() {
	printf("               ,_   .  ._. _.  .\n");
	printf("           , _-\\','|~\\~      ~/      ;-'_   _-'     ,;_;_,    ~~-\n");
	printf("  /~~-\\_/-'~'--' ~~| ',    ,'      /  / ~|-_\\_/~/~      ~~--~~~~'--_\n");
	printf("  /              ,/'-/~ '\\ ,' _  , '|,'|~                   ._/-, /~\n");
	printf("  ~/-'~\\_,       '-,| '|. '   ~  ,\\ /'~                /    /_  /~\n");
	printf(".-~      '|        '',\\~|\\       _\\~     ,_  ,               /|\n");
	printf("          '\\        /'~          |_/~\\,-,~  \\ '         ,_,/ |\n");
	printf("           |       /            ._-~'\\_ _~|              \\ ) /\n");
	printf("            \\   __-\\           '/      ~ |\\  \\_          /  ~\n");
	printf("  .,         '\\ |,  ~-_      - |          \\_' ~|  /\\  \\~ ,\n");
	printf("               ~-_'  _;       '\\           '-,   \\,' /\\/  |\n");
	printf("                 '\\_,~'\\_       \\_ _,       /'    '  |, /|'\n");
	printf("                   /     \\_       ~ |      /         \\  ~'; -,_.\n");
	printf("                   |       ~\\        |    |  ,        '-_, ,; ~ ~\\\n");
	printf("                    \\,      /        \\    / /|            ,-, ,   -,\n");
	printf("                     |    ,/          |  |' |/          ,-   ~ \\   '.\n");
	printf("                    ,|   ,/           \\ ,/              \\       |\n");
	printf("                    /    |             ~                 -~~-, /   _\n");
	printf("                    |  ,-'                                    ~    /\n");
	printf("                    / ,'                                      ~\n");
	printf("                    ',|  ~\n");
	printf("                      ~'\n");
}

void china() {
	printf("                                               ****\n");
	printf("                       *****                  ******\n");
	printf("                     ******                 ************\n");
	printf("               **************                ***********\n");
	printf("               ****************          **************\n");
	printf("               ********************* *****************\n");
	printf("                  ****************************** **\n");
	printf("                *****************************\n");
	printf("               *********************************\n");
	printf("                 ******************************\n");
	printf("                   *****************************\n");
	printf("                           *********************\n");
	printf("                             ********************\n");
	printf("                            *******************    *\n");
	printf("                            ******************   **\n");
	printf("                             **    *********     *\n");
	printf("                                      **\n");
}

void chinaFlag() {
	printf("________________________\n");
	printf("|     *                 |\n");
	printf("|  *   *                |\n");
	printf("|      *                |\n");
	printf("|    *                  |\n");
	printf("|                       |\n");
	printf("|                       |\n");
	printf("------------------------\n");
}

/*
 void elf_example1(char * commandLine) {
 char *cp = strdup(commandLine);
 char delimiter[] = " ";
 char *filename = strtok(cp, delimiter);
 free(filename);
 filename = strtok(NULL, delimiter);

 int i, fd;
 Elf *e;
 char *id, bytes[6];
 size_t n;

 GElf_Ehdr ehdr;

 if (elf_version(EV_CURRENT) == EV_NONE) {
 errx(1, "ELF library initialization failed: %s", elf_errmsg(-1));
 }

 if ((e = elf_begin(fd, ELF_C_READ, NULL)) == NULL) {
 errx(1, "elf_begin() failed: %s.", elf_errmsg(-1));
 }

 if (elf_kind(e) != ELF_K_ELF) {
 errx(1, "\"%s\" is not an ELF object.", filename);
 }

 if (gelf_getehdr(e, &ehdr) == NULL) {
 errx(1, "getehdr() failed: %s.", elf_errmsg(-1));
 }

 if ((i = gelf_getclass(e)) == ELFCLASSNONE) {
 errx(1, "getclass() failed: %s.", elf_errmsg(-1));
 }

 printf("%s: %d-bit ELF object\n", filename, i == ELFCLASS32 ? 32 : 64);

 if ((id = elf_getident(e, NULL)) == NULL) {
 errx(1, "getident() failed: %s.", elf_errmsg(-1));
 }

 printf("%3s e_ident[0..%1d] %7s", " ", EI_ABIVERSION, " ");

 for (i = 0; i <= EI_ABIVERSION; i++) {
 // vis(bytes, id[i], VIS_WHITE, 0);
 printf(" [%c %X]", id[i], id[i]);
 }

 printf("\n");

 #define        PRINT_FORMAT        "    %-20s 0x%jx\n"
 #define        PRINT_FIELD(N)	printf(PRINT_FORMAT ,#N, (uintmax_t) ehdr.N);

 PRINT_FIELD(e_type);
 PRINT_FIELD(e_machine);
 PRINT_FIELD(e_version);
 PRINT_FIELD(e_entry);
 PRINT_FIELD(e_phoff);
 PRINT_FIELD(e_shoff);
 PRINT_FIELD(e_flags);
 PRINT_FIELD(e_ehsize);
 PRINT_FIELD(e_phentsize);
 PRINT_FIELD(e_shentsize);

 if (elf_getshdrnum(e, &n) != 0) {
 errx(1, "elf_getshdrnum() failed: %s, %d.", elf_errmsg(-1), n);
 }
 printf(PRINT_FORMAT, "(shnum)", (uintmax_t) n);

 if (elf_getshdrstrndx(e, &n) != 0) {
 errx(1, "getshstrndx() failed: %s.", elf_errmsg(-1));
 }
 printf(PRINT_FORMAT, "(shstrndx)", (uintmax_t) n);

 if (elf_getphdrnum(e, &n) != 0) {
 errx(1, "getphnum() failed: %s.", elf_errmsg(-1));
 }
 printf(PRINT_FORMAT, "(phnum)", (uintmax_t) n);

 elf_end(e);
 close(fd);
 exit(0);
 }
 */

void lseek_sample() {
	printf("lseek_sample()\n");
	int fd, i, rev;
	char buf[] = "Hello world!";

	fd = open("test.txt", O_WRONLY | O_CREAT); //以可读写的方式打开一个文件，如果不存在则创建该文件

	for (i = 0; i < 10; i++) {

		write(fd, buf, sizeof (buf));
		lseek(fd, -5, SEEK_CUR);
	}

	close(fd);
}

void testelf(char *file) {
	int i, fd;
	Elf *e;
	size_t n;

	fd = open(file, O_RDONLY, 0);
	if ((e = elf_begin(fd, ELF_C_READ, NULL)) == NULL) {
		printf("elf_begin() failed: %s.", elf_errmsg(-1));
	}
	if (elf_getphdrnum(e, &n) != 0) {

		printf("elf_getphdrnum() failed: %s.", elf_errmsg(-1));
	}
	printf("n=%d\n", n);
}

void pshell() {
	// init
	//	currentDirectory[0] = PFS_Main::getPFSMain()->getSeparator();
	currentDirectory[0] = pfs_getSeparator();
	currentDirectory[1] = '\0';
	// end init

	struct Page_Directory *pageDirectories;
	memset(pageDirectories, 0, 1024 * sizeof (struct Page_Directory));
	loadELFExecutable("/lib/test.exe", true, pageDirectories);

	while (1) {
		while (!login()) {
			printf("Login fail\n");
		}
		//china();
		cd("/");

		char buffer[101];
		while (1) {
			printf("%s>", currentDirectory);
			gets(buffer);
			if (strlen(buffer) > 0) {
				if (strcmp(buffer, "ls") == 0) {
					lsCurrentDirectory();
				} else if (strncmp((char *) buffer, (char *) "ls", 2) == 0) {
					ls(buffer);
				} else if (strncmp((char *) buffer, (char *) "cd ", 3) == 0) {
					if (strcspn(buffer, " ") != strlen(buffer)) {
						char *cp = strdup(buffer);
						char delimiter[] = " ";
						char *directoryPath;
						directoryPath = strtok(cp, delimiter);
						directoryPath = strtok(NULL, delimiter);
						cd(directoryPath);
						free(directoryPath);
					} else {
						// goto home directory
					}
				} else if (strcmp((char *) buffer, (char *) "pwd") == 0) {
					pwd();
				} else if (strncmp((char *) buffer, (char *) "cat ", 4) == 0) {
					cat(buffer);
				} else if (strncmp((char *) buffer, (char *) "test", 4) == 0) {
					test();
				} else if (strncmp((char *) buffer, (char *) "rm ", 3) == 0) {
					//rm(buffer);
				} else if (strcmp(buffer, "clear") == 0) {
					setCursor(0, 0);
					clearScreen();
				} else if (strcmp(buffer, "logout") == 0) {
					break;
				} else if (strcmp(buffer, "china") == 0) {
					china();
				} else if (strcmp(buffer, "whoami") == 0) {
					whoami();
				} else if (strcmp(buffer, "capson") == 0) {
					turnOnCapsLock();
				} else if (strcmp(buffer, "printscache") == 0) {
					printFSCache();
				} else if (strcmp(buffer, "chinaflag") == 0) {
					chinaFlag();
				} else if (strncmp((char *) buffer, (char *) "filesize ", 9) == 0) {
					filesize(buffer);
					//} else if (strncmp((char *) buffer, (char *) "peterloader ", 11) == 0) {
					//    peterloader(buffer + 11);
				} else if (strcmp((char *) buffer, (char *) "elf1") == 0) {
					//elf_example1(buffer);
				} else if (strcmp((char *) buffer, (char *) "lseek") == 0) {
					printf("run la\n");
					lseek_sample();
				} else if (strncmp((char *) buffer, (char *) "testelf ", 8) == 0) {
					testelf(buffer + 8);
				} else {
					printf("command not found\n");
				}
			}
		} // end while
	} // end while
} //end pshell
