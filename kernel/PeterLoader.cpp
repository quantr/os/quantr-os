/* 
 * File:   PeterLoader.cpp
 * Author: peter
 * Created on 2016/2/11, 1:27am
 */

#include "PeterLoader.h"
extern "C" {
#include <system.h>
#include <stdio.h>
}

/*
PeterLoader::PeterLoader() {
}

PeterLoader::PeterLoader(const PeterLoader& orig) {
}

PeterLoader::~PeterLoader() {
}
*/

void loader(unsigned int parameterOffset) {
    // __asm__ __volatile__("popl %k1" : "+r" (got1));
	unsigned int got1 = *(&parameterOffset - 1);
    unsigned int offset = *(&parameterOffset - 0);
    //BOCHS_PAUSE
    printf("got1 = %x\n", got1);
    printf("offset = %x\n", offset);
	//printf("fuck\n");
    BOCHS_PAUSE
	//while(1);
    __asm__ __volatile__(
        "nop\n"
        "leave\n"
    );

    //pop the things that pushed by PLT[n] and PLT[0]
    __asm__ __volatile__(
        "pop %eax\n"
        "pop %eax\n"
    );

}
