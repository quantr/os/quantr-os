#include "../app/pshell/pshell.h"

extern "C" {
	#include "header.h"
	#include <stdio.h>
	#include <display.h>
	#include <system.h>
	#include <string.h>
	#include <io.h>
	#include <general.h>
	#include <vfs/vfs.h>
	#include <xml.h>
	#include <unistd.h>

	//#include "../interrupt/interrupt8/interrupt8.h"
	//#include "../interrupt/interrupt9/interrupt9.h"

	//extern finish_timer_interrupt () asm ("finish_timer_interrupt");
	extern void * interrupt0();
	extern void * int7_isr();
	extern void * finish_timer_interrupt();
	extern void * finish_keyboard_interrupt();

	/*struct _reent{

	 };

	 struct _reent *
	 _DEFUN_VOID(__getreent)
	 {
	 return 0x12345678;
	 }*/

#include <reent.h>
	void peter_init(unsigned long magic, unsigned long addr);
	//void start_peter(unsigned long magic, unsigned long addr);
	void start_peter();
	//static struct _reent global_reent;
	//struct _reent p_reentp;
}

//#include <libxml/parser.h>
//#include <libxml/tree.h>

#define MINUTE  60L    /* Seconds in a minute          */
#define HOUR    (60L * MINUTE) /* Seconds in an hour           */
#define DAY     (24L * HOUR) /* Seconds in a day             */
#define YEAR    (365L * DAY) /* Seconds in a year            */
#define LYEAR   (366L * DAY) /* Seconds in a leap year       */
#define TO_BCD(x) x = ((x / 10) << 4) | (x % 10)
#define FROM_BCD(x)       x = ((x >> 4) * 10) + (x & 0x0f)
#define REDUCE(x,y)       while (stime >= y) {stime -= y; x++;}
#define CMOS_CLOCK        0x70 /* CMOS Clock port              */
#define CMOS_CLOCK1       0x71 /* CMOS Clock port              */

#define PARTITIONOFFSET 6291456
//#define PARTITIONOFFSET 0x037931e00

struct {
	int tm_sec; /* Number of seconds in BCD     */
	int tm_min; /* Number of minutes in BCD     */
	int tm_hour; /* Number of hours in BCD       */
	int tm_mday; /* Month day in BCD             */
	int tm_mon; /* Month in BCD                 */
	int tm_year; /* Year in BCD                  */
	int tm_century; /* Century in BCD               */
} RC_time;

struct Page_Directory *pageDirectories = (struct Page_Directory *) KERNEL_PDs;
struct Page_Table *pageTables = (struct Page_Table *) KERNEL_PTs;
char *freePageList = (char *) FREEPAGELIST;
char *freePageList_upper_1GB = (char *) FREEPAGELIST_UPPER_1GB;
struct VirutalAddressBlock *freeVirtualAddressList = (struct VirutalAddressBlock *) FREEVIRTUALADDRESSLIST;
struct VirutalAddressBlock *usedVirtualAddressList = (struct VirutalAddressBlock *) USEDVIRTUALADDRESSLIST;
struct VirutalAddressBlock *freeVirtualAddressList_upper1GB = (struct VirutalAddressBlock *) FREEVIRTUALADDRESSLIST_UPPER1GB;
struct VirutalAddressBlock *usedVirtualAddressList_upper1GB = (struct VirutalAddressBlock *) USEDVIRTUALADDRESSLIST_UPPER1GB;

struct Descriptor *GDTTableAddress;

void setupPageTable();

void setupGDT();
void setupIDT();
//void setupPageTable();
void setupTSS();

void enableNE();

/*__attribute__((section(".startup")))*/

char *osName = "Peter Operating System";
char *kernelName = "SL";
char *kernelVersion = "2016";


struct _reent reent; /* every process needs one instance of reent */
//extern void __init();

void peter_init(unsigned long magic, unsigned long addr) {
	__asm__ __volatile__("finit");
	__asm__ __volatile__("call _init\n");
	start_peter();
}

void enableNE() {
	__asm__ __volatile__(
			"movl	%cr0,%eax;"
			"or		$0x20,%eax;"
			"movl	%eax,%cr0"
			);
}

void enableMP() {
	__asm__ __volatile__(
			"movl	%cr0,%eax;"
			"or		$0x2,%eax;"
			"movl	%eax,%cr0"
			);
}

void start_peter() {
	enableNE();
	enableMP();
	setupPageTable();
	//dumpOSStatus();

	k_clearScreen();

	//printf("Entering kernel\n");

	//printf("disableAllIRQs\n");

	disableAllIRQs();

	//printf("enableIRQ(0)\n");
	enableIRQ(0);
	//printf("enableIRQ(1)\n");
	enableIRQ(1);
	memset(getKeyboardBuffer(), 0, 8);
	setupGDT();
	setupTSS();
	setupIDT();
	ltr(4); // this line have to call after setupIDT(). It need the tss setup before calling it.

	//_REENT_INIT_PTR(&reent);

	//	detectVesa();

	//__asm__ __volatile__("sti");

	__asm__ __volatile__("mov $0x20,%al");
	__asm__ __volatile__("out %al,$0x20");

	__asm__ __volatile__("mov $0x11,%al");
	__asm__ __volatile__("out %al,$0x20");
	//__asm__ __volatile__("out %al,$0xA0");
	//				; don't do this; unless you reprogram the 2nd 8259 too

	__asm__ __volatile__("mov $0x8,%al");
	//				; IRQ0-IRQ7 <-- interrupt number
	__asm__ __volatile__("out %al,$0x21");

	__asm__ __volatile__("mov $4, %al");
	__asm__ __volatile__("out %al,$0x21");

	__asm__ __volatile__("mov $1,%al");
	__asm__ __volatile__("out %al,$0x21");

	__asm__ __volatile__("mov $0xfc,%al");
	//				; IRQ0 (timer) and IRQ1 (keyboard)
	__asm__ __volatile__("out %al,$0x21");
	__asm__ __volatile__("sti");

	printf("---------------- Detect CPU model ----------------------\n");
	// check cpuid
	cpuid_regs_t regs, regs_ext;
	char idstr[13];

	unsigned int max_cpuid;
	unsigned int max_ext_cpuid;

	char *model_name = "Unknown CPU";
	unsigned int amd_flags;
	int i;

	char processor_name[49];
	regs = cpuid(0);
	max_cpuid = regs.eax;
	store(regs.ebx, idstr);
	store(regs.edx, idstr + 4);
	store(regs.ecx, idstr + 8);
	idstr[12] = '\0';
	printf("vendor_id\t: %s\n", idstr);
	if (strcmp(idstr, "GenuineIntel") == 0) {
		model_name = "Unkown Intel CPU";
	} else if (strcmp(idstr, "AuthenticAMD") == 0) {
		model_name = "Unknown AMD CPU";
	}
	regs_ext = cpuid((1 << 31) + 0);
	max_ext_cpuid = regs_ext.eax;
	if (max_ext_cpuid >= (unsigned int) (1 << 31) + 1) {
		regs_ext = cpuid((1 << 32) + 1);
		amd_flags = regs_ext.edx;
		if (max_ext_cpuid >= (1 << 31) + 4) {
			for (i = 2; i <= 4; i++) {
				regs_ext = cpuid((1 << 31) + i);
				store(regs_ext.eax, processor_name + (i - 2) * 16);
				store(regs_ext.ebx, processor_name + (i - 2) * 16 + 4);
				store(regs_ext.ecx, processor_name + (i - 2) * 16 + 8);
				store(regs_ext.edx, processor_name + (i - 2) * 16 + 16);
			}
			processor_name[48] = 0;
			model_name = processor_name;
		}
	} else {
		amd_flags = 0;
	}

	if (max_cpuid >= 1) {

		static struct {
			int bit;
			char *desc;
			char *description;
		} cap[] = {
			{ 0, "fpu", "Floating-point unit on-chip"},
			{ 1, "vme", "Virtual Mode Enhancements"},
			{ 2, "de", "Debugging Extension"},
			{ 3, "pse", "Page Size Extension"},
			{ 4, "tsc", "Time Stamp Counter"},
			{ 5, "msr", "Pentium Processor MSR"},
			{ 6, "pae", "Physical Address Extension"},
			{ 7, "mce", "Machine Check Exception"},
			{ 8, "cx8", "CMPXCHG8B Instruction Supported"},
			{ 9, "apic", "On-chip CPIC Hardware Enabled"},
			{ 11, "sep", "SYSENTER and SYSEXIT"},
			{ 12, "mtrr", "Memory Type Range Registers"},
			{ 13, "pge", "PTE Global Bit"},
			{ 14, "mca", "Machine Check Architecture"},
			{ 15, "cmov", "Conditional Move/Compare Instruction"},
			{ 16, "pat", "Page Attribute Table"},
			{ 17, "pse", "Page Size Extension"},
			{ 18, "psn", "Processor Serial Number"},
			{ 19, "cflsh", "CFLUSH instruction"},
			{ 21, "ds", "Debug Store"},
			{ 22, "acpi", "Thermal Monitor and Clock Ctrl"},
			{ 23, "mmx", "MMX Technolog"},
			{ 24, "fxsr", "FXSAVE/FXRSTOR"},
			{ 25, "sse", "SSE Extensions"},
			{ 26, "sse2", "SSE2 Extensions"},
			{ 27, "ss", "Self Snoop"},
			{ 29, "tm", "Therm. Monitor"},
			{ -1}
		};

		static struct {
			int bit;
			char *desc;
			char *description;
		} cap_amd[] = {
			{ 22, "mmxext", "MMX Technology (AMD Extensions)"},
			{ 30, "3dnowext", "3Dnow! Extensions"},
			{ 31, "3dnow", "3Dnow!"},
			{ -1}
		};
		int i;
		regs = cpuid(1);
		printf("cpu family\t: %d\n" "model\t\t: %d\n" "stepping\t: %d\n", (regs.eax >> 8) & 0xf, (regs.eax >> 4) & 0xf, regs.eax & 0xf);
		printf("flags\t\t:");
		for (i = 0; cap[i].bit >= 0; i++) {
			if (regs.edx & (1 << cap[i].bit)) {
				printf(" %s", cap[i].desc);
			}
		}
		for (i = 0; cap_amd[i].bit >= 0; i++) {
			if (amd_flags & (1 << cap_amd[i].bit)) {
				printf(" %s", cap_amd[i].desc);
			}
		}
		printf("\n");
		if (regs.edx & (1 << 4)) {
			unsigned long long tsc_start, tsc_end;

			//struct timeval tv_start, tv_end; int usec_delay;
			unsigned long long tv_start, tv_end;
			int usec_delay;

			//printf("tsc_start=%d\n",tsc_start);
			tv_start = getTimeOfDay();
			tsc_start = rdtsc();

			//printf("tv_start=%d\n",tv_start);
			//printf("start sleep\n");
			sleep(1);

			//			__asm__ __volatile__("xchg %bx,%bx");
			//printf("stop sleep\n");
			tsc_end = rdtsc();
			//printf("finished rdtsc\n");

			//printf("tsc_end=%d\n",tsc_end);
			//			__asm__ __volatile__("xchg %bx,%bx");
			tv_end = getTimeOfDay();
			//			__asm__ __volatile__("xchg %bx,%bx");
			//printf("finished getTimeOfDay\n");

			//printf("tv_end=%d\n",tv_end);

			//usec_delay = 1000000 * (tv_end - tv_start) + (tv_end - tv_start);
			//double ddd=(tsc_end-tsc_start);

			printf("tsc_start=%llu\n", tsc_start);
			printf("tsc_end=%llu\n", tsc_end);
			printf("tsc_end-tsc_start=%llu\n", tsc_end - tsc_start);
			printf("tv_start=%llu\n", tv_start);
			printf("tv_end=%llu\n", tv_end);
			//			__asm__ __volatile__("xchg %bx,%bx");
			printf("tv_end-tv_start=%llu\n", tv_end - tv_start);

			int hertz;
			if ((tv_end - tv_start) > 0) {
				hertz = (tsc_end - tsc_start) / (tv_end - tv_start) / 1000000;
			}
			//			__asm__ __volatile__("xchg %bx,%bx");
			printf("cpu MHz\t\t: %d\n", hertz);
		}
	}


	//	__asm__ __volatile__("xchg %bx,%bx");
	printf("model name\t: %s\n", model_name);
	// end check cpuid

	////////////////////////////// harddisk informations /////////////////////
	printf("-------------------------------------------------------\n");
	printf("Harddisk size = %d MB , %d Bytes\n", getMaxCylinder() * getMaxHead() * getMaxSector() * 512 / 1024 / 1024);
	printf("Max Cylinder = %d\n", getMaxCylinder());
	printf("Max Head = %d\n", getMaxHead());
	printf("Max Sector = %d\n", getMaxSector());

	printf("partition type=%s\n", vfs_detectPatitionType());

	///////////////////////////// SB Block /////////////////////////////////////////
	/*printf("---------------------- PFS ----------------------------\n");
	 printf("init end\n");

	 SuperBlock *superBlock = PFS_Main::getPFSMain()->getSuperBlock();
	 printf("SB ID=%s\n", superBlock->getID());
	 printf("SB partition name=%s\n", superBlock->getPartitionName());
	 printf("SB root directory link=%llu\n", superBlock->getRootDirectoryLink());
	 printf("SB create time=%llu\n", superBlock->getCreateTime());
	 printf("SB last modified time=%llu\n", superBlock->getLastModifiedTime());
	 printf("SB block size=%u\n", superBlock->getBlockSize());
	 printf("SB number of free address block=%llu\n", superBlock->getNumberOfFreeAddressBlock());*/
	////////////////////////// End SB Block ////////////////////////////////////////
	/* sscanf example */
	/*char sentence []="Rudolph is 12 years old";
	 char str [20];
	 int iii;
	 printf("step 1\n");
	 sscanf (sentence,"%s %*s %d",str,&iii);
	 printf("step 2\n");
	 printf ("%s -> %d\n",str,iii);
	 printf("step 3\n");
	 while(1);*/
	printf("get into pshell\n");

	//	printf("global_reent=%x\n", &global_reent);

	/*struct _reent p_reent;
	 struct _reent * p_reentp;
	 p_reentp = &p_reent;
	 _REENT_INIT_PTR(p_reentp);*/

	//_REENT_INIT_PTR(&p_reentp);
	/*struct _reent reent;
	 _REENT_INIT_PTR((&(reent)));

	 _impure_ptr = &reent;
	 printf("&reent=%x\n", &reent);*/

	//_GLOBAL_REENT->__sglue=0x123;
	//printf("p_reentp->__sglue addr=%x\n",&(p_reentp->__sglue));
	/*_REENT_INIT_PTR(_GLOBAL_REENT);
	 struct _glue *g;
	 FILE *fp;
	 int n;
	 int x = 0;
	 //	__asm__ __volatile__("xchg %bx,%bx");
	 printf("_niobs=%d\n",_GLOBAL_REENT->__sglue._niobs);
	 printf("_niobs=%x\n",&(_GLOBAL_REENT->__sglue._niobs));

	 */
	/*for (g = &_GLOBAL_REENT->__sglue;; g = g->_next) {
	 printf("    g->_niobs=%d\n", g->_niobs);
	 printf("    g->_niobs=%x\n", &(g->_niobs));
	 while(1);
	 for (fp = g->_iobs, n = g->_niobs; --n >= 0; fp++) {
	 printf("%d>fp->_flags=%d\n", n, fp->_flags);
	 if (fp->_flags == 0) {
	 printf("__sfp found\n");
	 while(1);
	 }
	 }
	 //		if (g->_next == NULL && (g->_next = __sfmoreglue(d, NDYNAMIC)) == NULL) {
	 //			printf("__sfp break\n");
	 //			break;
	 //		}
	 }
	 while (1)
	 ;*/

	pshell();

	while (1)
		;
} // end start_peter

void setupPageTable() {
	////////////////////// Info ///////////////////////////////////////
	// PD0 is started at 0x200000, and reserved 5MB
	// 4GB memory have 1048576 PT and 1024 PD, total bytes = 4100 KB,
	// so 5MB is enough for a computer with 4GB memory
	//////////////////// end Info /////////////////////////////////////

	/////////////////// Memory layout of PD and PT //////////////////////////
	//  0x20000 -> all PD -> all PT
	//////////////// end Memory layout of PD and PT /////////////////////////
	// Build FreePageList //
	unsigned int x;
	for (x = 0; x < (KERNEL_RESERVED_MEMORY_END - KERNEL_RESERVED_MEMORY + 1) / 4096; x++) {
		*(freePageList + x) = 0;
	}
	// end build FreePageList //
	// Manage virtual addresses //

	//printf("Build freeVirtualAddressList array - ");
	for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
		(freeVirtualAddressList + x)->offset = 0;
		(freeVirtualAddressList + x)->size = 0;
	}
	(freeVirtualAddressList + 0)->offset = KERNEL_RESERVED_MEMORY;
	(freeVirtualAddressList + 0)->size = KERNEL_RESERVED_MEMORY_END - KERNEL_RESERVED_MEMORY;
	//printf("success\n");

	//printf("Build usedVirtualAddressList array - ");
	for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
		(usedVirtualAddressList + x)->offset = 0;
		(usedVirtualAddressList + x)->size = 0;
	}
	//printf("success\n");

	// Build freePageList_upper_1GB //
	for (x = 0; x < (KERNEL_RESERVED_MEMORY_FOR_UPPER1GB_END - KERNEL_RESERVED_MEMORY_FOR_UPPER1GB + 1) / 4096; x++) {
		*(freePageList_upper_1GB + x) = 0;
	}
	// end build freePageList_upper_1GB //
	//printf("Build freeVirtualAddressList_upper1GB array - ");
	for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
		(freeVirtualAddressList_upper1GB + x)->offset = 0;
		(freeVirtualAddressList_upper1GB + x)->size = 0;
	}
	(freeVirtualAddressList_upper1GB + 0)->offset =
			KERNEL_RESERVED_MEMORY_FOR_UPPER1GB; // upper 1GB
	(freeVirtualAddressList_upper1GB + 0)->size = 1024 * 1024 * 1024; // 1GB
	//printf("success\n");

	//printf("Build usedVirtualAddressList_upper1GB array - ");
	for (x = 0; x < 1024 * 1024 / sizeof (struct VirutalAddressBlock); x++) {
		(usedVirtualAddressList_upper1GB + x)->offset = 0;
		(usedVirtualAddressList_upper1GB + x)->size = 0;
	}
	//printf("success\n");

	// end Manage virtual addresses //

	///////////////////// Map all the memory into Pag Directory and Page Table //////////////
	//printf("///////////////// Build Kernel PD & PT ///////////////\n");
	//printf("Map all the memory\n");
	unsigned baseAddress = 0;
	//unsigned int numberOfPageTable = ((unsigned long long) 4 * 1024 * 1024
	//		* 1024) / PAGE_SIZE; //getMemorySize() / PAGE_SIZE;
	unsigned int numberOfPageTable = ((unsigned long long) 64 * 1024 * 1024) / PAGE_SIZE; //getMemorySize() / PAGE_SIZE;
	//printf("Number Of Page Table=%u\n", numberOfPageTable);
	unsigned int numberOfPageDirectory = numberOfPageTable / 1024;
	//printf("Number Of Page Directory=%u\n", numberOfPageDirectory);
	unsigned int numberOfPageTableMapped = 0;
	for (x = 0; x < numberOfPageDirectory; x++) {
		//printf("\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r");
		//printf("Mapped %d/%d Page Directory", (x + 1), numberOfPageDirectory);
		unsigned int y;
		for (y = 0; numberOfPageTableMapped < numberOfPageTable && y < 1024; y++, numberOfPageTableMapped++) {
			//printf("y=%d\n",y);
			int tempOffset = x * 1024 + y;
			pageTables[tempOffset].p = 1;
			pageTables[tempOffset].rw = 1;
			pageTables[tempOffset].us = 0;
			pageTables[tempOffset].pwt = 0;
			pageTables[tempOffset].pcd = 0;
			pageTables[tempOffset].a = 0;
			pageTables[tempOffset].d = 0;
			pageTables[tempOffset].pat = 0;
			pageTables[tempOffset].g = 0;
			pageTables[tempOffset].avl = 0;
			pageTables[tempOffset].pageTableBase = baseAddress >> 12;
			baseAddress += 0x1000;
		}
		// Map Page table address to the corresponding page directory
		pageDirectories[x].p = 1;
		pageDirectories[x].rw = 1;
		pageDirectories[x].us = 0;
		pageDirectories[x].pwt = 0;
		pageDirectories[x].pcd = 0;
		pageDirectories[x].a = 0;
		pageDirectories[x].d = 0;
		pageDirectories[x].reserved = 0;
		pageDirectories[x].g = 0;
		pageDirectories[x].avl = 0;
		pageDirectories[x].pageTableBase = ((unsigned int) (&(pageTables[x * 1024]))) >> 12;
	}
	//printf("\n");
	//////////////////// enable new page structure //////////////////////////////////////////
	//printf("Turn on Paging : ");
	__asm__ __volatile__(
			"movl	$0x1100000,%%eax;"
			"movl	%%eax,%%cr3;"
			"movl	%%cr0,%%eax;"
			"or		$0x80000000,%%eax;"
			"movl	%%eax,%%cr0"::"a"(pageDirectories[0])
			);
	/*
		__asm__ __volatile__(
				"movl $0x10,%eax;"
				"movl %eax,%ds;"
				"movl %eax,%es;"
				"movl %eax,%fs;"
				"movl %eax,%gs;"
				"ljmp $0x8, 1;"
		);
		__asm__("1:");
	 */
	//printf("success\n");
	//////////////// end enable new page structure //////////////////////////////////////////

	// Map video memory to VIDEO_MEMORY_VIRTUAL_ADDRESS
	kmalloc2(VIDEO_MEMORY_VIRTUAL_ADDRESS, VIDEOMEMORYPHYSICALADDRESS,
			VIDEO_MEMORY_VIRTUAL_ADDRESS_SIZE);
}

void setupGDT() {
	// we have to map to upper 1GB linear memory to physical memory, and use that upper memory to store GDT, otherwise task switch will crazy if the liner-to-physical mapping is different for GDT (that mean GDT have to be the same for every task
	GDTTableAddress = (struct Descriptor *) kmalloc_upper1GB(65536);
	memset(GDTTableAddress, 0, 65536);

	//GDT *gdt = GDT::getGDT();
	//gdt->initGDT((unsigned int) GDTTableAddress, getPhysicalAddress((int *) GDTRAddress));

	//gdt->setDescriptor(unsigned int number, unsigned int baseAddress, unsigned int segmentLimit, unsigned char G, unsigned char D, unsigned char reserved0, unsigned char AVL, unsigned char P, unsigned char DPL, unsigned char S, unsigned char segmentType);
	/*GDTTableAddress[0]->byte0=0;
	 GDTTableAddress[0]->byte1=0;
	 GDTTableAddress[0]->byte2=0;
	 GDTTableAddress[0]->byte3=0;
	 GDTTableAddress[0]->byte4=0;
	 GDTTableAddress[0]->byte5=0;
	 GDTTableAddress[0]->byte6=0;
	 GDTTableAddress[0]->byte7=0;*/

	setDescriptor(&GDTTableAddress[0], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	setDescriptor(&GDTTableAddress[1], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	setDescriptor(&GDTTableAddress[2], 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	setDescriptor(&GDTTableAddress[3], 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 3);

	// os tss
	struct TSS *osTSS = (struct TSS *) kmalloc_upper1GB(4096);
	osTSS->backLink = 0;
	osTSS->ESP0 = 0;
	osTSS->SS0 = 0;
	osTSS->ESP1 = 0;
	osTSS->SS1 = 0;
	osTSS->ESP2 = 0;
	osTSS->SS2 = 0;
	osTSS->CR3 = KERNEL_PDs;
	osTSS->EIP = 0;
	osTSS->EFLAGS = 0;
	osTSS->EAX = 0;
	osTSS->ECX = 0;
	osTSS->EDX = 0;
	osTSS->EBX = 0;
	osTSS->ESP = 0;
	osTSS->EBP = 0;
	osTSS->ESI = 0;
	osTSS->EDI = 0;
	osTSS->ES = 0;
	osTSS->CS = 0;
	osTSS->SS = 0;
	osTSS->DS = 0;
	osTSS->FS = 0;
	osTSS->GS = 0;
	osTSS->LDTR = 0;
	osTSS->T = 0;
	osTSS->IOPB = 0;
	//GDT *gdt = GDT::getGDT();
	//gdt->addGDTDescriptor((unsigned int) osTSS, 104, 0, 0, 0, 0, 1, 0, 0, 9);
	setDescriptor(&GDTTableAddress[4], (unsigned int) osTSS, 0x68, 0, 0, 0, 0, 1, 0, 0, 0x9);
	// end os tss

	// setup timer interrupt
	setDescriptor(&GDTTableAddress[5], 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	setDescriptor(&GDTTableAddress[6], 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 3);
	struct TSS *timerTSS = (struct TSS *) kmalloc_upper1GB(300);
	timerTSS->backLink = 0;
	timerTSS->ESP0 = 0x900000 + 0x3000;
	timerTSS->SS0 = 6 << 3;
	timerTSS->ESP1 = 0x900000 + 0x4000;
	timerTSS->SS1 = 6 << 3;
	timerTSS->ESP2 = 0x900000 + 0x5000;
	timerTSS->SS2 = 6 << 3;
	timerTSS->CR3 = KERNEL_PDs;
	timerTSS->EIP = (unsigned long) finish_timer_interrupt + 1;
	timerTSS->EFLAGS = 0;
	timerTSS->EAX = 0;
	timerTSS->ECX = 0;
	timerTSS->EDX = 0;
	timerTSS->EBX = 0;
	timerTSS->ESP = 0x900000 + 0x6000;
	timerTSS->EBP = 0;
	timerTSS->ESI = 0;
	timerTSS->EDI = 0;
	timerTSS->ES = 6 << 3;
	timerTSS->CS = 5 << 3;
	timerTSS->SS = 6 << 3;
	timerTSS->DS = 6 << 3;
	timerTSS->FS = 6 << 3;
	timerTSS->GS = 6 << 3;
	timerTSS->LDTR = 0;
	timerTSS->T = 0;
	timerTSS->IOPB = 0;
	setDescriptor(&GDTTableAddress[7], (unsigned int) timerTSS, 0x68, 0, 0, 0, 0, 1, 0, 0, 0x9);

	// setup keyboard interrupt
	setDescriptor(&GDTTableAddress[8], 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	setDescriptor(&GDTTableAddress[9], 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 3);
	struct TSS *keyboardTSS = (struct TSS *) kmalloc_upper1GB(300);
	keyboardTSS->backLink = 0;
	keyboardTSS->ESP0 = 0x908000 + 0x3000;
	keyboardTSS->SS0 = 9 << 3;
	keyboardTSS->ESP1 = 0x908000 + 0x4000;
	keyboardTSS->SS1 = 9 << 3;
	keyboardTSS->ESP2 = 0x908000 + 0x5000;
	keyboardTSS->SS2 = 9 << 3;
	keyboardTSS->CR3 = KERNEL_PDs;
	keyboardTSS->EIP = (unsigned long) finish_keyboard_interrupt + 1;
	keyboardTSS->EFLAGS = 0;
	keyboardTSS->EAX = 0;
	keyboardTSS->ECX = 0;
	keyboardTSS->EDX = 0;
	keyboardTSS->EBX = 0;
	keyboardTSS->ESP = 0x908000 + 0x6000;
	keyboardTSS->EBP = 0;
	keyboardTSS->ESI = 0;
	keyboardTSS->EDI = 0;
	keyboardTSS->ES = 9 << 3;
	keyboardTSS->CS = 8 << 3;
	keyboardTSS->SS = 9 << 3;
	keyboardTSS->DS = 9 << 3;
	keyboardTSS->FS = 9 << 3;
	keyboardTSS->GS = 9 << 3;
	keyboardTSS->LDTR = 0;
	keyboardTSS->T = 0;
	keyboardTSS->IOPB = 0;
	setDescriptor(&GDTTableAddress[10], (unsigned int) keyboardTSS, 0x68, 0, 0, 0, 0, 1, 0, 0, 0x9);
	//setDescriptor((unsigned int) timerTSS, 104, 0, 0, 0, 0, 1, 0, 0, 9);

	//gdt->setDescriptor(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	//gdt->setDescriptor(1, 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	//gdt->setDescriptor(2, 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 2);

	setGDTR((char *) GDTTableAddress, 11);

	/*printf("GDTr address field : ");
	 printf("%d , %X\n", gdt->getGDTRBase(), gdt->getGDTRBase());
	 printf("GDTr size field : ");
	 printf("%d\n", gdt->getGDTRLimit());
	 printf("GDTr address: ");
	 printf("%d , %X\n", gdt->getGDTR(), gdt->getGDTR());
	 printf("GDT address : ");
	 printf("%d, %X\n", gdt->getGDTTable(), gdt->getGDTTable());
	 printf("Memory Size : %u Bytes , %u MB, 0x0-%x\n", getMemorySize(), getMemorySize() / 1024 / 1024, getMemorySize() - 1);*/

}

void setupIDT() {
	// construct IDT record
	unsigned char *IDTTable = (unsigned char *) kmalloc_upper1GB(65536);
	//BOCHS_PAUSE
	printf("IDTTable=0x%x\n", IDTTable);
	//BOCHS_PAUSE

	setIDTDescriptor(&IDTTable[0], (unsigned int) interrupt0, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[1 * 8], INTERRUPT1, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[2 * 8], INTERRUPT2, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[3 * 8], INTERRUPT3, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[4 * 8], INTERRUPT4, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[5 * 8], INTERRUPT5, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[6 * 8], INTERRUPT6, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[7 * 8], (unsigned int) int7_isr, 2 << 3, 0x8e, 0, 0);
	//setIDTDescriptor(&IDTTable[8 * 8], INTERRUPT8, 2 << 3, 0x8e, 0, 0);
	//setIDTDescriptor(&IDTTable[9 * 8], INTERRUPT9, 2 << 3, 0x8e, 0, 0);
	setIDTTaskGateDescriptor(&IDTTable[8 * 8], 7, 0, 1);
	setIDTTaskGateDescriptor(&IDTTable[9 * 8], 10, 0, 1);
	setIDTDescriptor(&IDTTable[10 * 8], INTERRUPTA, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[11 * 8], INTERRUPTB, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[12 * 8], INTERRUPTC, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[13 * 8], INTERRUPTD, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[14 * 8], (unsigned int) int7_isr, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[15 * 8], (unsigned int) int7_isr, 2 << 3, 0x8e, 0, 0);
	setIDTDescriptor(&IDTTable[16 * 8], (unsigned int) int7_isr, 2 << 3, 0x8e, 0, 0);

	int numberOfIDTDescriptor = 17;

	// setup IDTR register
	char *IDTR = (char *) kmalloc(6);

	IDTR[0] = 0;
	IDTR[1] = 0;
	IDTR[2] = (unsigned int) IDTTable;
	IDTR[3] = (unsigned int) IDTTable >> 8;
	IDTR[4] = (unsigned int) IDTTable >> 16;
	IDTR[5] = (unsigned int) IDTTable >> 24;
	*IDTR = (unsigned short) (8 * numberOfIDTDescriptor) - 1;
	__asm__ __volatile__("lidt (%%eax)"::"a"(IDTR));

	/*__asm__ __volatile__("mov	$0x20, %al");
	 __asm__ __volatile__("outb	%al, $0x20");
	 __asm__ __volatile__("mov	$0x10, %al");
	 __asm__ __volatile__("outb	%al, $0x21");
	 __asm__ __volatile__("mov	$0x4, %al");
	 __asm__ __volatile__("outb	%al, $0x21");
	 __asm__ __volatile__("mov	$0x1, %al");
	 __asm__ __volatile__("outb	%al, $0x21");
	 __asm__ __volatile__("mov	$0xfd, %al");
	 __asm__ __volatile__("outb	%al, $0x21");*/

	/*
	 ;mov	al, 0x20
	 ;out	0x20, al

	 ;mov	al,0x11
	 ;out	0x20,al
	 ;out 0xA0,al				; don't do this; unless you reprogram the 2nd 8259 too

	 ;mov	al,0x10				; IRQ0-IRQ7 <-- interrupt number
	 ;out	0x21,al

	 ;mov	al,4
	 ;out	0x21,al

	 ;mov	al,1
	 ;out	0x21,al

	 ;mov	al,0xFd				; IRQ0 (timer) and IRQ1 (keyboard)
	 ;out	0x21,al
	 ;sti
	 */

	//GDT *gdt = GDT::getGDT();
	/*if (IDTTableAddress != NULL && IDTRAddress != NULL && gdt != NULL) {
	 printf("IDTRAddress=%x\n", IDTRAddress);
	 //IDT *idt = IDT::getIDT();
	 //idt->initIDT((unsigned int) IDTTableAddress, getPhysicalAddress((int *) IDTRAddress));
	 //gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 //setDescriptor(GDTTableAddress[0], 0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);

	 idt->addIDTDescriptor(INTERRUPT0, (gdt->getNumberOfDescriptor() - 1) << 3, 0x8e, 0, false);
	 gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 idt->addIDTDescriptor(INTERRUPT1, (gdt->getNumberOfDescriptor() - 1) << 3, 0x8e, 0, false);
	 gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 idt->addIDTDescriptor(INTERRUPT2, (gdt->getNumberOfDescriptor() - 1) << 3, 0x8e, 0, false);
	 gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 idt->addIDTDescriptor(INTERRUPT3, (gdt->getNumberOfDescriptor() - 1) << 3, 0x8e, 0, false);
	 gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 idt->addIDTDescriptor(INTERRUPT4, (gdt->getNumberOfDescriptor() - 1) << 3, 0x8e, 0, false);
	 gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 idt->addIDTDescriptor(INTERRUPT5, (gdt->getNumberOfDescriptor() - 1) << 3, 0x8e, 0, false);
	 gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 idt->addIDTDescriptor(INTERRUPT6, (gdt->getNumberOfDescriptor() - 1) << 3, 0x8e, 0, false);
	 gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 idt->addIDTDescriptor(INTERRUPT7, (gdt->getNumberOfDescriptor() - 1) << 3, 0x8e, 0, false);
	 //gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 //idt->addIDTDescriptor(INTERRUPT8, (gdt->getNumberOfDescriptor()-1)<<3, 0x8e, 0, false);   //int 8
	 gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);

	 // setup timer interrupt
	 TSS *timerTSS = (TSS *) kmalloc_upper1GB(300);
	 timerTSS->ESP0 = INTERRUPT8 + 0x3000;
	 timerTSS->SS0 = 2 << 3;
	 timerTSS->ESP1 = INTERRUPT8 + 0x4000;
	 timerTSS->SS1 = 2 << 3;
	 timerTSS->ESP2 = INTERRUPT8 + 0x5000;
	 timerTSS->SS2 = 2 << 3;
	 timerTSS->CR3 = KERNEL_PDs;
	 timerTSS->EIP = INTERRUPT8 + 1;
	 timerTSS->EFLAGS = 0;
	 timerTSS->EAX = 0;
	 timerTSS->ECX = 0;
	 timerTSS->EDX = 0;
	 timerTSS->EBX = 0;
	 timerTSS->ESP = INTERRUPT8 + 0x6000;
	 timerTSS->EBP = 0;
	 timerTSS->ESI = 0;
	 timerTSS->EDI = 0;
	 timerTSS->ES = 2 << 3;
	 timerTSS->CS = 1 << 3;
	 timerTSS->SS = 2 << 3;
	 timerTSS->DS = 2 << 3;
	 timerTSS->FS = 2 << 3;
	 timerTSS->GS = 2 << 3;
	 timerTSS->LDTR = 0;
	 timerTSS->T = 0;
	 timerTSS->IOPB = 0;

	 gdt->addGDTDescriptor((unsigned int) timerTSS, 104, 0, 0, 0, 0, 1, 0, 0, 9);
	 idt->addIDTDescriptor(0, (gdt->getNumberOfDescriptor() - 1) << 3, 0x85, 0, false);	//int 9
	 // end setup timer interrupt

	 // setup keyboard interrupt
	 gdt->addGDTDescriptor(0, 0xfffff, 1, 1, 0, 0, 1, 0, 1, 0xa);
	 TSS *keyboardTSS = (TSS *) kmalloc_upper1GB(300);
	 keyboardTSS->ESP0 = INTERRUPT9 + 0x3000;
	 keyboardTSS->SS0 = 2 << 3;
	 keyboardTSS->ESP1 = INTERRUPT9 + 0x4000;
	 keyboardTSS->SS1 = 2 << 3;
	 keyboardTSS->ESP2 = INTERRUPT9 + 0x5000;
	 keyboardTSS->SS2 = 2 << 3;
	 keyboardTSS->CR3 = KERNEL_PDs;
	 keyboardTSS->EIP = INTERRUPT9 + 1;
	 keyboardTSS->EFLAGS = 0;
	 keyboardTSS->EAX = 0;
	 keyboardTSS->ECX = 0;
	 keyboardTSS->EDX = 0;
	 keyboardTSS->EBX = 0;
	 keyboardTSS->ESP = INTERRUPT9 + 0x6000;
	 keyboardTSS->EBP = 0;
	 keyboardTSS->ESI = 0;
	 keyboardTSS->EDI = 0;
	 keyboardTSS->ES = 2 << 3;
	 keyboardTSS->CS = 1 << 3;
	 keyboardTSS->SS = 2 << 3;
	 keyboardTSS->DS = 2 << 3;
	 keyboardTSS->FS = 2 << 3;
	 keyboardTSS->GS = 2 << 3;
	 keyboardTSS->LDTR = 0;
	 keyboardTSS->T = 0;
	 keyboardTSS->IOPB = 0;

	 gdt->addGDTDescriptor((unsigned int) keyboardTSS, 104, 0, 0, 0, 0, 1, 0, 0, 9);
	 idt->addIDTDescriptor(0, (gdt->getNumberOfDescriptor() - 1) << 3, 0x85, 0, false);	//int 9
	 // end setup keyboard interrupt
	 //__asm__ __volatile__("int $0x8");
	 //while(1);
	 __asm__ __volatile__("sti");
	 } else {
	 printf("setupIDT() error\n");
	 while (1);
	 }*/
}

void setupTSS() {

}

void detectVesa() {
	//////////////////////// VESA ///////////////////////////////
	printf("---------------- VESA Informations ---------------------\n");

	if (vesaBiosEnable()) {
		struct VBE_VgaInfo *vesaInfo = getVESAInfo();
		printf("Vesa supported : Yes\n");
		char signature[5];
		strcpy(signature, vesaInfo->VESASignature);
		printf("VbeSignature : %s\n", signature);
		printf("VESAVersion : %d.%d\n", vesaInfo->VESAVersion >> 8, vesaInfo->VESAVersion & 0xff);
		unsigned long capabilities = vesaInfo->Capabilities;
		printf("CapaBilities : \n");
		if (capabilities & 1 == 1) {
			printf("  DAC can be switched into 8-bit mode\n");
		}
		if (capabilities & 2 == 2) {
			printf("  Non-VGA controller\n");
		}
		if (capabilities & 4 == 4) {
			printf("  Programmed DAC with blank bit (i.e. only during blanking interval)\n");
		}
		if (capabilities & 8 == 8) {
			printf("  (VBE v3.0) controller supports hardware stereoscopic signalling, controller supports VBE/AF v1.0P extensions\n");
		}
		if (capabilities & 16 == 16) {
			if (capabilities & 8 == 8) {
				printf("  Stereo signalling via VESA EVC connector\n");
			} else {
				printf("  Stereo signalling via external VESA stereo connector\n");
			}
			printf("  (VBE/AF v1.0P) must call fDirectAccess to access framebuffer\n");
		}
		if (capabilities & 32 == 32) {
			printf("  (VBE/AF v1.0P) controller supports hardware mouse cursor\n");
		}
		if (capabilities & 64 == 64) {
			printf("  (VBE/AF v1.0P) controller supports hardware clipping\n");
		}
		if (capabilities & 128 == 128) {
			printf("  (VBE/AF v1.0P) controller supports transparent BitBLT\n");
		}
		printf("OemStringPtr : %s\n", vesaInfo->OemStringPtr);
	} else {
		printf("Vesa supported : No\n");
	}
	struct VBE_ModeNumberInfo *modeNumberInfo = (struct VBE_ModeNumberInfo *) VESA_MODE;
	struct VBE_ModeInfo *modeinfo = (struct VBE_ModeInfo *) VESA_MODE_INFORMATIONS;
	printf("\n");
	unsigned short *numberOfMode = (unsigned short *) VESA_NUMBER_OF_MODE;
	int x;
	for (x = 0; x < *numberOfMode; x++) {
		printf("Mode number=%x, ", modeNumberInfo->modeNumber);
		printf("bit per pixel=%u, ", modeinfo->BitsPerPixel);
		modeNumberInfo++;
		printf("%x, %ux%u (%u) \n", modeinfo->PhysBasePtr, modeinfo->XRes, modeinfo->YRes, modeinfo->BitsPerPixel);
		modeinfo++;
	}
	printf("\n--------------------------------------------------------\n");
	/////////////////////////// END VESA ///////////////////////////
}

int main() {
	return 0;
}

void dumpXML() {
	// os debug info
	char *osDebugInfo = (char *) 0x100000;
	strcpy(osDebugInfo, "PETER---");
	char xml[4096];
	strcat(xml, "<xml>\n");
	strcat(xml, "<name>");
	strcat(xml, osName);
	strcat(xml, "</name>\n"
			"<kernelName>SL</kernelName>\n"
			"<kernelVersion>1</kernelVersion>\n"
			"<libraries>\n"
			"<library>\n"
			"<name>libc.so</name>\n"
			"<status>loaded</status>\n"
			"</library>\n"
			"<library>\n"
			"<name>libNet.so</name>\n"
			"<status>loaded</status>\n"
			"</library>\n"
			"</libraries>\n"
			"<processes>\n"
			"<process>\n"
			"<name>test.elf</name>\n"
			"<tssNo>12</tssNo>\n"
			"</process>\n"
			"</processes>\n"
			"<processes>\n"
			"<process>\n"
			"<name>test2.elf</name>\n"
			"<tssNo>13</tssNo>\n"
			"</process>\n"
			"</processes>\n"
			"</xml>");

	strcpy(osDebugInfo + 12, xml);
	int xmlLen = strlen(xml);
	osDebugInfo[8] = xmlLen;
	osDebugInfo[9] = xmlLen >> 8;
	osDebugInfo[10] = xmlLen >> 16;
	osDebugInfo[11] = xmlLen >> 24;
	// end os debug info
}

/*void dumpOSStatus() {
 xmlDocPtr doc = NULL;
 xmlNodePtr root_node = NULL, node = NULL, node1 = NULL;
 xmlDtdPtr dtd = NULL;
 char buff[256];
 int i, j;

 LIBXML_TEST_VERSION;

 doc = xmlNewDoc(BAD_CAST "1.0");
 root_node = xmlNewNode(NULL, BAD_CAST "root");
 xmlDocSetRootElement(doc, root_node);
 }*/
