#ifndef __HEADER_H__
#define __HEADER_H__

/*************************
This file have to sync with header_asm.h
*************************/

/************************    os data area  0x90000 - 0x9ffff  *******************************/
#define VBE		0x90000
#define MEMSIZE		0x90001
/*#define CPUID_VENDORSIGN		0x90005	// 13 bytes
#define CPUID_FAMILY		0x90012		// 4 bytes
#define CPUID_MODEL		0x90016			// 4 bytes
#define CPUID_STEPPING		0x9001A		// 4 bytes
#define CPUID_FAMILYEX		0x9001E		// 4 bytes
#define CPUID_MODELEX		0x90022		// 4 bytes
#define CPUID_TYPE		0x90026			// 19 bytes
#define CPUID_BRAND		0x90039			// 4 bytes
#define CPUID_CACHELINESIZE		0x9003D
#define CPUID_LOGICALPROCESSORCOUNT		0x90041
#define CPUID_LOCALAPICID		0x90045
#define CPU_SPEED		0x90049*/
#define VESA_BUFFER		0x9004D	/* 512 bytes buffer */
#define VESA_NUMBER_OF_MODE		0x9024D
#define VESA_MODE		0x9024F	/* assume only got 100 modes, so = 8 bytes * 100 = 800 bytes */
#define VESA_MODE_INFORMATIONS		0x9056F	/* assum only got 100 mode information, so =  256 bytes * 100 = 25600 bytes */
#define DRIVE_TYPE		0x9696F		/* 1 byte */
#define MAX_CYLINDER	0x96970		/* 2 bytes */
#define MAX_HEAD		0x96972		/* 1 byte */
#define MAX_SECTOR		0x96973		/* 1 byte */
#define NUMBER_OF_DRIVE		0x96974
#define DRIVE_PARAMETER_TABLE		0x96975	/* 4 bytes, 32 bits address */
#define KEYBOARD_IS_SHIFT_KEY_PRESSING	0x96979	/* 1 byte */
#define KEYBOARD_IS_CAPSLOCK_ON		0x9697A	/* 1 byte */

/* keyboard */
#define KEYBOARD_BUFFER_SIZE 8
#define KEYBOARD_BUFFER_TAIL			0x9697b	/* 4 bytes */
#define KEYBOARD_BUFFER			0x9697f	/* 8 bytes */
/* keyboard_end */

/************************************************************************************************
////PD0, PT[1..7] is useless now
////PD0                         0x400000		// 4K = 4*2^10
////PT0				0x400000+4096		// 4K = 4*2^10	PT0,1,2... has 1024 page tables
////PT1				0x400000+4096*2		// 4K = 4*2^10
////PT2				0x400000+4096*3		// 4K = 4*2^10
////PT3				0x400000+4096*4		// 4K = 4*2^10
////PT4				0x400000+4096*5		// 4K = 4*2^10
////PT5				0x400000+4096*6		// 4K = 4*2^10
////PT6				0x400000+4096*7		// 4K = 4*2^10
////PT7				0x400000+4096*8		// 4k = 4*2^10
************************************************************************************************/

///////////////////////// memory map //////////////////////////////////////////////////////////

#define INTERRUPT0				0x500000		// 0.5 MB
#define INTERRUPT1				0x508000		// 0.5 MB
#define INTERRUPT2				0x600000		// 0.5 MB
#define INTERRUPT3				0x608000		// 0.5 MB
#define INTERRUPT4				0x700000		// 0.5 MB
#define INTERRUPT5				0x708000		// 0.5 MB
#define INTERRUPT6				0x800000		// 0.5 MB
#define INTERRUPT7				0x808000		// 0.5 MB
#define INTERRUPT8				0x900000		// 0.5 MB
#define TIMER					0x900000		// 0.5 MB
#define INTERRUPT9				0x908000		// 0.5 MB
#define KEYBOARD				0x908000		// 0.5 MB
#define INTERRUPTA				0xa00000		// 0.5 MB
#define INTERRUPTB				0xa08000		// 0.5 MB
#define INTERRUPTC				0xb00000		// 0.5 MB
#define INTERRUPTD				0xb08000		// 0.5 MB

#define KERNEL_PDs				0x1100000		// 1024 PD, 4Bytes each
#define KERNEL_PTs				0x1101000		// 1024 * 1024 PTE * 4 Bytes each
#define FREEPAGELIST			0x1501000
#define FREEPAGELIST_UPPER_1GB	0x1502000

#define KERNEL					0x1600000               // 1 MB
//#define KERNEL_SBRK_LIMIT		0x1680000
#define KERNEL_END				0x20fffff               // used for ESP
//#define PETERLOADER				0x2000000
//#define PETERLOADER_END			0x2100000
#define FREEVIRTUALADDRESSLIST	0x2100000
//#define PLIB					0x2000000               // 1 MB
//#define PLIB_SIZE				1024*1024
#define USEDVIRTUALADDRESSLIST			0x2200000
#define KERNEL_RESERVED_MEMORY			0x2400000
#define KERNEL_RESERVED_MEMORY_END		0x2ffffff
//#define GLOBAL_OFFSET_TABLE				0x3000000


///////////////////////// end memory map //////////////////////////////////////////////////////////

#define FREEVIRTUALADDRESSLIST_UPPER1GB				0x3000000
#define USEDVIRTUALADDRESSLIST_UPPER1GB				0x3100000
#define KERNEL_RESERVED_MEMORY_FOR_UPPER1GB			0x3200000 //0xC0000000
#define KERNEL_RESERVED_MEMORY_FOR_UPPER1GB_END		0x38fffff
#define VIDEO_MEMORY_VIRTUAL_ADDRESS				0x3900000
#define VIDEO_MEMORY_VIRTUAL_ADDRESS_SIZE			0x1000


#define VIDEOMEMORYPHYSICALADDRESS 					0xb8000
#define VIDEOMEMORYADDRESS 							VIDEO_MEMORY_VIRTUAL_ADDRESS

#define _640x800        0
#define _800x600        1
#define _1024x768       2


#define HD_DATA_REGISTER        0x1f0
#define HD_ERROR_REGISTER       0x1f1
#define HD_SECTOR_COUNT         0x1f2
#define HD_SECTOR_NUMBER        0x1f3
#define HD_CYLINDER_LOW         0x1f4
#define HD_CYLINDER_HIGH        0x1f5
#define HD_DRIVE_OR_HEAD        0x1f6
#define HD_STATUS_REGISTER      0x1f7
#define HD_COMMAND_REGISTER     0x1f7

#define PAGE_SIZE	4096

struct U20 {
	unsigned segmentLimit:20;
} __attribute__ ((packed));
typedef struct U20 u20;

struct U12 {
	//unsigned data:12;
	unsigned segmentType:4;
	unsigned S:1;
	unsigned DPL:2;
	unsigned P:1;
	unsigned AVL:1;
	unsigned reserver0:1;
	unsigned D:1;
	unsigned G:1;
} __attribute__ ((packed));
typedef struct U12 u12;

struct U4 {
	unsigned data:4;
} __attribute__ ((packed));

typedef struct U4 u4;


#endif
