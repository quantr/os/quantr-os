mov     cx,NUMBER_OF_SECTOR_TO_READ
repeat_harddisk_read:
	push	cx

	mov	al, 1
	mov	dx, 0x1f2
	out	dx, al

	mov	bx, [lba]

	mov	al, bl
	mov	dx, 0x1f3
	out	dx, al

	mov	al, bh
	mov	dx, 0x1f4
	out	dx, al

	mov	al, 0
	mov	dx, 0x1f5
	out	dx, al

	mov	al, 0xe0 ; drive number
	mov	dx, 0x1f6
	out	dx, al

	mov	al, 0x20
	mov	dx, 0x1f7
	out	dx, al

wait_for_harddisk_read_all_sectors:
	mov	dx, 0x1f7
	in	al, dx
	and	al, 0x8
	cmp	al, 0x8
	jne	wait_for_harddisk_read_all_sectors

	; es:bx is the desination
	mov	ax,[dest]
	mov	es,ax
	mov	bx,0
	; end es:bx is the desination

	mov	cx, MAX_SECTOR_PER_READ_FOR_HARDDISK*512/2
harddisk_read_again:
	mov	dx, 0x1f0
	in	ax, dx
	mov	[es:bx], ax
	add	bx,2
	loop	harddisk_read_again

	;;;;;;;;;;;;;; increase dest ;;;;;;;;;;;;;;;;
        add     word    [dest],0x20

	add	word	[lba],MAX_SECTOR_PER_READ_FOR_HARDDISK
	pop	cx
	loop	repeat_harddisk_read
