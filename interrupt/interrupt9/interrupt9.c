#include <io.h>
#include <display.h>
#include <stdio.h>

//int numberOfKeyHasPressed = 0;

void int9_isr(void)
{
	//printf("int9_isr\n");
	//return;
	/*int x=0;
	   char chr=getScreenChr(0,1);
	   if (chr<0x30 || chr >0x38)
	   chr=0x30;
	   else
	   chr++;
	   printChrPos(chr,0,1); */
	__asm__ __volatile__("clts\n");
	unsigned char c;
	//__asm__ __volatile__("mov     $0x10,%%ax\n" "mov  %%ax,%%ds\n" "in      $0x60,%%al\n" "mov    %%al,%%dl\n" "mov     $0x20,%%al\n" "outb    %%al,$0x20\n":"=d"(c));
	__asm__ __volatile__("in      $0x60,%%al\n" "mov	%%al,%%dl\n" :"=d"(c));

	char *keyboardBuffer = getKeyboardBuffer();
	unsigned int bufferSize = getKeyboardBufferSize();
	volatile unsigned int *bufferTail = getKeyboardBufferTail();
	int isDisplayableKey = 0;


	if (*bufferTail < bufferSize - 1) {
		if (c == 0x36 || c == 0x2a) {
			setShiftKeyPressing(1);
		}
		if (c == 0xb6 || c == 0xaa) {
			setShiftKeyPressing(0);
		}

		switch (c) {
		case 0x1e:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'A';
			} else {
				keyboardBuffer[*bufferTail] = 'a';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x30:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'B';
			} else {
				keyboardBuffer[*bufferTail] = 'b';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x2e:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'C';
			} else {
				keyboardBuffer[*bufferTail] = 'c';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x20:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'D';
			} else {
				keyboardBuffer[*bufferTail] = 'd';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x12:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'E';
			} else {
				keyboardBuffer[*bufferTail] = 'e';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x21:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'F';
			} else {
				keyboardBuffer[*bufferTail] = 'f';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x22:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'G';
			} else {
				keyboardBuffer[*bufferTail] = 'g';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x23:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'H';
			} else {
				keyboardBuffer[*bufferTail] = 'h';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x17:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'I';
			} else {
				keyboardBuffer[*bufferTail] = 'i';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x24:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'J';
			} else {
				keyboardBuffer[*bufferTail] = 'j';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x25:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'K';
			} else {
				keyboardBuffer[*bufferTail] = 'k';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x26:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'L';
			} else {
				keyboardBuffer[*bufferTail] = 'l';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x32:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'M';
			} else {
				keyboardBuffer[*bufferTail] = 'm';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x31:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'N';
			} else {
				keyboardBuffer[*bufferTail] = 'n';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x18:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'O';
			} else {
				keyboardBuffer[*bufferTail] = 'o';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x19:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'P';
			} else {
				keyboardBuffer[*bufferTail] = 'p';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x10:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'Q';
			} else {
				keyboardBuffer[*bufferTail] = 'q';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x13:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'R';
			} else {
				keyboardBuffer[*bufferTail] = 'r';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x1f:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'S';
			} else {
				keyboardBuffer[*bufferTail] = 's';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x14:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'T';
			} else {
				keyboardBuffer[*bufferTail] = 't';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x16:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'U';
			} else {
				keyboardBuffer[*bufferTail] = 'u';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x2f:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'V';
			} else {
				keyboardBuffer[*bufferTail] = 'v';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x11:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'W';
			} else {
				keyboardBuffer[*bufferTail] = 'w';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x2d:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'X';
			} else {
				keyboardBuffer[*bufferTail] = 'x';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x15:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'Y';
			} else {
				keyboardBuffer[*bufferTail] = 'y';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x2c:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = 'Z';
			} else {
				keyboardBuffer[*bufferTail] = 'z';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x0b:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = ')';
			} else {
				keyboardBuffer[*bufferTail] = '0';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x02:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '!';
			} else {
				keyboardBuffer[*bufferTail] = '1';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x03:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '@';
			} else {
				keyboardBuffer[*bufferTail] = '2';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x04:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '#';
			} else {
				keyboardBuffer[*bufferTail] = '3';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x05:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '$';
			} else {
				keyboardBuffer[*bufferTail] = '4';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x06:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '%';
			} else {
				keyboardBuffer[*bufferTail] = '5';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x07:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '^';
			} else {
				keyboardBuffer[*bufferTail] = '6';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x08:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '&';
			} else {
				keyboardBuffer[*bufferTail] = '7';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x09:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '*';
			} else {
				keyboardBuffer[*bufferTail] = '8';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x0a:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '(';
			} else {
				keyboardBuffer[*bufferTail] = '9';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x29:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '~';
			} else {
				keyboardBuffer[*bufferTail] = '`';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x0c:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '_';
			} else {
				keyboardBuffer[*bufferTail] = '-';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x0d:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '+';
			} else {
				keyboardBuffer[*bufferTail] = '=';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x2b:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '|';
			} else {
				keyboardBuffer[*bufferTail] = '\\';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x0e:
			// backspace
			keyboardBuffer[*bufferTail] = 0x8;
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x39:
			// space
			keyboardBuffer[*bufferTail] = ' ';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x0f:
			// tab
			keyboardBuffer[*bufferTail] = '\t';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x3a:
			outport(0x60, 0xed);
			int x;
			for (x = 0; x < 1000; x++) {
				unsigned char a = inport(0x64);
				if (a == 2) {
					break;
				}
			}
			outport(0x60, 0x4);
			break;
		case 0x2a:
			break;
		case 0x1d:
			break;
		case 0x38:
			break;
		case 0x36:
			break;
		case 0x1c:
			keyboardBuffer[*bufferTail] = '\n';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x01:
			break;
		case 0x3b:
			break;
		case 0x3c:
			break;
		case 0x3d:
			break;
		case 0x3e:
			break;
		case 0x3f:
			break;
		case 0x40:
			break;
		case 0x41:
			break;
		case 0x42:
			break;
		case 0x43:
			break;
		case 0x44:
			break;
		case 0x57:
			break;
		case 0x58:
			break;
		case 0x46:
			break;
		case 0xe1:
			break;
		case 0x1a:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '{';
			} else {
				keyboardBuffer[*bufferTail] = '[';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x45:
			break;
		case 0x37:
			keyboardBuffer[*bufferTail] = '*';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x4a:
			keyboardBuffer[*bufferTail] = '-';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x4e:
			keyboardBuffer[*bufferTail] = '+';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x53:
			keyboardBuffer[*bufferTail] = '.';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x52:
			keyboardBuffer[*bufferTail] = '0';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x4f:
			keyboardBuffer[*bufferTail] = '1';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x50:
			keyboardBuffer[*bufferTail] = '2';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x51:
			keyboardBuffer[*bufferTail] = '3';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x4b:
			keyboardBuffer[*bufferTail] = '4';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x4c:
			keyboardBuffer[*bufferTail] = '5';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x4d:
			keyboardBuffer[*bufferTail] = '6';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x47:
			keyboardBuffer[*bufferTail] = '7';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x48:
			keyboardBuffer[*bufferTail] = '8';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x49:
			keyboardBuffer[*bufferTail] = '9';
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x1b:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '}';
			} else {
				keyboardBuffer[*bufferTail] = ']';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x27:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = ':';
			} else {
				keyboardBuffer[*bufferTail] = ';';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x28:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '"';
			} else {
				keyboardBuffer[*bufferTail] = '\'';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x33:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '<';
			} else {
				keyboardBuffer[*bufferTail] = ',';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x34:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '>';
			} else {
				keyboardBuffer[*bufferTail] = '.';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		case 0x35:
			if (isShiftKeyPressing()) {
				keyboardBuffer[*bufferTail] = '?';
			} else {
				keyboardBuffer[*bufferTail] = '/';
			}
			(*bufferTail) = *bufferTail + 1;
			isDisplayableKey = 1;
			break;
		}// end switch

		// display the pressed key to screen
		/*if (isDisplayableKey) {
			if (keyboardBuffer[*bufferTail - 1] == '\n') {
				numberOfKeyHasPressed = 0;
				printf("\n");
			} else if (keyboardBuffer[*bufferTail - 1] == 0x8) {
				//backspace
				if (numberOfKeyHasPressed > 0) {
					//moveCursorLeft();
					printf("XXX");
					//moveCursorLeft();
					numberOfKeyHasPressed--;
					//(*bufferTail)-=2;
					//print("bufferTail=%d\n",*bufferTail);
				}else{
					//(*bufferTail)-=1;
				}
			} else {
				//printf("cursorY=%d\n",cursorY);
				//printf("%c", keyboardBuffer[*bufferTail - 1]);
				numberOfKeyHasPressed++;
			}
		}*/
	}

	/*
	__asm__ __volatile__("pusha");
	__asm__ __volatile__("mov     $0x20,%al");
	__asm__ __volatile__("outb    %al,$0x20");
	__asm__ __volatile__("popa");
	__asm__ __volatile__("iret");
	*/

	//printf("interrupt9 occur\n");
	//print("Windows has a permanent error !!!\n");
}
