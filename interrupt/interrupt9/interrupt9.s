.code32
.global finish_keyboard_interrupt
finish_keyboard_interrupt:
		iret
		pusha
		;fninit
		call    int9_isr
        ;mov     $0xb8006,%eax
        ;incb    (%eax)

		;in      $0x60,%al
		;mov	%al,%dl

        mov     $0x20,%al
        outb    %al,$0x20

        popa
        jmp     finish_keyboard_interrupt
