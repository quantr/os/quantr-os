%xdefine KERNEL_OFFSET 0x8000
%xdefine VIDEO_MEMORY 0xb8000
%xdefine NUMBER_OF_SECTOR_TO_READ 400
%xdefine MAX_SECTOR_PER_READ_FOR_HARDDISK 1

;%xdefine LOAD_FROM_FLOPPY

org	0x7c00

bits      16
first_sector:                                   ;boot sector
;	mov	ax,0x7C0		; boot loader start here , initial all the segment registers

	xor	ax,ax
	mov	ds,ax
	mov	es,ax
	mov	fs,ax
	mov	gs,ax
	mov	ss,ax
	mov	sp,0x8000		; !!! if sp too large, then will break KERNEL_OFFSET segment

	call	clear_screen

	mov	bp,pos
	mov	cx,pos_end-pos
	mov	dl,0xf
	mov	al,0
	mov	bl,0
	call	print_str


;----------------------------------------------------------------------------------------
; read first NUMBER_OF_SECTOR_TO_READ sectors, to 0x8000, ending address is 22DFF
; 0,0,10  : 8 sectors
; 0,1,1   : 18 sectors
; 1,0,1   : 18 sectors
; 1,1,1   : 11 sectors
; 1,1,12  : 7 sectors
; 2,0,1   : 18 sectors
; 2,1,1   : 18 sectors
; 3,0,1   : 18 sectors
; 3,1,1   : 18 sectors
; 4,0,1   : 18 sectors
; 4,1,1   : 18 sectors
; 5,0,1   : 13 sectors
; 5,0,14  : 5 sectors
; 5,1,1   : 18 sectors
; 6,0,1   : 18 sectors
; 6,1,1   : 18 sectors
; 7,0,1   : 18 sectors
; 7,1,1   : 18 sectors
; 8,0,1   : 18 sectors
; 8,1,1   : 15 sectors
;----------------------------------------------------------------------------------------

%ifdef LOAD_FROM_FLOPPY
	mov	cx,NUMBER_OF_SECTOR_TO_READ		; number of sector

repeat_floopy_read:
	push	cx

	mov	ch,[c]
	mov	dh,[h]
	mov	cl,[s]
	mov	al,1
	mov	bx,[dest]
	mov	es,bx
	xor	bx,bx
	call	readFloppy


	;;;;;;;;;;;;;; increase s ;;;;;;;;;;;;;;;;
	inc	byte [s]
	cmp	byte [s],19
	jne	L1
	mov	byte [s],1
	inc	byte [h]
L1:

	;;;;;;;;;;;;;; increase h ;;;;;;;;;;;;;;;;
	cmp	byte [h],2
	jne	no_need_to_reset_h
	mov	byte [h],0
	inc	byte [c]
no_need_to_reset_h:

	;;;;;;;;;;;;;; increase dest ;;;;;;;;;;;;;;;;
	add	word	[dest],0x20

	pop	cx
	loop	repeat_floopy_read

;----------------------------------------------------------------------------------------
	mov	dx,0x3f2			;stop the floppy disk motor
	xor	al,al
	out	dx,al
;----------------------------------------------------------------------------------------
%else
	mov     cx,NUMBER_OF_SECTOR_TO_READ
repeat_harddisk_read:
	push	cx

	mov	al, 1
	mov	dx, 0x1f2
	out	dx, al

	mov	bx, [lba]

	mov	al, bl
	mov	dx, 0x1f3
	out	dx, al

	mov	al, bh
	mov	dx, 0x1f4
	out	dx, al

	mov	al, 0
	mov	dx, 0x1f5
	out	dx, al

	mov	al, 0xe0 ; drive number
	mov	dx, 0x1f6
	out	dx, al

	mov	al, 0x20
	mov	dx, 0x1f7
	out	dx, al

wait_for_harddisk_read_all_sectors:
	mov	dx, 0x1f7
	in	al, dx
	and	al, 0x8
	cmp	al, 0x8
	jne	wait_for_harddisk_read_all_sectors

	; es:bx is the desination
	mov	ax,[dest]
	mov	es,ax
	mov	bx,0
	; end es:bx is the desination

	mov	cx, MAX_SECTOR_PER_READ_FOR_HARDDISK*512/2
harddisk_read_again:
	mov	dx, 0x1f0
	in	ax, dx
	mov	[es:bx], ax
	add	bx,2
	loop	harddisk_read_again

	;;;;;;;;;;;;;; increase dest ;;;;;;;;;;;;;;;;
    add     word    [dest],0x20

	add	word	[lba],MAX_SECTOR_PER_READ_FOR_HARDDISK
	pop	cx
	loop	repeat_harddisk_read
%endif

	jmp     0h:KERNEL_OFFSET

print_str:		;bp = string offset, dl = attri, cx = count ,al = row, bl = column
	xor	ah,ah
	mov	dh,160
	mul	dh
	push	ax	; finish cal row*160

	xor	bh,bh
	mov	ax,bx
	mov	dh,2
	mul	dh	; finish cal col*2
	pop	bx
	add	ax,bx	; ax=row*160+col*2
	mov	bx,ax	; save ax to bx

	push	es
	mov     ax,0xb800
	mov     es,ax

repeat:	mov	ah,[ds:bp]
	mov	[es:bx],ah
	inc	bx
	mov     [es:bx],dl
	inc     bx
	inc	bp
	;dec	cx
	loop	repeat
	pop	es
	ret
;----------------------------------------------------------------------------------------

;----------------------------------------------------------------------------------------
; function read floppy
; paramaters:
;     ch : C
;     dh : H
;     cl : S
;     al : number of byte
;     es:bx : data
;----------------------------------------------------------------------------------------
readFloppy:
	pusha
	xor     ah,ah                           ;reset FDC
	xor     dl,dl
	int     13h
	popa

	mov     ah,2
	mov     dl,0                                    ;drive number (bit 7 set for hard disk)
	int     13h
	cmp     ah,0
	je      finished_read_floppy

	dec     byte [retry_time]
	cmp     byte [retry_time],0
	jne     readFloppy

	;call    printINT13Error
	;call	printINT13Error_simple
halt1:
	jmp	halt1
finished_read_floppy:
	ret


;----------------------------------------------------------------------------------------
; function printINT13Error
; paramaters:
;     ah : error number
;----------------------------------------------------------------------------------------
printINT13Error:
	cmp     ah,4
	jne     next1
	mov     bp,err4
	mov     cx,err4_end-err4
	mov     dl,0xf
	mov     al,1
	mov     bl,0
	call    print_str
	ret
next0:
	cmp	ah,8
	jne	next1
	mov     bp,err8
	mov     cx,err8_end-err8
	mov     dl,0xf
	mov     al,1
	mov     bl,0
	call    print_str
	ret
next1:
	cmp	ah,9
	jne	next2
	mov     bp,err9
	mov     cx,err9_end-err9
	mov     dl,0xf
	mov     al,1
	mov     bl,0
	call    print_str
	ret
next2:
	add     ah,0x30
	mov     [floppy_error_end-1],ah
	mov     bp,floppy_error
	mov     cx,floppy_error_end-floppy_error
	mov     dl,0xf
	mov     al,1
	mov     bl,0
	call    print_str
	ret
;----------------------------------------------------------------------------------------

clear_screen
	pusha
	push	ds
	mov		cx,80*40
	mov		ax,VIDEO_MEMORY/0x10
	mov		ds,ax
	xor		bx,bx
repeat_clear_screen:
	mov		word	[ds:bx],0
	inc		bx
	inc		bx
	loop	repeat_clear_screen
	pop		ds
	popa
	ret



pos:	db	"Peter"
pos_end:

floppy_error:	db	"read floppy, error number : "
floppy_error_end:

err4:	db	"sector not found/read error"
err4_end:

err8:	db	"DMA overrun"
err8_end:

err9:	db	"data boundary error (attempted DMA across 64K boundary or >80h sectors)"
err9_end:

retry_time	db	3
sector_number:	db	1

c	db	0
h	db	0
s	db	2
dest	dw	0x8000 / 0x10
lba	dw	1

%ifndef LOAD_FROM_FLOPPY
sector_read	db	0
%endif

	times   512-($-$$)-2    db      0
	signature	dw	0xaa55
